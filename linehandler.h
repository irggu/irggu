/****************************************************************************
**  linehandler.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef HANDLELINE_H
#define HANDLELINE_H

/**
 *  This class is for handling lines received from server and lines send to server.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-04-01
 */

#include "channel.h"
#include "connection.h"
#include "dbus/dbushandler.h"
#include "other/currentsong.h"
#include <QSslSocket>
#include <QList>
#include <QMap>
#include <QStringList>

enum Commands {
    PING = 1,
    WELLCOME,
    ERRORNICKNAME,
    PRIVMSG,
    JOIN,
    NOTOPIC,
    TOPIC,
    TOPICWHO,
    TOPICSET,
    NAMES,
    ENDOFNAMES,
    WHOSPCRPL,
    ENDOFWHO,
    MODE,
    NP,
    ME,
    QUERY,
    PART,
    NICK,
    WHOIS,
    WHOISUSER,
    WHOISSERVER,
    WHOISOPERATOR,
    WHOISIDLE,
    ENDOFWHOIS,
    WHOISCHANNELS,
    WHOISACCOUNT,
    KICK,
    BANLIST,
    ENDOFBANLIST,
    QUIT,
    RAW,
    NOTICE,
    PONG
};

enum Modes {
    GIVEOP = 1,
    TAKEOP,
    GIVEVOICE,
    TAKEVOICE,
    BAN,
    UNBAN
};

class LineHandler
{
public:
    static void set();
    static void handleIn(QString line, Connection *connection, bool currentConnection);
    static void handleOut(QString line, Connection *connection);

private:
    static QMap<QString, Commands> commandStringToEnum;
    static QMap<QString, Modes>    modeStringToEnum;

    static void    write(QString msg, Connection *connection);
    static void    writeCommand(QString command, Connection *connection);
    static void    writeAction(QString action, Connection *connection);
    static void    addChannel(QMap<QString, Channel *> *channels, Channel **currentChannel,
                              QString channel, int type, Connection *connection);
    static void    removeChannel(QMap<QString, Channel *> *channels, QString channel,
                                 Connection *connection);
    static QString handleCTCPMsg(QString msg, QString sender = "*");
};
#endif // HANDLELINE_H
