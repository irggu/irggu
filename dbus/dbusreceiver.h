/****************************************************************************
**  dbusreceiver.h
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef DBUSRECEIVER_H
#define DBUSRECEIVER_H

/**
 *  This class is for receiving dbus signals.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-03-31
 */

#include <QObject>
#include <QtDBus>

class DbusReceiver : public QObject
{
    Q_OBJECT
public:
    explicit DbusReceiver(QObject *parent = 0);

public Q_SLOTS:
    void startGUI();
    void displayStatusChanged(QString displayStatus);

Q_SIGNALS:
    void startGUICall();
};

#endif // DBUSRECEIVER_H
