/****************************************************************************
**  dbushandlerdata.cpp
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "dbus/dbushandlerdata.h"
#include <QDebug>

DbusReceiver    DbusHandlerData::dbusReceiver;
QDBusConnection DbusHandlerData::dbusSessionConnection = QDBusConnection::sessionBus();
QDBusConnection DbusHandlerData::dbusSystemConnection  = QDBusConnection::systemBus();

/**
 * Returns dbus signal receiver.
 *
 * @return Dbus signal receiver.
 */
DbusReceiver *DbusHandlerData::getReceiver()
{
    return &dbusReceiver;
}

/**
 * Returns dbus session connection.
 *
 * @return Dbus session connection.
 */
QDBusConnection *DbusHandlerData::getDbusSessionConnection()
{
    return &dbusSessionConnection;
}

/**
 * Returns dbus system connection.
 *
 * @return Dbus system connection.
 */
QDBusConnection *DbusHandlerData::getDbusSystemConnection()
{
    return &dbusSystemConnection;
}
