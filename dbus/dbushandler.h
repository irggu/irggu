/****************************************************************************
**  dbushandler.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef DBUSHANDLER_H
#define DBUSHANDLER_H

/**
 *  This class is for using dbus.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-04-02
 */

#include "dbus/dbushandlerdata.h"
#include "dbus/dbusreceiver.h"
#include "dbus/irgguadaptor.h"
#include <QtDBus>

template<typename TP1 = void, typename TP2 = void, typename TP3 = void, typename TP4 = void,
typename TP5 = void, typename TP6 = void, typename TP7 = void, typename TP8 = void>
class DbusHandler
{
public:
    /**
     * Register Dbus service and check screen status.
     *
     * @return Was service registered.
     */
    static bool registerService()
    {
        QDBusConnection *dbusConnection = DbusHandlerData::getDbusSessionConnection();
        DbusReceiver    *dbusReceiver   = DbusHandlerData::getReceiver();

        new IrGGuAdaptor(dbusReceiver);
        bool ok = false;

        if (!dbusConnection->registerService("com.irggu.dbus")) {
            ok = false;
            QDBusMessage msg = QDBusMessage::createMethodCall("com.irggu.dbus", "/",
                                                              "com.irggu.dbus", "startGUI");

            dbusConnection->send(msg);
        } else {
            dbusConnection->registerObject("/", dbusReceiver);
            ok = true;
        }

        QList<QVariant> reply = DbusHandler<QString>::dbusReplyCall("system", "com.nokia.mce",
                                                                    "/com/nokia/mce/request",
                                                                    "com.nokia.mce.request",
                                                                    "get_display_status");

        if (!reply.isEmpty()) {
            dbusReceiver->displayStatusChanged(reply.at(0).toString());

            connectDbusSignal("system", "com.nokia.mce", "/com/nokia/mce/signal",
                              "com.nokia.mce.signal", "display_status_ind", dbusReceiver,
                              SLOT(displayStatusChanged(QString)));
        }

        return ok;
    }

    /**
     * Call Dbus method.
     *
     * @param bus       Dbus bus type.
     * @param service   Dbus service.
     * @param path      Dbus path.
     * @param interface Dbus interface.
     * @param method    Dbus method.
     * @param arguments Dbus method arguments.
     * @return Was message queued successfully.
     */
    static bool dbusCall(QString bus, QString service, QString path, QString interface,
                         QString method, QList<QVariant> arguments = QList<QVariant>())
    {
        bool ok          = false;
        QDBusMessage msg = QDBusMessage::createMethodCall(service, path, interface, method);

        if (!arguments.isEmpty())
            msg.setArguments(arguments);


        if (bus == "system")
            ok = DbusHandlerData::getDbusSystemConnection()->send(msg);
        else
            ok = DbusHandlerData::getDbusSessionConnection()->send(msg);

        return ok;
    }

    /**
     * Call Dbus method with waiting for reply.
     *
     * @param bus       Dbus bus type.
     * @param service   Dbus service.
     * @param path      Dbus path.
     * @param interface Dbus interface.
     * @param method    Dbus method.
     * @param arguments Dbus method arguments.
     * @return Reply.
     */
    static QList<QVariant> dbusReplyCall(QString bus, QString service, QString path,
                                          QString interface, QString method,
                                          QList<QVariant> arguments = QList<QVariant>())
    {
        QList<QVariant> replyList;
        QDBusMessage msg = QDBusMessage::createMethodCall(service, path, interface, method);
        QDBusPendingReply<TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8> reply;

        if (!arguments.isEmpty())
            msg.setArguments(arguments);

        if (bus == "system")
            reply = DbusHandlerData::getDbusSystemConnection()->call(msg);
        else
            reply = DbusHandlerData::getDbusSessionConnection()->call(msg);

        if (reply.isValid()) {
            int x = reply.count();
            for (int i = 0; i < x; i++)
                replyList.append(reply.argumentAt(i));
        }

        return replyList;
    }

    /**
     * Connect slot to Dbus signal.
     *
     * @param bus       Dbus bus type.
     * @param service   Dbus service.
     * @param path      Dbus path.
     * @param interface Dbus interface.
     * @param signal    Dbus signal.
     * @param *object   Pointer to object.
     * @param *slot     Pointer to slot.
     * @return Was connection successful.
     */
    static bool connectDbusSignal(QString bus, QString service, QString path,
                                  QString interface, QString signal, QObject *object,
                                  const char *slot)
    {
        bool ok;
        if (bus == "system")
            ok = DbusHandlerData::getDbusSystemConnection()->connect(service, path, interface,
                                                                     signal, object, slot);
        else
            ok = DbusHandlerData::getDbusSessionConnection()->connect(service, path, interface,
                                                                      signal, object, slot);
        return ok;
    }

    /**
     * Returns Dbus method call receiver.
     *
     * @return Dbus method call receiver.
     */
    static DbusReceiver *getReceiver()
    {
        return DbusHandlerData::getReceiver();
    }
};

#endif // DBUSHANDLER_H
