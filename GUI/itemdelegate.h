/****************************************************************************
**  itemdelegate.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ITEMDELEGATE_H
#define ITEMDELEGATE_H

/**
 *  This class is for controlling painting of QListWidget items.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-01-10
 */

#include <QItemDelegate>
#include <QPainter>

class ItemDelegate: public QItemDelegate
{
public:
    explicit ItemDelegate(QObject *parent = 0);
    void     paint (QPainter *painter, const QStyleOptionViewItem &option,
                    const QModelIndex &index) const;
};


#endif // ITEMDELEGATE_H
