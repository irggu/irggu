/****************************************************************************
**  optionsdialog.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "config.h"
#include "GUI/optionsdialog.h"
#include "GUI/ui_optionsdialog.h"
#include "other/misc.h"
#include "other/sound.h"
#include <QDebug>
#include <QColorDialog>
#include <QFileDialog>

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
OptionsDialog::OptionsDialog(QWidget *parent) : QDialog(parent), ui(new Ui::OptionsDialog)
{
    languages     = QStringList() << "System" << "English" << "Finnish";
    states        = QStringList() << "1. Window active, screen off"
                    << "2. Window deactive, screen off" << "3. Window closed, screen off"
                    << "4. Window active, screen on" << "5. Window deactive, screen on"
                    << "6. Window closed, screen on"
                    << "7. Same as 4th but, alert from currently open chat";
    notifications = QStringList() << "None" << "Email style" << "Interaction required"
                    << "No interaction required";

    ui->setupUi(this);
    setStyle();

    configList             = Config::getConfig();
    getSettings();

    colorsList             = Config::getColorList();
    chatboxFontChanged     = false;
    backgroundColorChanged = false;
    getColorsAndFont();

    lineEdit = NULL;
    setKeyToStringMap();
    shortcuts = Config::getShortcutList();
    getShortcuts();

    getGestureSettings();

    alertsList        = Config::getAlertList();
    alertSettings     = Config::getAlertSettings();
    currentAlertState = "1. Window active, screen off";
    alertChanged      = false;

    ui->oneNotification->setChecked(alertSettings->at(1));
    alertStateChanged(0, false);

    if (alertSettings->at(0)) {
        ui->disableAlerts->setChecked(true);
        disableAlerts(true);
    }

    animationChanged = false;
    getAnimationSettings();

    signalMapperLineEdit = new QSignalMapper(this);
    signalMapperLineEdit->setMapping(ui->nextNetworkEdit, ui->nextNetworkEdit);
    signalMapperLineEdit->setMapping(ui->prevNetworkEdit, ui->prevNetworkEdit);
    signalMapperLineEdit->setMapping(ui->nextChannelEdit, ui->nextChannelEdit);
    signalMapperLineEdit->setMapping(ui->prevChannelEdit, ui->prevChannelEdit);
    signalMapperLineEdit->setMapping(ui->fullscreenEdit, ui->fullscreenEdit);
    connect(ui->nextNetworkEdit, SIGNAL(gotFocus()), signalMapperLineEdit, SLOT(map()));
    connect(ui->prevNetworkEdit, SIGNAL(gotFocus()), signalMapperLineEdit, SLOT(map()));
    connect(ui->nextChannelEdit, SIGNAL(gotFocus()), signalMapperLineEdit, SLOT(map()));
    connect(ui->prevChannelEdit, SIGNAL(gotFocus()), signalMapperLineEdit, SLOT(map()));
    connect(ui->fullscreenEdit, SIGNAL(gotFocus()), signalMapperLineEdit, SLOT(map()));

    signalMapperColor = new QSignalMapper(this);
    signalMapperColor->setMapping(ui->sentMessageColor, ui->sentMessageColor);
    signalMapperColor->setMapping(ui->incommingMessageColor, ui->incommingMessageColor);
    signalMapperColor->setMapping(ui->highlightMessageColor, ui->highlightMessageColor);
    signalMapperColor->setMapping(ui->otherTextColor, ui->otherTextColor);
    signalMapperColor->setMapping(ui->backgroundColor, ui->backgroundColor);

    connect(ui->sentMessageColor, SIGNAL(editingFinished()), signalMapperColor, SLOT(map()));
    connect(ui->incommingMessageColor, SIGNAL(editingFinished()), signalMapperColor, SLOT(map()));
    connect(ui->highlightMessageColor, SIGNAL(editingFinished()), signalMapperColor, SLOT(map()));
    connect(ui->otherTextColor, SIGNAL(editingFinished()), signalMapperColor, SLOT(map()));
    connect(ui->backgroundColor, SIGNAL(editingFinished()), signalMapperColor, SLOT(map()));

    signalMapperColorMap = new QSignalMapper(this);
    signalMapperColorMap->setMapping(ui->sentMessageColorMap, ui->sentMessageColor);
    signalMapperColorMap->setMapping(ui->incommingMessageColorMap, ui->incommingMessageColor);
    signalMapperColorMap->setMapping(ui->highlightMessageColorMap, ui->highlightMessageColor);
    signalMapperColorMap->setMapping(ui->otherTextColorMap, ui->otherTextColor);
    signalMapperColorMap->setMapping(ui->backgroundColorMap, ui->backgroundColor);

    connect(ui->sentMessageColorMap, SIGNAL(clicked()), signalMapperColorMap, SLOT(map()));
    connect(ui->incommingMessageColorMap, SIGNAL(clicked()), signalMapperColorMap, SLOT(map()));
    connect(ui->highlightMessageColorMap, SIGNAL(clicked()), signalMapperColorMap, SLOT(map()));
    connect(ui->otherTextColorMap, SIGNAL(clicked()), signalMapperColorMap, SLOT(map()));
    connect(ui->backgroundColorMap, SIGNAL(clicked()), signalMapperColorMap, SLOT(map()));

    connect(signalMapperLineEdit, SIGNAL(mapped(QWidget*)), this, SLOT(lineEditHasFocus(QWidget*)));
    connect(signalMapperColor, SIGNAL(mapped(QWidget*)), this, SLOT(colorChanged(QWidget*)));
    connect(signalMapperColorMap, SIGNAL(mapped(QWidget*)), this, SLOT(openColorDialog(QWidget*)));
    connect(ui->nextNetworkEdit, SIGNAL(focusLost()), this, SLOT(lineEditLostFocus()));
    connect(ui->prevNetworkEdit, SIGNAL(focusLost()), this, SLOT(lineEditLostFocus()));
    connect(ui->nextChannelEdit, SIGNAL(focusLost()), this, SLOT(lineEditLostFocus()));
    connect(ui->prevChannelEdit, SIGNAL(focusLost()), this, SLOT(lineEditLostFocus()));
    connect(ui->fullscreenEdit, SIGNAL(focusLost()), this, SLOT(lineEditLostFocus()));
}

/**
 * Destructor.
 */
OptionsDialog::~OptionsDialog()
{
    delete ui;
}

/**
 * Sets style sheet.
 */
void OptionsDialog::setStyle()
{
    QStringList styleList = Misc::getOptionsDialogStyle();
    QWidget *object;

    QStringListIterator styleListIt(styleList);
    QString style;
    QString oldStyle;
    QString objectName;

    while (styleListIt.hasNext()) {
        style      = styleListIt.next();
        objectName = style.section(' ', 0, 0);

        if (objectName == "#optionsDialogDialog") {
            oldStyle = this->styleSheet();
            this->setStyleSheet(oldStyle + "\r\n" + style);
        } else {
            object = this->findChild<QWidget *>(objectName.remove(0, 1));

            if (object) {
                oldStyle = object->styleSheet();
                object->setStyleSheet(oldStyle + "\r\n" + style);
            }
        }
    }
}

/**
 * Detect change events.
 *
 * @param *e Pointer to event.
 */
void OptionsDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange: {
            ui->retranslateUi(this);
            QStringList settings = configList->value("Settings");
            ui->language->setCurrentIndex(languages.indexOf(settings.at(1)));
            break;
        }
    default:
        break;
    }
}

/**
 * Detect key Press events.
 *
 * @param *e Pointer to key event.
 */
void OptionsDialog::keyPressEvent(QKeyEvent *e)
{
    if (lineEdit != NULL) {
        switch(e->key()) {
        case Qt::Key_Control:
            modifiersText.append("Ctrl");
            break;
        case Qt::Key_Meta:
            modifiersText.append("Meta");
            break;
        case Qt::Key_Alt:
            modifiersText.append("Alt");
            break;
        case Qt::Key_Shift:
            modifiersText.append("Shift");
            break;
        default:
            break;
        }
    }
}

/**
 * Detect key release events.
 *
 * @param *e Pointer to key event.
 */
void OptionsDialog::keyReleaseEvent(QKeyEvent *e)
{
    if (lineEdit != NULL) {
        if (e->key() != Qt::Key_Control && e->key() != Qt::Key_Meta && e->key() != Qt::Key_Alt &&
            e->key() != Qt::Key_Shift && modifiersText.count() != 0) {
            QString shortcut = "";
            QListIterator<QString> modifiersTextIt(modifiersText);

            while(modifiersTextIt.hasNext()) {
                shortcut += modifiersTextIt.next() + "+";
            }

            if (keyToString[e->key()] != NULL) {
                shortcut += keyToString[e->key()];
            }
            else {
                shortcut += e->text();
            }

            lineEdit->setText(shortcut);
            QString modifier;
            QString key;

            shortcuts->insert(lineEdit->objectName().section("Edit", 0 ,0),
                              QStringList() << shortcut << modifier.setNum(e->modifiers())
                              << key.setNum(e->key()));

            modifiersText.clear();

            Q_EMIT shortcutsChanged();
        }
        modifiersText.clear();
    }
}

/**
 * Detect close events.
 *
 * @param *e Pointer to close event.
 */
void OptionsDialog::closeEvent(QCloseEvent *e)
{
    QStringList settings = configList->value("Settings");

    bool translate = false;

    if (ui->doNotQuit->isChecked()) {
        settings.replace(0, "true");
        qApp->setQuitOnLastWindowClosed(false);
    } else {
        settings.replace(0, "");
        qApp->setQuitOnLastWindowClosed(true);
    }

    int i = ui->language->currentIndex();

    if (i != languages.indexOf(settings.at(1))) {
        settings.replace(1, languages.at(i));
        translate = true;
    }

    settings.replace(2, ui->nickCompletionSuffix->text());
    settings.replace(3, ui->highlights->text());
    settings.replace(4, ui->leaveMessage->text());
    settings.replace(5, ui->quitMessage->text());
    configList->insert("Settings", settings);

    if (backgroundColorChanged || chatboxFontChanged)
        Q_EMIT backgroundAndFontChanged(backgroundColorChanged, chatboxFontChanged);

    bool gesturesChanged = false;
    settings             = configList->value("Gestures");

    settings.replace(0, ui->invertedGestures->isChecked() ? "true" : "");
    if (!settings.at(1).toInt() != ui->moveThreshold->value()) {
        settings.replace(1, QString::number(ui->moveThreshold->value()));
        gesturesChanged = true;
    }

    configList->insert("Gestures", settings);

    if (gesturesChanged)
        Q_EMIT moveThresholdChanged();

    bool setting = ui->disableAlerts->isChecked();

    if (!setting)
        saveAlert(currentAlertState);

    if (alertSettings->at(0) != setting) {
        alertChanged = true;
        alertSettings->replace(0, setting);
    }

    setting = ui->oneNotification->isChecked();

    if (alertSettings->at(1) != setting) {
        alertChanged = true;
        alertSettings->replace(1, setting);
    }

    if (alertChanged)
        Sound::reset();

    settings = configList->value("Animations");

    if (animationChanged) {
        bool checked = ui->enableAnimation->isChecked();
        int value    = ui->animationDuration->value();

        settings.replace(0, checked ? "true" : "");
        settings.replace(1, QString::number(value));
        configList->insert("Animations", settings);

        Q_EMIT scrollAnimationChanged();
    }

    if (translate)
        Q_EMIT languageChanged();

    chatboxFontChanged     = false;
    backgroundColorChanged = false;
    alertChanged           = false;
    ui->tabs->setCurrentIndex(0);
    Config::writeOptions();
    QDialog::closeEvent(e);
}

/**
 * Set key enum to string map.
 */
void OptionsDialog::setKeyToStringMap()
{
    keyToString.insert(Qt::Key_Return, "Return");
    keyToString.insert(Qt::Key_Enter, "Enter");
    keyToString.insert(Qt::Key_Left, "Left");
    keyToString.insert(Qt::Key_Up, "Up");
    keyToString.insert(Qt::Key_Right, "Right");
    keyToString.insert(Qt::Key_Down, "Down");
    keyToString.insert(Qt::Key_Space, "Space");
    keyToString.insert(Qt::Key_Dollar, "$");
    keyToString.insert(Qt::Key_Apostrophe, "'");
    keyToString.insert(Qt::Key_Asterisk, "*");
    keyToString.insert(Qt::Key_Plus, "+");
    keyToString.insert(Qt::Key_Comma, ",");
    keyToString.insert(Qt::Key_Minus, "-");
    keyToString.insert(Qt::Key_Period, ".");
    keyToString.insert(Qt::Key_Slash, "/");
    keyToString.insert(Qt::Key_0, "0");
    keyToString.insert(Qt::Key_1, "1");
    keyToString.insert(Qt::Key_2, "2");
    keyToString.insert(Qt::Key_3, "3");
    keyToString.insert(Qt::Key_4, "4");
    keyToString.insert(Qt::Key_5, "5");
    keyToString.insert(Qt::Key_6, "6");
    keyToString.insert(Qt::Key_7, "7");
    keyToString.insert(Qt::Key_8, "8");
    keyToString.insert(Qt::Key_9, "9");
    keyToString.insert(Qt::Key_Colon, ":");
    keyToString.insert(Qt::Key_Semicolon, ";");
    keyToString.insert(Qt::Key_Less, "<");
    keyToString.insert(Qt::Key_Equal, "=");
    keyToString.insert(Qt::Key_Greater, ">");
    keyToString.insert(Qt::Key_Question, "?");
    keyToString.insert(Qt::Key_At, "@");
    keyToString.insert(Qt::Key_A, "A");
    keyToString.insert(Qt::Key_B, "B");
    keyToString.insert(Qt::Key_C, "C");
    keyToString.insert(Qt::Key_D, "D");
    keyToString.insert(Qt::Key_E, "E");
    keyToString.insert(Qt::Key_F, "F");
    keyToString.insert(Qt::Key_G, "G");
    keyToString.insert(Qt::Key_H, "H");
    keyToString.insert(Qt::Key_I, "I");
    keyToString.insert(Qt::Key_J, "J");
    keyToString.insert(Qt::Key_K, "K");
    keyToString.insert(Qt::Key_L, "L");
    keyToString.insert(Qt::Key_M, "M");
    keyToString.insert(Qt::Key_N, "N");
    keyToString.insert(Qt::Key_O, "O");
    keyToString.insert(Qt::Key_P, "P");
    keyToString.insert(Qt::Key_Q, "Q");
    keyToString.insert(Qt::Key_R, "R");
    keyToString.insert(Qt::Key_S, "S");
    keyToString.insert(Qt::Key_T, "T");
    keyToString.insert(Qt::Key_U, "U");
    keyToString.insert(Qt::Key_V, "V");
    keyToString.insert(Qt::Key_W, "W");
    keyToString.insert(Qt::Key_X, "X");
    keyToString.insert(Qt::Key_Y, "Y");
    keyToString.insert(Qt::Key_Z, "Z");
    keyToString.insert(Qt::Key_ParenLeft, "(");
    keyToString.insert(Qt::Key_ParenRight, ")");
    keyToString.insert(Qt::Key_BracketLeft, "[");
    keyToString.insert(Qt::Key_Backslash, "\\");
    keyToString.insert(Qt::Key_BracketRight, "]");
    keyToString.insert(Qt::Key_Underscore, "_");
    keyToString.insert(Qt::Key_BraceLeft, "{");
    keyToString.insert(Qt::Key_BraceRight, "}");
}

/**
 * Insert application settings to widgets.
 */
void OptionsDialog::getSettings()
{
    QStringList settings = configList->value("Settings");

    ui->doNotQuit->setChecked(!settings.at(0).isEmpty());
    ui->language->setCurrentIndex(languages.indexOf(settings.at(1)));
    ui->nickCompletionSuffix->setText(settings.at(2));
    ui->highlights->setText(settings.at(3));
    ui->leaveMessage->setText(settings.at(4));
    ui->quitMessage->setText(settings.at(5));

    qApp->setQuitOnLastWindowClosed(settings.at(0).isEmpty());
}

/**
 * Insert chatbox colors and font settings to widgets.
 */
void OptionsDialog::getColorsAndFont()
{
    QStringList chatboxFont = configList->value("Chatbox");
    int i = ui->font->findText(chatboxFont.at(0));
    ui->font->setCurrentIndex(i);
    i = ui->fontSize->findText(chatboxFont.at(1));
    ui->fontSize->setCurrentIndex(i);

    QMapIterator<QString, QString> colorsListIt(*colorsList);
    QLineEdit *colorEdit;
    QFont font(chatboxFont.at(0), chatboxFont.at(1).toInt());
    QString bgColor = colorsList->value("background");
    QString color;

    while (colorsListIt.hasNext()) {
        colorsListIt.next();
        color = colorsListIt.value();

        colorEdit = this->findChild<QLineEdit *>(colorsListIt.key() + "Color");
        if (colorEdit) {
            colorEdit->setText(color);
            colorEdit->setFont(font);

            if (colorEdit->objectName() != "backgroundColor") {
                colorEdit->setStyleSheet("background-color: " + bgColor + ";"
                                         "border: 1px solid #888888;"
                                         "border-radius: 6px;"
                                         "color: " + color + ";");
            } else {
                colorEdit->setStyleSheet("background-color: " + bgColor + ";"
                                         "border: 1px solid #888888;"
                                         "border-radius: 6px;"
                                         "color: " + colorsList->value("sentMessage") + ";");
            }
        }
    }


}

/**
 * Insert shorcuts strings to lineedit widgets.
 */
void OptionsDialog::getShortcuts()
{
    QMapIterator<QString, QStringList> shortcutsIt(*shortcuts);

    while (shortcutsIt.hasNext()) {
        shortcutsIt.next();
        QStringList shortcut = shortcutsIt.value();
        lineEdit = this->findChild<LineEdit *>(shortcutsIt.key() + "Edit");
        if (lineEdit) {
            lineEdit->setText(shortcut.at(0));
        }
    }

    lineEdit = NULL;
}

/**
 * Insert application gesture settings to widgets.
 */
void OptionsDialog::getGestureSettings()
{
    QStringList settings = configList->value("Gestures");

    ui->invertedGestures->setChecked(!settings.at(0).isEmpty());
    ui->moveThreshold->setValue(settings.at(1).toInt());
    ui->moveThresholdSlider->setValue(settings.at(1).toInt());
}

/**
 * Insert application animation settings to widgets.
 */
void OptionsDialog::getAnimationSettings()
{
    QStringList settings = configList->value("Animations");

    ui->enableAnimation->setChecked(!settings.at(0).isEmpty());
    ui->animationDuration->setValue(settings.at(1).toInt());
    ui->animationDurationSlider->setValue(settings.at(1).toInt());

    enableAnimation(!settings.at(0).isEmpty());
}

/**
 * Save alert settings.
 *
 * @param state Alert state.
 */
void OptionsDialog::saveAlert(QString state)
{
    bool changed = false;
    QStringList alert = alertsList->value(state, QStringList());

    if (!alert.isEmpty()) {
        QString setting = notifications.at(ui->notification->currentIndex());

        if (alert.at(0) != setting) {
            alert.replace(0, setting);
            changed = true;
        }

        setting = (ui->vibration->isChecked() ? "true" : "");

        if (alert.at(1) != setting) {
            alert.replace(1, setting);
            changed = true;
        }

        setting = (ui->led->isChecked() ? "true" : "");

        if (alert.at(2) != setting) {
            alert.replace(2, setting);
            changed = true;
        }

        setting = (ui->sound->isChecked() ? "true" : "");

        if (alert.at(3) != setting) {
            alert.replace(3, setting);
            changed = true;
        }

        setting = ui->soundFile->text();

        if (alert.at(4) != setting) {
            alert.replace(4, setting);
            changed = true;
        }

        setting = setting.setNum(((float)ui->soundVolume->value()/100));

        if (alert.at(5) != setting) {
            alert.replace(5, setting);
            changed = true;
        }
    } else {
        QString volume;
        volume = volume.setNum(((float)ui->soundVolume->value()/100));

        alert.append(ui->notification->currentText());
        alert.append(ui->vibration->isChecked() ? "true" : "");
        alert.append(ui->led->isChecked() ? "true" : "");
        alert.append(ui->sound->isChecked() ? "true" : "");
        alert.append(ui->soundFile->text());
        alert.append(volume);

        changed = true;
    }


    if (changed) {
        alertsList->insert(state, alert);
        alertChanged = true;
    }
}

/**
 * Detect color settings change.
 *
 * @param *w Pointer to widget that has the value of changed color.
 */
void OptionsDialog::colorChanged(QWidget *w)
{
    QLineEdit *colorEdit = static_cast<QLineEdit *>(w);
    QString name         = colorEdit->objectName();
    QString color        = colorEdit->text();
    colorsList->insert(name.remove("Color"), color);

    if (name == "background")
        backgroundColorChanged = true;

    getColorsAndFont();
}

/**
 * Open color dialog.
 *
 * @param *w Pointer to widget which the color will be inserted.
 */
void OptionsDialog::openColorDialog(QWidget *w)
{
    QLineEdit *colorEdit = static_cast<QLineEdit *>(w);
    QString name         = colorEdit->objectName();
    QString color        = QColorDialog::getColor(QColor(colorEdit->text()), this).name();
    colorEdit->setText(color);
    colorsList->insert(name.remove("Color"), color);

    if (name == "background")
        backgroundColorChanged = true;

    getColorsAndFont();
}

/**
 * Detect font setting change.
 */
void OptionsDialog::fontChanged()
{
    QStringList chatboxFont = configList->value("Chatbox");
    chatboxFontChanged      = true;
    chatboxFont.replace(0, ui->font->currentText());
    configList->insert("Chatbox", chatboxFont);
    getColorsAndFont();
}

/**
 * Detect font size setting change.
 */
void OptionsDialog::fontSizeChanged()
{
    QStringList chatboxFont = configList->value("Chatbox");
    chatboxFontChanged      = true;
    chatboxFont.replace(1, ui->fontSize->currentText());
    configList->insert("Chatbox", chatboxFont);
    getColorsAndFont();
}

/**
 * Detect when lineedit that has value of shortcut has focus.
 *
 * @param *w Pointer to the linedit.
 */
void OptionsDialog::lineEditHasFocus(QWidget *w)
{
    lineEdit = static_cast<LineEdit *>(w);
}

/**
 * Detect when lineedit that has value of shortcut losts focus.
 */
void OptionsDialog::lineEditLostFocus()
{
    lineEdit = NULL;
    modifiersText.clear();
}

/**
 * Detect when gesture threshold value changes.
 *
 * @param value New gesture threshold value.
 */
void OptionsDialog::moveThresholdChanged(int value)
{
    ui->moveThreshold->setValue(value);
    ui->moveThresholdSlider->setValue(value);
}

/**
 * Open file dialog.
 */
void OptionsDialog::openFileDialog()
{
    QString path = QFileDialog::getOpenFileName(this, "Open Sound File", "~/",
                                                "Audio files (*.mp3 *.wav *.aac)");
    if (!path.isEmpty())
        ui->soundFile->setText(QFileInfo(path).filePath());
}

/**
 * Disable or enable alert settings widgets
 */
void OptionsDialog::disableAlerts(bool checked)
{
    if (checked)
    {
        saveAlert(currentAlertState);
        ui->oneNotification->setEnabled(false);
        ui->oneNotification->setChecked(false);
        ui->state->setEnabled(false);
        ui->state->setCurrentIndex(0);
        ui->notification->setEnabled(false);
        ui->notification->setCurrentIndex(0);
        ui->led->setEnabled(false);
        ui->led->setChecked(false);
        ui->vibration->setEnabled(false);
        ui->vibration->setChecked(false);
        ui->sound->setEnabled(false);
        enableSound();
    } else {
        ui->oneNotification->setEnabled(true);
        ui->oneNotification->setChecked(alertSettings->at(1));
        ui->state->setEnabled(true);
        ui->notification->setEnabled(true);
        ui->led->setEnabled(true);
        ui->vibration->setEnabled(true);
        ui->sound->setEnabled(true);
        alertStateChanged(ui->state->currentIndex(), false);
    }
}

/**
 * Disable or enable alert sound settings widgets.
 */
void OptionsDialog::enableSound()
{
    if (ui->sound->isChecked() && ui->sound->isEnabled()) {
        ui->soundFile->setEnabled(true);
        ui->soundVolume->setEnabled(true);
        ui->browse->setEnabled(true);
    } else {
        ui->sound->setChecked(false);
        ui->soundFile->setEnabled(false);
        ui->soundFile->setText("");
        ui->soundVolume->setEnabled(false);
        ui->soundVolume->setValue(100);
        ui->browse->setEnabled(false);
    }
}

/**
 * Detect when alert state value changes.
 *
 * @param i    Alert state index.
 * @param save Save alert.
 */
void OptionsDialog::alertStateChanged(int i, bool save)
{
    if (i != -1 && !ui->disableAlerts->isChecked()) {
        if (save)
            saveAlert(currentAlertState);

        currentAlertState = states.at(i);
        QStringList alert = alertsList->value(currentAlertState, QStringList());

        if (!alert.isEmpty()) {
            ui->notification->setCurrentIndex(notifications.indexOf(alert.at(0)));
            ui->vibration->setChecked(alert.at(1) != "");
            ui->led->setChecked(alert.at(2) != "");
            ui->sound->setChecked(alert.at(3) != "");
            ui->soundFile->setText(alert.at(4));
            ui->soundVolume->setValue((alert.at(5).toFloat()*100));
            enableSound();
        } else {
            ui->notification->setCurrentIndex(0);
            ui->vibration->setChecked(false);
            ui->led->setChecked(false);
            ui->sound->setChecked(false);
            ui->soundFile->setText("");
            ui->soundVolume->setValue(1);
            enableSound();
        }
    }
}

/**
 * Detect when animation duration changes.
 *
 * @param value Animation duration value.
 */
void OptionsDialog::animationDurationChanged(int value)
{
    ui->animationDuration->setValue(value);
    ui->animationDurationSlider->setValue(value);
    animationChanged = true;
}

/**
 * Enable or disable animation settings widgets.
 *
 * @param checked Is enable animation checked.
 */
void OptionsDialog::enableAnimation(bool checked)
{
    if (checked)
    {
        ui->animationDuration->setEnabled(true);
        ui->animationDurationSlider->setEnabled(true);
    } else {
        ui->animationDuration->setEnabled(false);
        ui->animationDurationSlider->setEnabled(false);
    }

    animationChanged = true;
}
