/****************************************************************************
**  optionsdialog.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

/**
 *  This class is for options dialog.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-04-02
 */

#include "GUI/lineedit.h"
#include <QDialog>
#include <QKeyEvent>
#include <QMap>
#include <QSignalMapper>

namespace Ui {
    class OptionsDialog;
}

class OptionsDialog : public QDialog {
    Q_OBJECT
public:
    OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();

    void setStyle();

protected:
    void changeEvent(QEvent *e);
    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void closeEvent(QCloseEvent *e);

private:
    Ui::OptionsDialog            *ui;
    LineEdit                     *lineEdit;
    QMap<QString, QStringList>   *configList;
    QMap<QString, QStringList>   *shortcuts;
    QMap<QString, QStringList>   *alertsList;
    QMap<QString, QString>       *colorsList;
    QMap<int, QString>           keyToString;
    QList<bool>                  *alertSettings;
    QStringList                  languages;
    QStringList                  modifiersText;
    QStringList                  states;
    QStringList                  notifications;
    QString                      currentAlertState;
    bool                         chatboxFontChanged;
    bool                         backgroundColorChanged;
    bool                         alertChanged;
    bool                         animationChanged;
    QSignalMapper                *signalMapperLineEdit;
    QSignalMapper                *signalMapperColor;
    QSignalMapper                *signalMapperColorMap;

    void setKeyToStringMap();
    void getSettings();
    void getColorsAndFont();
    void getShortcuts();
    void getGestureSettings();
    void getAnimationSettings();
    void saveAlert(QString state);

private Q_SLOTS:
    void colorChanged(QWidget *w);
    void openColorDialog(QWidget *w);
    void fontChanged();
    void fontSizeChanged();
    void lineEditHasFocus(QWidget *w);
    void lineEditLostFocus();
    void moveThresholdChanged(int value);
    void openFileDialog();
    void disableAlerts(bool checked);
    void enableSound();
    void alertStateChanged(int i, bool save = true);
    void animationDurationChanged(int value);
    void enableAnimation(bool checked);

Q_SIGNALS:
    void languageChanged();
    void backgroundAndFontChanged(bool, bool);
    void shortcutsChanged();
    void moveThresholdChanged();
    void scrollAnimationChanged();
};

#endif // OPTIONSDIALOG_H
