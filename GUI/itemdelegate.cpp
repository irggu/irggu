/****************************************************************************
**  itemdelegate.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "GUI/itemdelegate.h"
#include <QDebug>

/**
 * Constructor.
 */
ItemDelegate::ItemDelegate(QObject *parent) : QItemDelegate(parent)
{
}

/**
 * Custom paint the QListWidgetItem.
 *
 * @param *painter Pointer to painter.
 * @param &option  Reference to option.
 * @param &index   Reference to index.
 */
void ItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                       const QModelIndex &index) const
{
    QStyleOptionViewItem styleOption(option);

    if (option.state & QStyle::State_Selected)
        painter->fillRect(option.rect, option.palette.color(QPalette::Highlight));

    if (index.column() != 0) {
        styleOption.palette.setColor(QPalette::Highlight, option.palette.color(QPalette::Base));
    }

    QItemDelegate::paint(painter, styleOption, index);
}
