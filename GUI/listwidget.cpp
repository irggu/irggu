/****************************************************************************
**  listwidget.cpp
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "GUI/listwidget.h"
#include <QDebug>

/**
 * Constructor.
 */
ListWidget::ListWidget(QWidget *parent) : QListWidget(parent)
{
}

/**
 * Get mouse press x position.
 *
 * @param *e Pointer to mouse event.
 */
void ListWidget::mousePressEvent(QMouseEvent *e)
{
    startX = e->x();
}

/**
 * If there is no start x position use current.
 *
 * @param *e Pointer to mouse event.
 */
void ListWidget::mouseMoveEvent(QMouseEvent *e)
{
    if (!startX)
        startX = e->x();
}

/**
 * If x position has changed less than 50 pixels select item.
 *
 * @param *e Pointer to mouse event.
 */
void ListWidget::mouseReleaseEvent(QMouseEvent *e)
{
    int distance = e->x() > startX ? e->x() - startX : startX - e->x();

    if (distance < 50) {
        QListWidget::mousePressEvent(e);
        this->setCurrentIndex(this->currentIndex());
    }
}
