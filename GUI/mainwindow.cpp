/****************************************************************************
**  mainwindow.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "config.h"
#include "GUI/mainwindow.h"
#include "GUI/ui_mainwindow.h"
#include "other/misc.h"
#include <QDebug>
#ifdef Q_WS_MAEMO_5
#include <QAbstractKineticScroller>
#endif

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    configList = Config::getConfig();
    setShortcutMap();

    ui->setupUi(this);
    setStyle();
    setBackgroundAndFont(true, true);

    rightSwipeGestureRecognizer = NULL;
    leftSwipeGestureRecognizer  = NULL;
    setGestures();

    ui->networkList->installEventFilter(this);
    ui->channelList->installEventFilter(this);
    ui->namesList->installEventFilter(this);
    ui->chatLine->installEventFilter(this);
    ui->chatBrowser->installEventFilter(this);

    networkListDialog = new NetworkListDialog(this);
    optionsDialog     = new OptionsDialog(this);

    hScrollBar        = ui->scrollArea->horizontalScrollBar();
    hScrollBarState   = chat;
    scrollAnimation   = new QPropertyAnimation(hScrollBar, "value", this);
    setAnimationDuration();
    hScrollBar->setValue(800);

    #ifdef Q_WS_MAEMO_5
    QAbstractKineticScroller *scroller = ui->scrollArea->property("kineticScroller")
                                         .value<QAbstractKineticScroller *>();
    if (scroller) {
        scroller->setEnabled(false);
    }
    #endif

    connect(optionsDialog, SIGNAL(backgroundAndFontChanged(bool,bool)), this,
            SLOT(setBackgroundAndFont(bool,bool)));
    connect(optionsDialog, SIGNAL(shortcutsChanged()), this, SLOT(setShortcutMap()));
    connect(optionsDialog, SIGNAL(moveThresholdChanged()), this, SLOT(setGestures()));
    connect(optionsDialog, SIGNAL(scrollAnimationChanged()), this, SLOT(setAnimationDuration()));
    connect(ui->actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
}

/**
 * Destructor.
 */
MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * Set networks to network list.
 *
 * @param networks       List of networks.
 * @param currentNetwork current network.
 */
void MainWindow::networks(QStringList networks, QString currentNetwork)
{
    QStringListIterator networksIt(networks);
    QList<QListWidgetItem *> itemList;

    while (networksIt.hasNext())
        ui->networkList->addItem(networksIt.next());

    itemList = ui->networkList->findItems(currentNetwork, Qt::MatchCaseSensitive);

    if (!itemList.isEmpty()) {
        ui->networkList->setCurrentItem(itemList.at(0));
        ui->closeNetwork->setEnabled(true);
    }
}

/**
 * Set channels to channel list.
 *
 * @param channels       List of channels.
 * @param currentChannel current channel.
 */
void MainWindow::channels(QStringList channels, QString currentChannel)
{
    channels.removeOne("(server)");

    QStringListIterator channelsIt(channels);
    QList<QListWidgetItem *> itemList;

    ui->channelList->clear();

    ui->channelList->addItem("(server)");

    while (channelsIt.hasNext())
        ui->channelList->addItem(channelsIt.next());

    itemList = ui->channelList->findItems(currentChannel, Qt::MatchCaseSensitive);

    if (!itemList.isEmpty())
        ui->channelList->setCurrentItem(itemList.at(0));

    if (ui->channelList->count() > 1)
        ui->closeChat->setEnabled(true);
    else
        ui->closeChat->setEnabled(false);

    ui->whois->clear();
    ui->query->setEnabled(false);
}

/**
 * Returns pointer to network list.
 *
 * @return Pointer to network list.
 */
QListWidget *MainWindow::getNetworkList()
{
    return ui->networkList;
}

/**
 * Returns pointer to channel list.
 *
 * @return Pointer to channel list.
 */
QListWidget *MainWindow::getChannelList()
{
    return ui->channelList;
}

/**
 * Returns pointer to network list dialog.
 *
 * @return Pointer to network list dialog.
 */
NetworkListDialog *MainWindow::getNetworkListDialog()
{
    return networkListDialog;
}

/**
 * Returns pointer to options dialog.
 *
 * @return Pointer to options dialog.
 */
OptionsDialog *MainWindow::getOptionsDialog()
{
    return optionsDialog;
}

/**
 * Returns pointer to close network button.
 *
 * @return Pointer to close network button.
 */
QPushButton *MainWindow::getCloseNetwork()
{
    return ui->closeNetwork;
}

/**
 * Returns pointer to close chat button.
 *
 * @return Pointer to close chat button.
 */
QPushButton *MainWindow::getCLoseChat()
{
    return ui->closeChat;
}

/**
 * Detects if user used gesture.
 *
 * @param *e Pointer to event.
 * @return Was event recognized and processed.
 */
bool MainWindow::event(QEvent *e)
{
    if (e->type() == QEvent::Gesture && lastEvent != QEvent::Gesture) {
        QGestureEvent *ge    = static_cast<QGestureEvent *>(e);
        QStringList gestures = configList->value("Gestures");

        if (ge->gestures().at(0) == ge->gesture(rightSwipeGesture)) {
            adjustHScroll(gestures.at(0).isEmpty() ? false : true);
        }
        if (ge->gestures().at(0) == ge->gesture(leftSwipeGesture)) {
            adjustHScroll(gestures.at(0).isEmpty() ? true : false);
        }
    }
    lastEvent = e->type();

    return QMainWindow::event(e);
}

/**
 * Detect change events.
 *
 * @param *e Pointer to event.
 */
void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);

    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    case QEvent::ActivationChange:
        Misc::setWindowActive(this->isActiveWindow());
        break;
    case QEvent::PaletteChange:
        if (Misc::setTheme()) {
            setStyle();
            networkListDialog->setStyle();
            optionsDialog->setStyle();
        }
        break;
    case QEvent::StyleChange:
        if (Misc::setTheme()) {
            setStyle();
            networkListDialog->setStyle();
            optionsDialog->setStyle();
        }
        break;
    default:
        break;
    }
}

/**
 * Detect resize events.
 *
 * @param *e Pointer to resize event.
 */
void MainWindow::resizeEvent(QResizeEvent *e)
{
    QMainWindow::resizeEvent(e);
    QScrollBar *sb = ui->chatBrowser->verticalScrollBar();
    sb->setValue(sb->maximum());
}

/**
 * Detect key release events.
 *
 * @param *e Pointer to key event.
 */
void MainWindow::keyReleaseEvent(QKeyEvent *e)
{
    if (!shortcutHandled) {
        QString modifier;
        QString key;

        int shortcut = stringToShortcut.value("modifier:" + modifier.setNum(e->modifiers())
                                              + "!key:" + key.setNum(e->key()), none);

        if (shortcut) {
                handleShortcut(shortcut);
        }
    } else {
        shortcutHandled = false;
    }

    ui->chatLine->setFocus();
    QMainWindow::keyReleaseEvent(e);
}

/**
 * Detect close events.
 *
 * @param *e Pointer to close event.
 */
void MainWindow::closeEvent(QCloseEvent *e)
{
    e->accept();
    Q_EMIT destroyGUI();
}

/**
 * Filters events.
 *
 * @param *e Pointer to event.
 */
bool MainWindow::eventFilter(QObject *, QEvent *e)
{
    if (e->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(e);
        QString modifier;
        QString key;

        int shortcut = stringToShortcut.value("modifier:" + modifier.setNum(keyEvent->modifiers())
                                              + "!key:" + key.setNum(keyEvent->key()), none);

        if (shortcut) {
                handleShortcut(shortcut);
                shortcutHandled = true;
                return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * Sets style sheet.
 */
void MainWindow::setStyle()
{
    QStringList styleList = Misc::getMainWindowStyle();
    QWidget *object;

    QStringListIterator styleListIt(styleList);
    QString style;
    QString oldStyle;
    QString objectName;

    while (styleListIt.hasNext()) {
        style      = styleListIt.next();
        objectName = style.section(' ', 0, 0);

        if (objectName == "#MainWindow") {
            oldStyle = this->styleSheet();
            this->setStyleSheet(oldStyle + "\r\n" + style);
        } else {
            object = this->findChild<QWidget *>(objectName.remove(0, 1));

            if (object) {
                oldStyle = object->styleSheet();
                object->setStyleSheet(oldStyle + "\r\n" + style);
            }
        }
    }
}

/**
 * Switch to fullscreen.
 */
void MainWindow::switchFullscreen()
{
    if (!this->isFullScreen()) this->showFullScreen();
    else this->showNormal();
}

/**
 * Adjusts the horizontal scroll.
 *
 * @param toRight Adjust scroll to right.
 */
void MainWindow::adjustHScroll(bool toRight)
{
    switch (hScrollBarState) {
    case chat:
        if (toRight) {
            scrollAnimation->setStartValue(800);
            scrollAnimation->setEndValue(1600);
            scrollAnimation->start();
            hScrollBarState = names;
        } else {
            scrollAnimation->setStartValue(800);
            scrollAnimation->setEndValue(0);
            scrollAnimation->start();
            hScrollBarState = networksChannels;
        }
        break;
    case networksChannels:
        if (toRight) {
            scrollAnimation->setStartValue(0);
            scrollAnimation->setEndValue(800);
            scrollAnimation->start();
            hScrollBarState = chat;
        }
        break;
    case names:
        if (!toRight) {
            scrollAnimation->setStartValue(1600);
            scrollAnimation->setEndValue(800);
            scrollAnimation->start();
            hScrollBarState = chat;
        }
        break;
    }
}

/**
 * Handle shortcut.
 *
 * @param shortcut Shortcut number.
 */
void MainWindow::handleShortcut(int shortcut)
{
    switch(shortcut) {
    case nextNetwork: {
            int row  = ui->networkList->currentRow();
            int last = ui->networkList->count() - 1;

            if (row != last)
                ui->networkList->setCurrentRow(row + 1);
            else
                ui->networkList->setCurrentRow(0);

            break;
        }
    case prevNetwork: {
            int row  = ui->networkList->currentRow();
            int last = ui->networkList->count() - 1;

            if (row != 0)
                ui->networkList->setCurrentRow(row - 1);
            else
                ui->networkList->setCurrentRow(last);

            break;
        }
    case nextChannel: {
            int row  = ui->channelList->currentRow();
            int last = ui->channelList->count() - 1;

            if (row != last)
                ui->channelList->setCurrentRow(row + 1);
            else
                ui->channelList->setCurrentRow(0);

            ui->whois->clear();
            ui->query->setEnabled(false);
            break;
        }
    case prevChannel: {
            int row  = ui->channelList->currentRow();
            int last = ui->channelList->count() - 1;

            if (row != 0)
                ui->channelList->setCurrentRow(row - 1);
            else
                ui->channelList->setCurrentRow(last);

            ui->whois->clear();
            ui->query->setEnabled(false);
            break;
        }
    case fullscreen:
        switchFullscreen();
        break;
    }
}

/**
 * Set chatbox text.
 *
 * @param chatText Chat text.
 */
void MainWindow::setChatText(QString chatText)
{
    ui->chatBrowser->setText(chatText);
    QScrollBar *sb = ui->chatBrowser->verticalScrollBar();
    sb->setValue(sb->maximum());
}

/**
 * Append text to chatbox.
 *
 * @param chatLine Chat line.
 */
void MainWindow::appendChatText(QString chatLine)
{
    if (chatLine != "") {
        QScrollBar *sb = ui->chatBrowser->verticalScrollBar();
        bool scroll = sb->value() == sb->maximum();

        ui->chatBrowser->append(chatLine);

        if (scroll)
            sb->setValue(sb->maximum());
    }
}

/**
 * Set netwok and channel information.
 *
 * @param ncInfo Netwok and channel information.
 */
void MainWindow::setNcInfo(QString ncInfo)
{
    ui->ncInfo->setText(ncInfo);
}

/**
 * Append nick to chat line.
 *
 * @param nick Nick.
 */
void MainWindow::appendNickToChatLine(QString nick)
{
    QString text = ui->chatLine->text();

    if (text.contains(' ')) {
        text.remove(text.lastIndexOf(' '), (text.length() - text.lastIndexOf(' ')));
        nick = text + " " + nick;
    }

    if (!text.startsWith('/'))
        nick = nick + configList->value("Settings").at(2);

    ui->chatLine->setText(nick);
}

/**
 * Add network to network list.
 *
 * @param network Network.
 */
void MainWindow::networkAdded(QString network)
{
    QListWidgetItem *item = new QListWidgetItem(network, ui->networkList);
    ui->networkList->addItem(item);
    ui->networkList->setCurrentItem(item);
    ui->closeNetwork->setEnabled(true);
}

/**
 * Remove network from network list.
 *
 * @param network Network.
 */
void MainWindow::networkRemoved(QString network)
{
    QList<QListWidgetItem *> itemList = ui->networkList->findItems(network, Qt::MatchCaseSensitive);

    if (!itemList.isEmpty())
        delete itemList.at(0);

    if (ui->networkList->count() > 0) {
        ui->closeNetwork->setEnabled(true);
    } else {
        ui->closeNetwork->setEnabled(false);
        ui->closeChat->setEnabled(false);
        ui->channelList->clear();
        ui->namesList->clear();
        ui->chatBrowser->setText("");
        ui->ncInfo->setText("");
    }

    ui->whois->clear();
    ui->query->setEnabled(false);
}

/**
 * Add channel to channel list.
 *
 * @param channel Channel.
 */
void MainWindow::channelAdded(QString channel)
{
    int count  = ui->channelList->count();
    int i      = 1;
    bool found = false;

    while (!found && i < count) {
        if (channel < ui->channelList->item(i)->text()) {
            QListWidgetItem *item = new QListWidgetItem(channel);
            ui->channelList->insertItem(i, item);
            ui->channelList->setCurrentItem(item);
            found = true;
        }
        i++;
    }

    if (!found) {
        QListWidgetItem *item = new QListWidgetItem(channel);
        ui->channelList->addItem(item);
        ui->channelList->setCurrentItem(item);
    }

    ui->closeChat->setEnabled(true);
    ui->whois->clear();
    ui->query->setEnabled(false);
}

/**
 * Remove channel from channel list.
 *
 * @param channel Channel.
 */
void MainWindow::channelRemoved(QString channel)
{
    QList<QListWidgetItem *> itemList = ui->channelList->findItems(channel, Qt::MatchCaseSensitive);

    if (!itemList.isEmpty())
        delete itemList.at(0);

    if (ui->channelList->count() > 1)
        ui->closeChat->setEnabled(true);
    else
        ui->closeChat->setEnabled(false);

    ui->whois->clear();
    ui->query->setEnabled(false);
}

/**
 * Set names to names list.
 *
 * @param names List of names.
 */
void MainWindow::namesChanged(QStringList names)
{
    QStringListIterator namesIt(names);

    ui->namesList->clear();

    while (namesIt.hasNext())
        ui->namesList->addItem(namesIt.next());

    ui->whois->clear();
    ui->query->setEnabled(false);
}

/**
 * Set new whois information.
 *
 * @param line First line of whois information.
 */
void MainWindow::newWhois(QString line)
{
    ui->whois->clear();
    ui->whois->append(line);
}

/**
 * Append whois information.
 *
 * @param line Line of whois information.
 */
void MainWindow::appendWhois(QString line)
{
    ui->whois->append(line);
}

/**
 * Set chatbox background color and font.
 *
 * @param backgroundColorChanged Has background color changed.
 * @param chatboxFontChanged     Has font changed.
 */
void MainWindow::setBackgroundAndFont(bool backgroundColorChanged, bool chatboxFontChanged)
{
    QStringList chatboxFont = configList->value("Chatbox");

    if (backgroundColorChanged) {
        ui->chatBrowser->setStyleSheet("background-color: "
                                       + Config::getColorList()->value("background") + ";");
    }

    if (chatboxFontChanged) {
        ui->chatBrowser->setFont(QFont(chatboxFont.at(0), chatboxFont.at(1).toInt()));
    }
}

/**
 * Set shortcut string to enum map.
 */
void MainWindow::setShortcutMap()
{
    QMap<QString, QStringList> *shortcutList = Config::getShortcutList();

    QStringList shortcut = shortcutList->value("nextNetwork", QStringList());
    QString     modifier;
    QString     key;

    if (!shortcut.isEmpty()) {
        modifier = shortcut.at(1);
        key      = shortcut.at(2);
        stringToShortcut.insert("modifier:" + modifier + "!key:" + key, nextNetwork);
    }

    shortcut = shortcutList->value("prevNetwork", QStringList());

    if (!shortcut.isEmpty()) {
        modifier = shortcut.at(1);
        key      = shortcut.at(2);
        stringToShortcut.insert("modifier:" + modifier + "!key:" + key, prevNetwork);
    }

    shortcut = shortcutList->value("nextChannel", QStringList());

    if (!shortcut.isEmpty()) {
        modifier = shortcut.at(1);
        key      = shortcut.at(2);
        stringToShortcut.insert("modifier:" + modifier + "!key:" + key, nextChannel);
    }

    shortcut = shortcutList->value("prevChannel", QStringList());

    if (!shortcut.isEmpty()) {
        modifier = shortcut.at(1);
        key      = shortcut.at(2);
        stringToShortcut.insert("modifier:" + modifier + "!key:" + key, prevChannel);
    }

    shortcut = shortcutList->value("fullscreen", QStringList());

    if (!shortcut.isEmpty()) {
        modifier = shortcut.at(1);
        key      = shortcut.at(2);
        stringToShortcut.insert("modifier:" + modifier + "!key:" + key, fullscreen);
    }
}

/**
 * Set gestures.
 */
void MainWindow::setGestures()
{
    QStringList gestures = configList->value("Gestures");

    if (rightSwipeGestureRecognizer && leftSwipeGestureRecognizer) {
        rightSwipeGestureRecognizer->setMoveThreshold(gestures.at(1).toInt());
        leftSwipeGestureRecognizer->setMoveThreshold(gestures.at(1).toInt());
    } else {
        rightSwipeGestureRecognizer = new RightSwipeGestureRecognizer(gestures.at(1).toInt());
        leftSwipeGestureRecognizer  = new LeftSwipeGestureRecognizer(gestures.at(1).toInt());
        rightSwipeGesture = QGestureRecognizer::registerRecognizer(rightSwipeGestureRecognizer);
        leftSwipeGesture  = QGestureRecognizer::registerRecognizer(leftSwipeGestureRecognizer);
        grabGesture(rightSwipeGesture);
        grabGesture(leftSwipeGesture);
    }
}

/**
 * Set animation duration.
 */
void MainWindow::setAnimationDuration()
{
    QStringList gestures = configList->value("Animations");

    if (gestures.at(0).isEmpty())
        scrollAnimation->setDuration(0);
    else
        scrollAnimation->setDuration(gestures.at(1).toInt());
}

/**
 * Open network list dialog.
 */
void MainWindow::openNetworkListDialog()
{
    networkListDialog->exec();
}

/**
 * Open options dialog.
 */
void MainWindow::openOptionsDialog()
{
    optionsDialog->exec();
}

/**
 * Emit line that needs to be written.
 */
void MainWindow::write()
{
    if (ui->chatLine->text() != "") {
        QString line = ui->chatLine->text();
        Q_EMIT lineToWrite(line);
        ui->chatLine->clear();
    }
}

/**
 * complete command or nick or list all that matches to text.
 */
void MainWindow::textCompletion()
{
    QString text = ui->chatLine->text();
    if (text != "") {
        if (text.startsWith("/") && !text.contains(' ')) {
            QString commands = Misc::commandCompletion(text);
            if (commands.contains(' '))
                ui->chatBrowser->append(commands);
            else if (!commands.isEmpty())
                ui->chatLine->setText("/" + commands + " ");
        } else {
            QStringList textList = text.split(' ');
            Q_EMIT nickCompletion(textList.last());
        }
    }
}

/**
 * Close chat.
 */
void MainWindow::closeChat()
{
    Q_EMIT lineToWrite("/LEAVE");
}

/**
 * Get whois information when name is selected.
 */
void MainWindow::nameSelected(QString name)
{
    ui->query->setEnabled(true);

    if (name.at(0) == '@'  || name.at(0) == '+')
        name.remove(0, 1);

    Q_EMIT lineToWrite("/WHOIS " + name);
}

/**
 * Open private chat.
 */
void MainWindow::query()
{
    QString name = ui->namesList->currentItem()->text();

    if (name.at(0) == '@'  || name.at(0) == '+')
        name.remove(0, 1);

    Q_EMIT lineToWrite("/QUERY " + name);
}
