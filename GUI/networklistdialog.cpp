/****************************************************************************
**  networklistdialog.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "config.h"
#include "GUI/networklistdialog.h"
#include "GUI/ui_networklistdialog.h"
#include "GUI/itemdelegate.h"
#include "other/misc.h"
#include <QMessageBox>
#include <QDebug>

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
NetworkListDialog::NetworkListDialog(QWidget *parent) : QDialog(parent),
    ui(new Ui::NetworkListDialog)
{
    ui->setupUi(this);
    setStyle();

    ItemDelegate *itemDelegate = new ItemDelegate(this);
    ui->networkList->setItemDelegate(itemDelegate);
    ui->serverList->setItemDelegate(itemDelegate);
    ui->ignoreTable->setItemDelegate(itemDelegate);
    ui->ignoreTable->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    ui->ignoreTable->verticalHeader()->close();

    configList  = Config::getConfig();
    networkList = Config::getNetworkList();
    charSetIn   = Config::getCharSetIn();
    charSetOut  = Config::getCharSetOut();

    loadConfig();
    loadCharSets();
}

/**
 * Destructor.
 */
NetworkListDialog::~NetworkListDialog()
{
    delete ui;
}

/**
 * Sets style sheet.
 */
void NetworkListDialog::setStyle()
{
    QStringList styleList = Misc::getNetworkListDialogStyle();
    QWidget *object;

    QStringListIterator styleListIt(styleList);
    QString style;
    QString oldStyle;
    QString objectName;

    while (styleListIt.hasNext()) {
        style      = styleListIt.next();
        objectName = style.section(' ', 0, 0);

        if (objectName == "#NetworkListDialog") {
            oldStyle = this->styleSheet();
            this->setStyleSheet(oldStyle + "\r\n" + style);
        } else {
            object = this->findChild<QWidget *>(objectName.remove(0, 1));

            if (object) {
                oldStyle = object->styleSheet();
                object->setStyleSheet(oldStyle + "\r\n" + style);
            }
        }
    }
}

/**
 * Detect change events.
 *
 * @param *e Pointer to event.
 */
void NetworkListDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);

    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

/**
 * Detect close events.
 *
 * @param *e Pointer to close event.
 */
void NetworkListDialog::closeEvent(QCloseEvent *e)
{
    QStringList userConfig = configList->value("User");
    QString text           = ui->nickEdit->text();

    if (!text.isEmpty())
        userConfig.replace(0, text);

    text = ui->alterEdit->text();

    if (!text.isEmpty())
        userConfig.replace(1, text);

    text = ui->nameEdit->text();

    if (!text.isEmpty())
        userConfig.replace(2, text);

    configList->insert("User", userConfig);

    Config::writeOptions();
    Config::writeNetworkList();

    clearAddNetwork(true);
    QDialog::closeEvent(e);
}

/**
 * Inserts network configuration to widgets.
 */
void NetworkListDialog::loadConfig()
{
    QStringList config = configList->value("User");

    ui->nickEdit->setText(config.at(0));
    ui->alterEdit->setText(config.at(1));
    ui->nameEdit->setText(config.at(2));

    if(!networkList->isEmpty()) {
        QMapIterator<QString, QStringList> networkListIt(*networkList);

        while (networkListIt.hasNext()) {
            networkListIt.next();
            QListWidgetItem *item = new QListWidgetItem(networkListIt.key(), ui->networkList);
            ui->networkList->addItem(item);
        }
    }
}

/**
 * Inserts charsets to dropdown widgets.
 */
void NetworkListDialog::loadCharSets()
{
    ui->inCharSet->addItems(*charSetIn);
    ui->outCharSet->addItems(*charSetOut);

    inCharSetIndex = charSetIn->indexOf("latin1/UTF-8");

    if (inCharSetIndex != -1) {
        ui->inCharSet->setCurrentIndex(inCharSetIndex);
    }
    else {
        inCharSetIndex = charSetIn->indexOf("System");
        ui->inCharSet->setCurrentIndex(inCharSetIndex);
    }

    outCharSetIndex = charSetOut->indexOf("UTF-8");

    if (outCharSetIndex != -1) {
        ui->outCharSet->setCurrentIndex(outCharSetIndex);
    }
    else {
        outCharSetIndex = charSetOut->indexOf("System");
        ui->outCharSet->setCurrentIndex(outCharSetIndex);
    }
}

/**
 * Clear widgets that display network configuration.
 *
 * @param all Clear all.
 */
void NetworkListDialog::clearAddNetwork(bool all)
{
    ui->networkEdit->clear();
    ui->serverList->clear();

    for (int i = ui->ignoreTable->rowCount()-1; i > -1; i--)
        ui->ignoreTable->removeRow(i);

    ui->passwdEdit->clear();
    ui->commandsEdit->clear();
    ui->connectStartup->setChecked(false);
    ui->useSsl->setChecked(false);
    ui->ignoreSslErrors->setChecked(false);
    ui->inCharSet->setCurrentIndex(inCharSetIndex);
    ui->outCharSet->setCurrentIndex(outCharSetIndex);
    ui->userInfoGlobal->setChecked(true);
    ui->nickNetworkEdit->clear();
    ui->alterNetworkEdit->clear();
    ui->nameNetworkEdit->clear();
    ui->nickNetworkEdit->setEnabled(false);
    ui->alterNetworkEdit->setEnabled(false);
    ui->nameNetworkEdit->setEnabled(false);

    if (all) {
        ui->networkList->clearSelection();
        ui->networkRemove->setEnabled(false);
        ui->networkModify->setEnabled(false);
        ui->connectToNetwork->setEnabled(false);
        ui->networkTabs->setCurrentIndex(0);
    }
}

/**
 * Add or modify network.
 *
 * @param modify Modify network.
 */
void NetworkListDialog::addNetwork(bool modify)
{
    bool ok         = true;
    QString error   = "";
    QString network = ui->networkEdit->text();

    if (network.isEmpty()) {
        ok = false;
        error = tr("You didn't input network name!");
    } else if (networkList->find(network) != networkList->end() && !modify) {
        ok = false;
        error = tr("There is network ") + network + tr(" already!");
    } else if (modify) {
        QString selectedNetwork = ui->networkList->currentItem()->text();

        if (networkList->find(network) != networkList->find(selectedNetwork)
            && networkList->find(network) != networkList->end()) {
            ok = false;
            error = tr("There is network ") + network + tr(" already!");
        }
    } else if (ui->serverList->count() == 0) {
        ok = false;
        error = tr("You didn't add any servers!");
    } else if(!ui->userInfoGlobal->isChecked()) {
        if (ui->nickNetworkEdit->text().isEmpty()) {
            ok = false;
            error = tr("You didn't input network specific nick!");
        } else if (ui->alterNetworkEdit->text().isEmpty()) {
            ok = false;
            error = tr("You didn't input network specific alternative nick!");
        } else if (ui->nameNetworkEdit->text().isEmpty()) {
            ok = false;
            error = tr("You didn't input network specific real name!");
        }
    }

    if (ok) {
        if (!modify) {
            ui->networkList->addItem(network);
        } else {
            networkList->remove(ui->networkList->currentItem()->text());
            ui->networkList->currentItem()->setText(network);
        }

        QStringList newNetwork;
        int count = ui->serverList->count();
        QString servers = ui->serverList->item(0)->text();

        for (int i = 1; i < count; i++) {
            servers += "," + ui->serverList->item(i)->text();
        }

        count = ui->ignoreTable->rowCount();

        QString ignoreNick        = "";
        QString ignorePrivateChat = "";
        QString ignoreAlerts      = "";

        if (count > 0) {
            ignoreNick        = ui->ignoreTable->item(0, 0)->text();
            int checkState    = ui->ignoreTable->item(0, 1)->checkState();
            ignorePrivateChat = checkState == Qt::Checked ? "true" : "";
            checkState        = ui->ignoreTable->item(0, 2)->checkState();
            ignoreAlerts      = checkState == Qt::Checked ? "true" : "";

            for (int i = 1; i < count; i++) {
                ignoreNick        += "," + ui->ignoreTable->item(i, 0)->text();
                checkState         = ui->ignoreTable->item(i, 1)->checkState();
                ignorePrivateChat += "," + QString(checkState == Qt::Checked ? "true" : "");
                checkState         = ui->ignoreTable->item(i, 2)->checkState();
                ignoreAlerts      += "," + QString(checkState == Qt::Checked ? "true" : "");
            }
        }

        newNetwork.append(servers);
        newNetwork.append(ui->passwdEdit->text());
        newNetwork.append(ui->connectStartup->isChecked() ? "true" : "");
        newNetwork.append(ui->commandsEdit->toPlainText());
        newNetwork.append(ui->useSsl->isChecked() ? "true" : "");
        newNetwork.append(ui->ignoreSslErrors->isChecked() ? "true" : "");
        newNetwork.append(ui->inCharSet->currentText());
        newNetwork.append(ui->outCharSet->currentText());
        newNetwork.append(ui->userInfoGlobal->isChecked() ? "true" : "");
        newNetwork.append(ui->nickNetworkEdit->text());
        newNetwork.append(ui->alterNetworkEdit->text());
        newNetwork.append(ui->nameNetworkEdit->text());
        newNetwork.append(ignoreNick);
        newNetwork.append(ignorePrivateChat);
        newNetwork.append(ignoreAlerts);

        networkList->insert(network, newNetwork);
        clearAddNetwork(true);
    } else {
        QMessageBox::information(this, (!modify ? tr("Cannot add network!")
                                                : tr("Cannot apply settings!")), error);
    }
}

/**
 * Remove network.
 */
void NetworkListDialog::removeNetwork()
{
    networkList->remove(ui->networkList->currentItem()->text());
    delete ui->networkList->currentItem();

    if (networkList->isEmpty()) {
        ui->networkRemove->setEnabled(false);
        ui->networkModify->setEnabled(false);
        ui->connectToNetwork->setEnabled(false);
    } else {
        networkSelected();
    }
}

/**
 * Modify network.
 */
void NetworkListDialog::modifyNetwork()
{
    addNetwork(true);
}

/**
 * Add server to server list.
 */
void NetworkListDialog::addServer()
{
    QListWidgetItem *item = new QListWidgetItem(tr("NewServer/6667"), ui->serverList);
    item->setFlags (item->flags () | Qt::ItemIsEditable);
}

/**
 * Remove server from server list.
 */
void NetworkListDialog::removeServer()
{
    delete ui->serverList->currentItem();

    if (ui->serverList->count() == 0) {
        ui->serverRemove->setEnabled(false);
    }
}

/**
 * Insert network configuration to widgets when network is selected.
 */
void NetworkListDialog::networkSelected()
{
    if (!networkList->isEmpty()) {
        clearAddNetwork();
        ui->networkRemove->setEnabled(true);
        ui->networkModify->setEnabled(true);
        ui->connectToNetwork->setEnabled(true);
        QString current     = ui->networkList->currentItem()->text();
        QStringList network = networkList->value(current);

        ui->networkEdit->setText(current);
        QStringList servers = network.at(0).split(',');
        QListIterator<QString> serversIt(servers);

        while (serversIt.hasNext()) {
            QListWidgetItem *item = new QListWidgetItem(serversIt.next(), ui->serverList);
            item->setFlags (item->flags () | Qt::ItemIsEditable);
            ui->serverList->addItem(item);
        }
        ui->passwdEdit->setText(network.at(1));
        ui->connectStartup->setChecked(network.at(2) != "");
        ui->commandsEdit->setPlainText(network.at(3));
        ui->useSsl->setChecked(network.at(4) != "");
        ui->ignoreSslErrors->setChecked(network.at(5) != "");
        ui->inCharSet->setCurrentIndex(charSetIn->indexOf(network.at(6)));
        ui->outCharSet->setCurrentIndex(charSetOut->indexOf(network.at(7)));
        ui->userInfoGlobal->setChecked(network.at(8) != "");
        ui->nickNetworkEdit->setText(network.at(9));
        ui->alterNetworkEdit->setText(network.at(10));
        ui->nameNetworkEdit->setText(network.at(11));

        if (network.at(8) != "") {
            ui->nickNetworkEdit->setEnabled(false);
            ui->alterNetworkEdit->setEnabled(false);
            ui->nameNetworkEdit->setEnabled(false);
        }
        else {
            ui->nickNetworkEdit->setEnabled(true);
            ui->alterNetworkEdit->setEnabled(true);
            ui->nameNetworkEdit->setEnabled(true);
        }

        if (!network.at(12).isEmpty()) {
            QStringList ignoreNick = network.at(12).split(',');
            QListIterator<QString> ignoreNickIt(ignoreNick);
            QStringList ignorePrivateChat = network.at(13).split(',');
            QListIterator<QString> ignorePrivateChatIt(ignorePrivateChat);
            QStringList ignoreAlerts = network.at(14).split(',');
            QListIterator<QString> ignoreAlertsIt(ignoreAlerts);

            while (ignoreNickIt.hasNext() && ignorePrivateChatIt.hasNext()
                   && ignoreAlertsIt.hasNext()) {
                addIgnore(ignoreNickIt.next(),
                          ignorePrivateChatIt.next() != "" ? Qt::Checked : Qt::Unchecked,
                          ignoreAlertsIt.next() != "" ? Qt::Checked : Qt::Unchecked);
            }
        }
    }
}

/**
 * Enable remove server button when server is selected.
 */
void NetworkListDialog::serverSelected()
{
    if (ui->serverList->count() > 0) {
        ui->serverRemove->setEnabled(true);
    }
}

/**
 * Enable or disable network specific user information widgets.
 */
void NetworkListDialog::userInfoChange()
{
    if (ui->userInfoGlobal->isChecked()) {
        ui->nickNetworkEdit->setEnabled(false);
        ui->alterNetworkEdit->setEnabled(false);
        ui->nameNetworkEdit->setEnabled(false);
    }
    else {
        ui->nickNetworkEdit->setEnabled(true);
        ui->alterNetworkEdit->setEnabled(true);
        ui->nameNetworkEdit->setEnabled(true);
    }
}

/**
 * Add ignore to ignore table.
 *
 * @param nick        Nick.
 * @param privateChat Ignore private chats.
 * @param alerts      Do not create alerts.
 */
void NetworkListDialog::addIgnore(QString nick, Qt::CheckState privateChat, Qt::CheckState alerts)
{
    int row = ui->ignoreTable->rowCount();

    ui->ignoreTable->insertRow(row);
    QTableWidgetItem *item = new QTableWidgetItem(nick);
    ui->ignoreTable->setItem(row, 0, item);
    item = new QTableWidgetItem();
    item->setCheckState(privateChat);
    ui->ignoreTable->setItem(row, 1, item);
    item = new QTableWidgetItem();
    item->setCheckState(alerts);
    ui->ignoreTable->setItem(row, 2, item);
}

/**
 * Remove ignore from ignore table.
 */
void NetworkListDialog::removeIgnore()
{
    ui->ignoreTable->removeRow(ui->ignoreTable->currentRow());

    if (ui->ignoreTable->rowCount() == 0)
        ui->ignoreRemove->setEnabled(false);
    else
        ui->ignoreTable->setCurrentCell(ui->ignoreTable->currentRow(), 0);


}

/**
 * Enable remove ignore button when ignore selected.
 *
 * @param column Column index.
 */
void NetworkListDialog::ignoreSelected(int, int column)
{
    if (ui->ignoreTable->rowCount() > 0 && column == 0)
        ui->ignoreRemove->setEnabled(true);
    else
        ui->ignoreRemove->setEnabled(false);
}

/**
 * Emit connectToNetwork signal.
 */
void NetworkListDialog::connectToNetwork()
{
    QString network = ui->networkList->currentItem()->text();
    QString nick    = ui->nickEdit->text();
    QString alter   = ui->alterEdit->text();
    QString name    = ui->nameEdit->text();

    QStringList networkConfig = networkList->value(network);
    QStringList userConfig    = configList->value("User");

    if (nick.isEmpty())
        nick = userConfig.at(0);

    if (alter.isEmpty())
        alter = userConfig.at(1);

    if (name.isEmpty())
        name = userConfig.at(2);

    if (networkConfig.at(8) != "") {
        Q_EMIT connectToNetwork(network, networkConfig.at(0), nick, alter, name,
                                networkConfig.at(1), networkConfig.at(3), networkConfig.at(4),
                                networkConfig.at(5), networkConfig.at(6), networkConfig.at(7),
                                networkConfig.at(12), networkConfig.at(13), networkConfig.at(14));
    } else {
        Q_EMIT connectToNetwork(network, networkConfig.at(0), networkConfig.at(9),
                                networkConfig.at(10), networkConfig.at(11), networkConfig.at(2),
                                networkConfig.at(3), networkConfig.at(4), networkConfig.at(5),
                                networkConfig.at(6), networkConfig.at(7), networkConfig.at(12),
                                networkConfig.at(13), networkConfig.at(14));
    }

    close();
}
