/****************************************************************************
**  mainwindow.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/**
 *  This class is for mainwindow.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-03-31
 */

#include "customgestures.h"
#include "GUI/networklistdialog.h"
#include "GUI/optionsdialog.h"
#include <QMainWindow>
#include <QKeyEvent>
#include <QScrollBar>
#include <QListWidget>
#include <QPropertyAnimation>

namespace Ui {
    class MainWindow;
}

enum hScrollStates {
    chat,
    networksChannels,
    names
};

enum Shortcuts {
    none,
    nextNetwork,
    prevNetwork,
    nextChannel,
    prevChannel,
    fullscreen
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void              networks(QStringList networks, QString currentNetwork);
    void              channels(QStringList channels, QString currentChannel);
    QListWidget       *getNetworkList();
    QListWidget       *getChannelList();
    NetworkListDialog *getNetworkListDialog();
    OptionsDialog     *getOptionsDialog();
    QPushButton       *getCloseNetwork();
    QPushButton       *getCLoseChat();

protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void closeEvent(QCloseEvent *e);
    bool eventFilter(QObject *obj, QEvent *e);
    bool event(QEvent *e);

private:
    Ui::MainWindow              *ui;
    NetworkListDialog           *networkListDialog;
    OptionsDialog               *optionsDialog;
    QMap<QString, QStringList>  *configList;
    QMap<QString, Shortcuts>    stringToShortcut;
    QScrollBar                  *hScrollBar;
    int                         hScrollBarState;
    int                         lastEvent;
    Qt::GestureType             rightSwipeGesture;
    Qt::GestureType             leftSwipeGesture;
    bool                        shortcutHandled;
    QString                     serverChannelName;
    QPropertyAnimation          *scrollAnimation;
    RightSwipeGestureRecognizer *rightSwipeGestureRecognizer;
    LeftSwipeGestureRecognizer  *leftSwipeGestureRecognizer;

    void setStyle();
    void switchFullscreen();
    void adjustHScroll(bool toRight);
    void handleShortcut(int shortcut);

public Q_SLOTS:
    void setChatText(QString chatText);
    void appendChatText(QString chatLine);
    void setNcInfo(QString ncInfo);
    void appendNickToChatLine(QString nick);
    void networkAdded(QString network);
    void networkRemoved(QString network);
    void channelAdded(QString channel);
    void channelRemoved(QString channel);
    void namesChanged(QStringList names);
    void newWhois(QString line);
    void appendWhois(QString line);

private Q_SLOTS:
    void setBackgroundAndFont(bool backgroundColorChanged, bool chatboxFontChanged);
    void setShortcutMap();
    void setGestures();
    void setAnimationDuration();
    void openNetworkListDialog();
    void openOptionsDialog();
    void write();
    void textCompletion();
    void closeChat();
    void nameSelected(QString name);
    void query();

Q_SIGNALS:
    void lineToWrite(QString);
    void nickCompletion(QString);
    void destroyGUI();
};

#endif // MAINWINDOW_H
