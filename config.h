/****************************************************************************
**  config.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

/**
 *  This class is for managing configuration.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-04-01
 */

#include <QStringList>

class Config
{
public:
    static void                       set();
    static void                       writeNetworkList();
    static void                       writeOptions();
    static QMap<QString, QStringList> *getConfig();
    static QMap<QString, QStringList> *getNetworkList();
    static QMap<QString, QStringList> *getShortcutList();
    static QMap<QString, QStringList> *getAlertList();
    static QMap<QString, QString>     *getColorList();
    static QStringList                *getCharSetIn();
    static QStringList                *getCharSetOut();
    static QStringList                *getHighlightList();
    static QList<bool>                *getAlertSettings();


private:
    static QMap<QString, QStringList> config;
    static QMap<QString, QStringList> networkList;
    static QMap<QString, QStringList> shortcutList;
    static QMap<QString, QStringList> alertList;
    static QMap<QString, QString>     colorList;
    static QStringList                charSetIn;
    static QStringList                charSetOut;
    static QStringList                highlightList;
    static QList<bool>                alertSettings;

    static void readConfig();
    static void readNetworkList();
    static void readCharSets();
};

#endif // CONFIG_H
