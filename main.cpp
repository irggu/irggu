/****************************************************************************
**  main.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "irggu.h"
#include "dbus/dbushandler.h"
#include "application.h"

/**
 * Main.
 *
 * @param argc  Argument count.
 * @param *argv Pointer to argument values.
 */
int main(int argc, char *argv[])
{
    int returnCode = 0;

    if (DbusHandler<>::registerService()) {
        Application a(argc, argv);
        IrGGu irggu(&a);
        returnCode = a.exec();
    }

    return returnCode;
}
