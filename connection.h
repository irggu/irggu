/****************************************************************************
**  connection.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef CONNECTION_H
#define CONNECTION_H

/**
 *  This class is for managing connections to networks.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2010-03-29
 */

#include "channel.h"
#include <QSslSocket>
#include <QTextCodec>
#include <QTimer>

class Connection: public QObject
{
    Q_OBJECT
public:
    Connection(QString network, QStringList addresses, QString nick, QString alter, QString name,
               QString pass, QStringList commands, bool useSsl, bool ignoreSslErrors,
               QString inCharSet, QString outCharSet, QMap<QString, QList<bool> > ignoreList,
               QObject *parent = 0);
    ~Connection();

    void                        start();
    void                        changeChannel(QString channel);
    void                        setCurrent(bool setCurrent);
    void                        write(QString msg);
    void                        channelChanged(bool added = false, bool removed = false,
                                               QString channel = "");
    void                        setNick(QString nick);
    void                        connectSignals(bool newChannel = false);
    void                        disconnectSignals();
    void                        whois(QString line, bool isNewWhois = false);
    QSslSocket                  *getTcpSocket();
    QString                     getNetwork();
    QString                     *getNick();
    QStringList                 getCommands();
    QStringList                 getChannelList();
    QMap<QString, Channel *>    *getChannels();
    QMap<QString, QList<bool> > *getIgnoreList();
    Channel                     *getCurrentChannel();
    Channel                     **getCurrentChannelPointer();
    bool                        getUseAlter();
  
private:
    QSslSocket                  tcpSocket;
    QString                     address;
    QString                     nick;
    QString                     alter;
    QString                     name;
    QString                     network;
    QString                     pass;
    QString                     inCharSet;
    QString                     outCharSet;
    quint16                     port;
    QStringList                 addresses;
    QStringList                 commands;
    QMap<QString, Channel *>    channels;
    QMap<QString, QList<bool> > ignoreList;
    Channel                     *currentChannel;
    bool                        useAlter;
    bool                        current;
    bool                        nickChecked;
    bool                        useSsl;
    bool                        ignoreSslErrors;
    bool                        noData;
    QTimer                      pingTimer;

public Q_SLOTS:
    void setUser(bool alter = false);

private Q_SLOTS:
    void checkConnection();
    void connected();
    void disconnected();
    void displaySslErrors(const QList<QSslError> &errors);
    void startRead();
    void newLine(QString);
    void notificationClicked(QString channel);
    void namesListChanged(QStringList names);
    void ping();

Q_SIGNALS:
    void chatTextChanged(QString);
    void chatTextAppend(QString);
    void ncInfoLabelChanged(QString);
    void changeNetwork(QString);
    void channelAdded(QString);
    void channelRemoved(QString);
    void namesChanged(QStringList);
    void newWhois(QString line);
    void appendWhois(QString line);
};
#endif // CONNECTION_H
