/****************************************************************************
**  irggu.cpp
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "irggu.h"
#include "config.h"
#include "linehandler.h"
#include "alert/alertmanager.h"
#include "dbus/dbushandler.h"
#include "other/misc.h"
#include "other/sound.h"
#include <QLocale>
#include <QPushButton>

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
IrGGu::IrGGu(QObject *parent) : QObject(parent)
{
    Config::set();
    Misc::set();
    AlertManager::set();
    LineHandler::set();

    currentConnection = NULL;
    w                 = NULL;
    dbusReceiver      = DbusHandler<>::getReceiver();
    t                 = new QTranslator();
    loadTranslation();
    qApp->installTranslator(t);

    connectStartUp();
    startGUI();
    Sound::set(this);
    this->connect(dbusReceiver, SIGNAL(startGUICall()), this, SLOT(startGUI()));
}

/**
 * Start GUI.
 */
void IrGGu::startGUI()
{
    if (w == NULL) {
        w = new MainWindow();
        w->setAttribute(Qt::WA_DeleteOnClose, true);
        w->show();
        Misc::setWindowClosed(false);
        connect(w, SIGNAL(destroyGUI()), this, SLOT(destroyGUI()));
        connect(w, SIGNAL(lineToWrite(QString)), this, SLOT(write(QString)));
        connect(w, SIGNAL(nickCompletion(QString)), this, SLOT(nickCompletion(QString)));
        connect(w->getNetworkList(), SIGNAL(currentTextChanged(QString)), this,
                SLOT(changeNetwork(QString)));
        connect(w->getChannelList(), SIGNAL(currentTextChanged(QString)), this,
                SLOT(changeChannel(QString)));
        connect(w->getNetworkListDialog(),
                SIGNAL(connectToNetwork(QString,QString,QString,QString,QString,QString,QString,
                                        QString,QString,QString,QString,QString,QString,QString)),
                this, SLOT(connectNetwork(QString,QString,QString,QString,QString,QString,QString,
                                          QString,QString,QString,QString,QString,QString,
                                          QString)));
        connect(w->getOptionsDialog(), SIGNAL(languageChanged()), this, SLOT(loadTranslation()));
        connect(this, SIGNAL(networkAdded(QString)), w, SLOT(networkAdded(QString)));
        connect(w->getCloseNetwork(), SIGNAL(clicked()), this, SLOT(closeNetwork()));

        if (!connections.isEmpty())
            w->networks(connections.keys(), currentConnection->getNetwork());
    }
}

/**
 * Destroy GUI.
 */
void IrGGu::destroyGUI()
{
    if (currentConnection)
        currentConnection->setCurrent(false);

    disconnect(w, SIGNAL(destroyGUI()), this, SLOT(destroyGUI()));
    disconnect(w, SIGNAL(lineToWrite(QString)), this, SLOT(write(QString)));
    disconnect(w, SIGNAL(nickCompletion(QString)), this, SLOT(nickCompletion(QString)));
    disconnect(w->getNetworkList(), SIGNAL(currentTextChanged(QString)), this,
               SLOT(changeNetwork(QString)));
    disconnect(w->getChannelList(), SIGNAL(currentTextChanged(QString)), this,
               SLOT(changeChannel(QString)));
    disconnect(w->getNetworkListDialog(),
               SIGNAL(connectToNetwork(QString,QString,QString,QString,QString,QString,QString,
                                       QString,QString,QString,QString,QString,QString,QString)),
               this, SLOT(connectNetwork(QString,QString,QString,QString,QString,QString,QString,
                                         QString,QString,QString,QString,QString,QString,
                                         QString)));
    disconnect(this, SIGNAL(networkAdded(QString)), w, SLOT(networkAdded(QString)));
    disconnectSignals();
    delete w;
    w = NULL;
    Misc::setWindowClosed(true);
}

/**
 * Connect networks that are configured to connect on startup.
 */
void IrGGu::connectStartUp()
{
    QMap<QString, QStringList> *networkList = Config::getNetworkList();
    QStringList confList                    = Config::getConfig()->value("User");

    if (!networkList->isEmpty()) {
        QMapIterator<QString, QStringList> networkListIt(*networkList);
        QStringList network;

        while (networkListIt.hasNext()) {
            networkListIt.next();
            network = networkListIt.value();

            if (network.at(2) != "") {
                if (network.at(8) != "") {
                    connectNetwork(networkListIt.key(), network.at(0), confList.at(0),
                                   confList.at(1), confList.at(2), network.at(1), network.at(3),
                                   network.at(4), network.at(5), network.at(6), network.at(7),
                                   network.at(12), network.at(13), network.at(14), false);
                } else {
                    connectNetwork(networkListIt.key(), network.at(0), network.at(9),
                                   network.at(10), network.at(11), network.at(2), network.at(3),
                                   network.at(4), network.at(5), network.at(6), network.at(7),
                                   network.at(12), network.at(13), network.at(14), false);
                }
            }
        }
    }
}

/**
 * Connect signals from current connection.
 */
void IrGGu::connectSignals()
{
    if (currentConnection) {
        connect(currentConnection, SIGNAL(chatTextChanged(QString)), w,
                SLOT(setChatText(QString)));
        connect(currentConnection, SIGNAL(chatTextAppend(QString)), w,
                SLOT(appendChatText(QString)));
        connect(currentConnection, SIGNAL(ncInfoLabelChanged(QString)), w,
                SLOT(setNcInfo(QString)));
        connect(currentConnection, SIGNAL(channelAdded(QString)), w, SLOT(channelAdded(QString)));
        connect(currentConnection, SIGNAL(channelRemoved(QString)), w,
                SLOT(channelRemoved(QString)));
        connect(currentConnection, SIGNAL(namesChanged(QStringList)), w,
                SLOT(namesChanged(QStringList)));
        connect(currentConnection, SIGNAL(newWhois(QString)), w, SLOT(newWhois(QString)));
        connect(currentConnection, SIGNAL(appendWhois(QString)), w, SLOT(appendWhois(QString)));
    }
}

/**
 * Disconnect signals from current connection.
 */
void IrGGu::disconnectSignals()
{
    if (currentConnection) {
        disconnect(currentConnection, SIGNAL(chatTextChanged(QString)), w,
                   SLOT(setChatText(QString)));
        disconnect(currentConnection, SIGNAL(chatTextAppend(QString)), w,
                   SLOT(appendChatText(QString)));
        disconnect(currentConnection, SIGNAL(ncInfoLabelChanged(QString)),
                   w, SLOT(setNcInfo(QString)));
        disconnect(currentConnection, SIGNAL(channelAdded(QString)), w,
                   SLOT(channelAdded(QString)));
        disconnect(currentConnection, SIGNAL(channelRemoved(QString)), w,
                   SLOT(channelRemoved(QString)));
        disconnect(currentConnection, SIGNAL(namesChanged(QStringList)), w,
                   SLOT(namesChanged(QStringList)));
        disconnect(currentConnection, SIGNAL(newWhois(QString)), w, SLOT(newWhois(QString)));
        disconnect(currentConnection, SIGNAL(appendWhois(QString)), w, SLOT(appendWhois(QString)));
    }
}

/**
 * Load translation.
 */
void IrGGu::loadTranslation()
{
    QStringList settings = Config::getConfig()->value("Settings");
    QString     language;

    if (settings.at(1) == "System") {
        language = QLocale::languageToString(QLocale::system().language());
        if (language == "C")
            language = "english";
        else
            language = language.toLower();
    } else {
        language = settings.at(1).toLower();
    }

    t->load(language, "/opt/usr/share/irggu/translations");
}

/**
 * Create new connection, start it and set it as current connection.
 *
 * @param network           Network name.
 * @param addresses         Server addresses.
 * @param nick              Nick.
 * @param alter             Alternative nick.
 * @param name              Real name.
 * @param pass              Server password.
 * @param commands          Commands after connected.
 * @param useSsl            Use SSL.
 * @param ignoreSslErrors   Ignore SSL errors.
 * @param inCharSet         Charset for sending.
 * @param outCharSet        Charset for receiving.
 * @param ignoreNick        Ignore nicks.
 * @param ignorePrivateChat Ignore private chats.
 * @param ignoreAlerts      Ignore alerts.
 * @param signalsToSlots    Connect signals to slots.
 * @param *parent           Pointer to parent.
 */
void IrGGu::connectNetwork(QString network, QString addresses, QString nick, QString alter,
                           QString name, QString pass, QString commands, QString useSsl,
                           QString ignoreSslErrors, QString inCharSet, QString outCharSet,
                           QString ignoreNick, QString ignorePrivateChat, QString ignoreAlerts,
                           bool signalsToSlots)
{
    bool boolUseSsl          = false;
    bool boolIgnoreSslErrors = false;

    if (!connections.isEmpty()) {
        currentConnection->setCurrent(false);
    }

    if (useSsl != "") {
        boolUseSsl = true;

        if (ignoreSslErrors != "")
            boolIgnoreSslErrors = true;
    }

    QMap<QString, QList<bool> > ignoreList;

    if (!ignoreNick.isEmpty()) {
        QStringList ignoreNickList = ignoreNick.split(',');
        QListIterator<QString> ignoreNickIt(ignoreNickList);
        QStringList ignorePrivateChatList = ignorePrivateChat.split(',');
        QListIterator<QString> ignorePrivateChatIt(ignorePrivateChatList);
        QStringList ignoreAlertsList = ignoreAlerts.split(',');
        QListIterator<QString> ignoreAlertsIt(ignoreAlertsList);

        while (ignoreNickIt.hasNext() && ignorePrivateChatIt.hasNext()
               && ignoreAlertsIt.hasNext()) {
            ignoreList.insert(ignoreNickIt.next(),
                              QList<bool>() << (ignorePrivateChatIt.next() != "")
                              << (ignoreAlertsIt.next() != ""));
        }
    }

    QString networkName = network;

    for (int i = 1; connections.find(network) != connections.end(); i++)
        network = networkName + "-" + QString::number(i);

    connections.insert(network, new Connection(network, addresses.split(','), nick, alter, name,
                                               (pass == "" ? "*" : pass), commands.split("\n"),
                                               boolUseSsl, boolIgnoreSslErrors, inCharSet,
                                               outCharSet, ignoreList, this));
    connections.value(network)->start();

    if (signalsToSlots) {
        if (!connections.isEmpty())
        {
            disconnectSignals();
        }

        currentConnection = connections.value(network);

        connectSignals();
    } else {
        currentConnection = connections.value(network);
    }

    connect(currentConnection, SIGNAL(changeNetwork(QString)), this,
            SLOT(changeNetwork(QString)));

    Q_EMIT networkAdded(network);
}

/**
 * Send line that needs to be written to current channel write method.
 *
 * @param line Line that needs to be written.
 */
void IrGGu::write(QString line)
{
    if (currentConnection)
        currentConnection->write(line);
}

/**
 * Send line to current channel to be completed.
 *
 * @param line Line that needs to be completed.
 */
void IrGGu::nickCompletion(QString line)
{
    QString names = currentConnection->getCurrentChannel()->nickCompletion(line);
    if (names.contains(' ')) {
        w->appendChatText(names);
    } else {
        w->appendNickToChatLine(names);
    }
}

/**
 * Change network.
 *
 * @param network Network.
 */
void IrGGu::changeNetwork(QString network)
{
    if (w == NULL)
        startGUI();

    w->activateWindow();

    if (!connections.isEmpty()) {
        currentConnection->setCurrent(false);
        disconnectSignals();

        currentConnection = connections.value(network);

        connectSignals();
        currentConnection->setCurrent(true);
        if (w->getNetworkList()->currentItem()->text() == network) {
            w->channels(currentConnection->getChannelList(),
                        currentConnection->getCurrentChannel()->getName());
        } else {
            QList<QListWidgetItem *> itemList;
            itemList =  w->getNetworkList()->findItems(currentConnection->getNetwork(),
                                                       Qt::MatchCaseSensitive);

            if (!itemList.isEmpty())
                w->getNetworkList()->setCurrentItem(itemList.at(0));
        }
    }
}

/**
 * Change channel.
 *
 * @param channel Channel.
 */
void IrGGu::changeChannel(QString channel)
{
    if (currentConnection && channel != "") {
        currentConnection->changeChannel(channel);

        if (channel == "(server)")
            w->getCLoseChat()->setEnabled(false);
        else
            w->getCLoseChat()->setEnabled(true);
    }
}

/**
 * Close network.
 */
void IrGGu::closeNetwork()
{
    QString network = currentConnection->getNetwork();

    currentConnection->setCurrent(false);
    disconnectSignals();

    delete connections.take(network);

    if (!connections.isEmpty())
        currentConnection = connections.value(connections.keys().at(0));
    else
        currentConnection = NULL;

    w->networkRemoved(network);
}
