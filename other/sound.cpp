/****************************************************************************
**  sound.cpp
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "config.h"
#include "other/sound.h"

QMap<QString, Phonon::MediaObject *> Sound::mediaObjects;
QMap<QString, Phonon::AudioOutput *> Sound::audioOutputs;
Phonon::MediaObject                  *Sound::currentMediaObject = NULL;
QObject                              *Sound::parent;
bool                                 Sound::canUse = false;

/**
 * Sets media objects and audio outputs for alert sounds.
 *
 * @param *parent Pointer to parent.
 */
void Sound::set(QObject *parent)
{
    Sound::parent              = parent;
    QList<bool> *alertSettings = Config::getAlertSettings();

    if (!alertSettings->at(0)) {
        QMap<QString, QStringList> *alerts = Config::getAlertList();

        QMapIterator<QString, QStringList> alertsIt(*alerts);
        QStringList alert;
        QString key;
        Phonon::MediaObject *mediaObject;
        Phonon::AudioOutput *audioOutput;

        while (alertsIt.hasNext()) {
            alertsIt.next();
            key   = alertsIt.key();
            alert = alertsIt.value();

            if (alert.at(3) != "") {
                mediaObject = new Phonon::MediaObject(parent);
                mediaObject->setCurrentSource(Phonon::MediaSource(alert.at(4)));

                audioOutput = new Phonon::AudioOutput(Phonon::NotificationCategory, parent);
                Phonon::createPath(mediaObject, audioOutput);
                audioOutput->setVolume(alert.at(5).toFloat());

                audioOutputs.insert(key, audioOutput);
                mediaObjects.insert(key, mediaObject);
            }
        }

        canUse = true;
    }
}

/**
 * Plays alert sound for alert state.
 *
 * @param alertState Alert state.
 */
void Sound::play(QString alertState)
{
    if (canUse) {
        if (mediaObjects.find(alertState) != mediaObjects.end())
        {
            Phonon::MediaObject *mediaObject = mediaObjects.value(alertState);
            mediaObject->stop();

            if (currentMediaObject != NULL)
                currentMediaObject->stop();


            mediaObject->play();
            currentMediaObject = mediaObject;
        }
    }
}

/**
 * Stops playing alert sound.
 *
 * @param alertState Alert state.
 */
void Sound::stop(QString alertState)
{
    if (canUse) {
        if (mediaObjects.find(alertState) != mediaObjects.end())
        {
            Phonon::MediaObject *mediaObject = mediaObjects.value(alertState);

            if (currentMediaObject == mediaObject) {
                currentMediaObject->stop();
                currentMediaObject = NULL;
            } else {
                mediaObject->stop();
            }
        }
    }
}

/**
 * Resets media objects and audio outputs for alert sounds.
 *
 * @param *parent Pointer to parent.
 */
void Sound::reset()
{
    canUse                     = false;
    QList<bool> *alertSettings = Config::getAlertSettings();

    Phonon::MediaObject *mediaObject;
    Phonon::AudioOutput *audioOutput;

    if (!alertSettings->at(0)) {
        QMap<QString, QStringList> *alerts = Config::getAlertList();
        QMapIterator<QString, QStringList> alertsIt(*alerts);
        QStringList alert;
        QString key;

        while (alertsIt.hasNext()) {
            alertsIt.next();
            key = alertsIt.key();
            alert = alertsIt.value();

            if (alert.at(3) != "" && mediaObjects.find(key) == mediaObjects.end()) {
                mediaObject = new Phonon::MediaObject(parent);
                mediaObject->setCurrentSource(Phonon::MediaSource(alert.at(4)));

                audioOutput = new Phonon::AudioOutput(Phonon::NotificationCategory, parent);
                Phonon::Path path = Phonon::createPath(mediaObject, audioOutput);
                audioOutput->setVolume(alert.at(5).toFloat());

                audioOutputs.insert(key, audioOutput);
                mediaObjects.insert(key, mediaObject);
            } else if (alert.at(3) != ""
                       && mediaObjects.find(key) != mediaObjects.end()) {
                mediaObject = mediaObjects.value(key);
                mediaObject->stop();
                mediaObject->setCurrentSource(Phonon::MediaSource(alert.at(4)));

                audioOutput = audioOutputs.value(key);
                audioOutput->setVolume(alert.at(5).toFloat());
            } else if (alert.at(3) == ""
                       && mediaObjects.find(key) != mediaObjects.end()) {
                mediaObject = mediaObjects.value(key);
                audioOutput = audioOutputs.value(key);

                mediaObject->stop();

                mediaObjects.remove(key);
                audioOutputs.remove(key);

                if (currentMediaObject == mediaObject)
                    currentMediaObject = NULL;

                delete mediaObject;
                delete audioOutput;
            }
        }
        canUse = true;
    } else {
        QMapIterator<QString, Phonon::MediaObject *> mediaObjectsIt(mediaObjects);

        while (mediaObjectsIt.hasNext()) {
            mediaObjectsIt.next();
            mediaObject = mediaObjectsIt.value();
            mediaObject->stop();
            mediaObjects.remove(mediaObjectsIt.key());
            delete mediaObject;
        }

        QMapIterator<QString, Phonon::AudioOutput *> audioOutputsIt(audioOutputs);

        while (audioOutputsIt.hasNext()) {
            audioOutputsIt.next();
            audioOutput = audioOutputsIt.value();
            audioOutputs.remove(audioOutputsIt.key());
            delete audioOutput;
        }

        currentMediaObject = NULL;
    }
}
