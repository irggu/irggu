/****************************************************************************
**  misc.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "misc.h"
#include "dbus/dbushandler.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QStringList>
#include <QNetworkInterface>

QString     Misc::theme;
QString     Misc::styleDir;
QStringList Misc::commands;
bool        Misc::windowActive = true;
bool        Misc::windowClosed = false;
bool        Misc::screenOff    = false;
bool        Misc::alerts;

/**
 * Set members.
 */
void Misc::set()
{
    setTheme();

    commands = QStringList() << "NP" << "NAMES" << "JOIN" << "LEAVE" << "ME" << "TOPIC" << "WHOIS"
                             << "KICK" << "QUERY" << "MODE" << "PART" << "NICK" << "BANLIST"
                             << "PING" << "RAW";
}

/**
 * Set member windowActive.
 *
 * @param active Is window active.
 */
void Misc::setWindowActive(bool active)
{
    windowActive = active;
}

/**
 * Set member windowClosed.
 *
 * @param closed Is window closed.
 */
void Misc::setWindowClosed(bool closed)
{
    windowClosed = closed;
}

/**
 * Set member screenOff.
 *
 * @param off Is screen off.
 */
void Misc::setScreenOff(bool off)
{
    screenOff = off;
}

/**
 * Set member alerts.
 *
 * @param use Use alerts.
 */
void Misc::setAlerts(bool use)
{
    alerts = use;
}

/**
 * Returns command/s that starts with the line.
 *
 * @param line Line that is checked if any command starts with it.
 * @return Command/s start with the line.
 */
QString Misc::commandCompletion(QString line)
{
    QString commandsCompletion;
    QString start = line.section("/", 1).toUpper().trimmed();
    QStringListIterator commandsIt(commands);
    QString command;

    while (commandsIt.hasNext()) {
         command = commandsIt.next();

        if (command.startsWith(start))
            commandsCompletion += command + " ";
    }

    return commandsCompletion.trimmed();
}

/**
 * Returns mainwindows style sheet.
 *
 * @return Mainwindows style sheet.
 */
QStringList Misc::getMainWindowStyle()
{
    QStringList styleList;
    QString style;
    QString line;
    QFile file(styleDir + "mainwindow.css");

    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);

        while (!stream.atEnd()) {
            line = stream.readLine();
            style = "";

            while (!line.trimmed().isEmpty()) {
                style += line;
                line = stream.readLine();
            }

            styleList.append(style);
        }
    }

    return styleList;
}

/**
 * Returns networklist dialog style sheet.
 *
 * @return Networklist dialog style sheet.
 */
QStringList Misc::getNetworkListDialogStyle()
{
    QStringList styleList;
    QString style;
    QString line;
    QFile file(styleDir + "networklistdialog.css");

    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);

        while (!stream.atEnd()) {
            line = stream.readLine();
            style = "";

            while (!line.trimmed().isEmpty()) {
                style += line;
                line = stream.readLine();
            }

            styleList.append(style);
        }
    }

    return styleList;
}

/**
 * Returns options dialog style sheet.
 *
 * @return Options dialog style sheet.
 */
QStringList Misc::getOptionsDialogStyle()
{
    QStringList styleList;
    QString style;
    QString line;
    QFile file(styleDir + "optionsdialog.css");

    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);

        while (!stream.atEnd()) {
            line = stream.readLine();
            style = "";

            while (!line.trimmed().isEmpty()) {
                style += line;
                line = stream.readLine();
            }

            styleList.append(style);
        }
    }

    return styleList;
}

/**
 * Sets theme and styleDir members.
 *
 * @return Was theme changed.
 */
bool Misc::setTheme()
{
    bool themeChanged = false;

    QString newTheme;
    QFile file("/etc/hildon/theme/index.theme");

    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);
        QString     line;
        bool        found = false;

        while (!stream.atEnd() && !found) {
            line = stream.readLine();
            if (line.indexOf("Name=") != -1)
            {
                newTheme = line.section("=",1, 1);
                found = true;
            }
        }
        file.close();
    }

    if (newTheme != theme) {
        theme        = newTheme;
        themeChanged = true;

        if (QDir("/opt/usr/share/irggu/styles/" + theme).exists())
            styleDir = "/opt/usr/share/irggu/styles/" + theme + "/";
        else
            styleDir = "/opt/usr/share/irggu/styles/general/";
    }

    return themeChanged;
}

/**
 * Returns is window active.
 *
 * @return Is window active.
 */
bool Misc::isWindowActive()
{
    return windowActive;
}

/**
 * Returns is window closed.
 *
 * @return Is window closed.
 */
bool Misc::isWindowClosed()
{
    return windowClosed;
}

/**
 * Returns is screen off.
 *
 * @return Is screen off.
 */
bool Misc::isScreenOff()
{
    return screenOff;
}

/**
 * Returns are alerts used.
 *
 * @return Are alerts used.
 */
bool Misc::useAlerts()
{
    return alerts;
}

/**
 * Returns is there network connection.
 *
 * @return Is there network connection.
 */
bool Misc::hasConnection()
{
    QNetworkInterface wlan = QNetworkInterface::interfaceFromName("wlan0");
    QNetworkInterface gprs = QNetworkInterface::interfaceFromName("gprs0");
    bool hasConnection     = false;

    if ((wlan.isValid() && wlan.flags().testFlag(QNetworkInterface::IsUp))
        || (gprs.isValid() && gprs.flags().testFlag(QNetworkInterface::IsUp))) {
        hasConnection = true;
    }

    return hasConnection;
}

/**
 * Returns alert state.
 *
 * @return Alert state.
 */
int Misc::alertState()
{
    int state = 0;
    if (windowClosed) {
        state = screenOff ? 7 : 3;
    } else {
        state = windowActive ? 2 : 1;
        state += screenOff ? 4 : 0;
    }
    return state;
}

