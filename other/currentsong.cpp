/****************************************************************************
**  currentsong.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "currentsong.h"
#include "dbus/dbushandler.h"
#include <QDebug>
#include <QVariant>
#include <QList>

QString         CurrentSong::service         = "com.nokia.mafw.renderer.Mafw-Gst-Renderer-Plugin"
                                               ".gstrenderer";
QString         CurrentSong::path            = "/com/nokia/mafw/renderer/gstrenderer";
QString         CurrentSong::interface       = "com.nokia.mafw.renderer";
QString         CurrentSong::getStatus       = "get_status";
QString         CurrentSong::getMetadata     = "get_current_metadata";

/**
 * Returns current song that MAFW is playing.
 *
 * @return Current song.
 */
QString CurrentSong::getCurrentSong()
{
    QString np = "";
    QList<QVariant> reply = DbusHandler<uint, uint, int, QString>::dbusReplyCall("session",
                                                                                 service, path,
                                                                                 interface,
                                                                                 getStatus);

    if (!reply.isEmpty()) {
        if (reply.at(2) == 1) {
            reply = DbusHandler<QString, QByteArray>::dbusReplyCall("session", service, path,
                                                                    interface, getMetadata);

            if (!reply.isEmpty()) {
                QString type         = reply.at(0).toString();
                QByteArray byteArray = reply.at(1).toByteArray();

                byteArray = byteArray.replace(NULL, 1);
                byteArray = byteArray.replace(244, 1);
                byteArray = byteArray.replace(226, 1);
                byteArray = byteArray.replace(64, 1);
                byteArray = byteArray.replace(20, 1);
                byteArray = byteArray.replace(24, 1);
                byteArray = byteArray.replace(4, 1);

                QString metadata(byteArray.replace(2, 1));
                metadata                 = metadata.replace(QRegExp("+"), "<split>");
                QStringList metadataList = metadata.split("<split>");

                if (type.indexOf("localtagfs") != -1) {
                    np = "is listening to: " + metadataList.at(metadataList.indexOf("artist") + 1)
                         + " - " + metadataList.at(metadataList.indexOf("title") + 1);
                } else {
                    np = "is listening to: " + metadataList.at(metadataList.indexOf("title") + 1);
                }
            }
        }
    }

    return np;
}
