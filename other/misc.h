/****************************************************************************
**  misc.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef MISC_H
#define MISC_H

/**
 *  This class is for miscellaneous functions.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-03-25
 */

#include <QStringList>

class Misc
{
public:
    static void        set();
    static void        setWindowActive(bool active);
    static void        setWindowClosed(bool closed);
    static void        setScreenOff(bool off);
    static void        setAlerts(bool alerts);
    static QString     commandCompletion(QString line);
    static QStringList getMainWindowStyle();
    static QStringList getNetworkListDialogStyle();
    static QStringList getOptionsDialogStyle();
    static bool        setTheme();
    static bool        isWindowActive();
    static bool        isWindowClosed();
    static bool        isScreenOff();
    static bool        useAlerts();
    static bool        hasConnection();
    static int         alertState();

private:
    static QString     theme;
    static QString     styleDir;
    static QStringList commands;
    static bool        windowActive;
    static bool        windowClosed;
    static bool        screenOff;
    static bool        alerts;
};

#endif // MISC_H
