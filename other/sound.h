/****************************************************************************
**  sound.h
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef SOUND_H
#define SOUND_H

/**
 *  This class is for alert sounds.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-03-25
 */

#include <phonon>

class Sound
{
public:
    static void set(QObject *parent);
    static void play(QString alertState);
    static void stop(QString alertState);
    static void reset();

private:
    static QMap<QString, Phonon::MediaObject *> mediaObjects;
    static QMap<QString, Phonon::AudioOutput *> audioOutputs;
    static Phonon::MediaObject                  *currentMediaObject;
    static QObject                              *parent;
    static bool                                 canUse;

};

#endif // SOUND_H
