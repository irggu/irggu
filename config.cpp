/****************************************************************************
**  config.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "config.h"
#include <QSettings>
#include <QDir>
#include <QTextCodec>
#include <QDebug>

QMap<QString, QStringList> Config::config;
QMap<QString, QStringList> Config::networkList;
QMap<QString, QStringList> Config::shortcutList;
QMap<QString, QStringList> Config::alertList;
QMap<QString, QString>     Config::colorList;
QStringList                Config::charSetIn;
QStringList                Config::charSetOut;
QStringList                Config::highlightList;
QList<bool>                Config::alertSettings;

/**
 * Set.
 */
void Config::set()
{
    readConfig();
    readNetworkList();
    readCharSets();
}


/**
 * Returns config.
 *
 * @return Config.
 */
QMap<QString, QStringList> *Config::getConfig()
{
    return &config;
}

/**
 * Returns network list.
 *
 * @return Network List.
 */
QMap<QString, QStringList> *Config::getNetworkList()
{
    return &networkList;
}

/**
 * Returns shortcut list.
 *
 * @return Shortcut List.
 */
QMap<QString, QStringList> *Config::getShortcutList()
{
    return &shortcutList;
}

/**
 * Returns alert list.
 *
 * @return %Alert List.
 */
QMap<QString, QStringList> *Config::getAlertList()
{
    return &alertList;
}

/**
 * Returns chatbox color list.
 *
 * @return Chatbox color list.
 */
QMap<QString, QString> *Config::getColorList()
{
    return &colorList;
}

/**
 * Returns charset options for receiving.
 *
 * @return Charset options for receiving.
 */
QStringList *Config::getCharSetIn()
{
    return &charSetIn;
}

/**
 * Returns charset options for sending.
 *
 * @return Charset options for sending.
 */
QStringList *Config::getCharSetOut()
{
    return &charSetOut;
}

/**
 * Returns highlight list.
 *
 * @return Highlight list.
 */
QStringList *Config::getHighlightList()
{
    return &highlightList;
}

/**
 * Returns alert settings list.
 *
 * @return Alert settings List.
 */
QList<bool> *Config::getAlertSettings()
{
    return &alertSettings;
}

/**
 * Writes network list to file.
 */
void Config::writeNetworkList()
{
    QSettings *networkListFile = new QSettings("irggu", "irggu");
    networkListFile->beginWriteArray("Networks");

    if(!networkList.isEmpty()) {
        QMapIterator<QString, QStringList> networkListIt(networkList);
        QStringList network;
        int i = 0;

        while (networkListIt.hasNext()) {
            networkListIt.next();

            network = networkListIt.value();

            if (network.count() == 15) {
                networkListFile->setArrayIndex(i);
                networkListFile->setValue("network", networkListIt.key());
                networkListFile->setValue("servers", network.at(0));
                networkListFile->setValue("password", network.at(1));
                networkListFile->setValue("autoconnect", network.at(2));
                networkListFile->setValue("command", network.at(3));
                networkListFile->setValue("useSsl", network.at(4));
                networkListFile->setValue("ignoreSslErrors", network.at(5));
                networkListFile->setValue("inCharSet", network.at(6));
                networkListFile->setValue("outCharSet", network.at(7));
                networkListFile->setValue("userInfoGlobal", network.at(8));
                networkListFile->setValue("nickNetwork", network.at(9));
                networkListFile->setValue("alterNetwork", network.at(10));
                networkListFile->setValue("nameNetwork", network.at(11));
                networkListFile->setValue("ignoreNick", network.at(12));
                networkListFile->setValue("ignorePrivateChat", network.at(13));
                networkListFile->setValue("ignoreAlerts", network.at(14));

                i++;
            }
        }
    }
    networkListFile->endArray();
    delete networkListFile;
}

/**
 * Writes options file.
 */
void Config::writeOptions()
{
    //Config::shortcutList = shortcutList;
    QSettings *configFile = new QSettings("irggu", "irggu");

    configFile->setValue("User/nick", config.value("User").at(0));
    configFile->setValue("User/alter", config.value("User").at(1));
    configFile->setValue("User/name", config.value("User").at(2));
    configFile->setValue("Settings/doNotQuit", config.value("Settings").at(0));
    configFile->setValue("Settings/language", config.value("Settings").at(1));
    configFile->setValue("Settings/nickCompeltionSuffix", config.value("Settings").at(2));
    configFile->setValue("Settings/highlights", config.value("Settings").at(3));
    configFile->setValue("Settings/leaveMsg", config.value("Settings").at(4));
    configFile->setValue("Settings/quitMsg", config.value("Settings").at(5));
    configFile->setValue("Chatbox/font", config.value("Chatbox").at(0));
    configFile->setValue("Chatbox/fontSize", config.value("Chatbox").at(1));
    highlightList = config.value("Settings").at(3).split(',');

    configFile->beginWriteArray("ChatboxColors");

    if(!colorList.isEmpty()) {
        QMapIterator<QString, QString> colorListIt(colorList);
        int i = 0;

        while (colorListIt.hasNext()) {
            colorListIt.next();

            configFile->setArrayIndex(i);
            configFile->setValue("name", colorListIt.key());
            configFile->setValue("color", colorListIt.value());

            i++;
        }
    }
    configFile->endArray();

    configFile->beginWriteArray("Shortcuts");

    if(!shortcutList.isEmpty()) {
        QMapIterator<QString, QStringList> shortcutListIt(shortcutList);
        QStringList shortcut;
        int i = 0;

        while (shortcutListIt.hasNext()) {
            shortcutListIt.next();
            shortcut = shortcutListIt.value();

            configFile->setArrayIndex(i);
            configFile->setValue("name", shortcutListIt.key());
            configFile->setValue("shortcut", shortcut.at(0));
            configFile->setValue("modifier", shortcut.at(1));
            configFile->setValue("key", shortcut.at(2));

            i++;
        }
    }
    configFile->endArray();

    configFile->setValue("Gestures/inverted", config.value("Gestures").at(0));
    configFile->setValue("Gestures/moveThreshold", config.value("Gestures").at(1));

    configFile->setValue("Alert/disabled", alertSettings.at(0) ? "true" : "");
    configFile->setValue("Alert/onlyOne", alertSettings.at(1) ? "true" : "");

    configFile->beginWriteArray("Alerts");

    if(!alertList.isEmpty()) {
        QMapIterator<QString, QStringList> alertListIt(alertList);
        QStringList alert;
        int i = 0;

        while (alertListIt.hasNext()) {
            alertListIt.next();
            alert = alertListIt.value();

            configFile->setArrayIndex(i);
            configFile->setValue("state", alertListIt.key());
            configFile->setValue("notification", alert.at(0));
            configFile->setValue("vibration", alert.at(1));
            configFile->setValue("led", alert.at(2));
            configFile->setValue("sound", alert.at(3));
            configFile->setValue("soundPath", alert.at(4));
            configFile->setValue("soundVolume", alert.at(5));
            i++;
        }
    }
    configFile->endArray();

    configFile->setValue("Animations/enable", config.value("Animations").at(0));
    configFile->setValue("Animations/duration", config.value("Animations").at(1));

    delete configFile;
}

/**
 * Reads config from file.
 */
void Config::readConfig()
{
    QSettings *configFile = new QSettings("irggu", "irggu");

    if (configFile->value("Version") != "2011-04-01") {
        configFile->setValue("Version", "2011-04-01");
        configFile->setValue("User/nick", "IrGGu^^");
        configFile->setValue("User/alter", "IrGGu^_^");
        configFile->setValue("User/name", "IrGGu");
        configFile->setValue("Settings/doNotQuit", "");
        configFile->setValue("Settings/language", "System");
        configFile->setValue("Settings/nickCompeltionSuffix", " ");
        configFile->setValue("Settings/highlights", "");
        configFile->setValue("Settings/leaveMsg", "");
        configFile->setValue("Settings/quitMsg", "");
        configFile->setValue("Chatbox/font", "Monospace");
        configFile->setValue("Chatbox/fontSize", "9");

        configFile->beginWriteArray("ChatboxColors");
        configFile->setArrayIndex(0);
        configFile->setValue("name", "sentMessage");
        configFile->setValue("color", "#0000ff");
        configFile->setArrayIndex(1);
        configFile->setValue("name", "incommingMessage");
        configFile->setValue("color", "#000000");
        configFile->setArrayIndex(2);
        configFile->setValue("name", "highlightMessage");
        configFile->setValue("color", "#ff0000");
        configFile->setArrayIndex(3);
        configFile->setValue("name", "otherText");
        configFile->setValue("color", "#B037B0");
        configFile->setArrayIndex(4);
        configFile->setValue("name", "background");
        configFile->setValue("color", "#ffffff");
        configFile->endArray();

        configFile->beginWriteArray("Shortcuts");
        configFile->setArrayIndex(0);
        configFile->setValue("name", "nextNetwork");
        configFile->setValue("shortcut", "Ctrl+Down");
        configFile->setValue("modifier", "67108864");
        configFile->setValue("key", "16777237");
        configFile->setArrayIndex(1);
        configFile->setValue("name", "prevNetwork");
        configFile->setValue("shortcut", "Ctrl+Up");
        configFile->setValue("modifier", "67108864");
        configFile->setValue("key", "16777235");
        configFile->setArrayIndex(2);
        configFile->setValue("name", "nextChannel");
        configFile->setValue("shortcut", "Ctrl+Right");
        configFile->setValue("modifier", "67108864");
        configFile->setValue("key", "16777236");
        configFile->setArrayIndex(3);
        configFile->setValue("name", "prevChannel");
        configFile->setValue("shortcut", "Ctrl+Left");
        configFile->setValue("modifier", "67108864");
        configFile->setValue("key", "16777234");
        configFile->setArrayIndex(4);
        configFile->setValue("name", "fullscreen");
        configFile->setValue("shortcut", "Ctrl+Enter");
        configFile->setValue("modifier", "603979776");
        configFile->setValue("key", "16777221");
        configFile->endArray();

        configFile->setValue("Gestures/inverted", "");
        configFile->setValue("Gestures/moveThreshold", "200");

        configFile->setValue("Alert/disabled", "");
        configFile->setValue("Alert/onlyOne", "");

        configFile->beginWriteArray("Alerts");
        configFile->setArrayIndex(0);
        configFile->setValue("state", "1. Window active, screen off");
        configFile->setValue("notification", "Interaction required");
        configFile->setValue("vibration", "true");
        configFile->setValue("led", "true");
        configFile->setValue("sound", "true");
        configFile->setValue("soundPath", "/usr/share/sounds/Emailalert1.aac");
        configFile->setValue("soundVolume", "0.3");
        configFile->setArrayIndex(1);
        configFile->setValue("state", "2. Window deactive, screen off");
        configFile->setValue("notification", "Interaction required");
        configFile->setValue("vibration", "true");
        configFile->setValue("led", "true");
        configFile->setValue("sound", "true");
        configFile->setValue("soundPath", "/usr/share/sounds/Emailalert1.aac");
        configFile->setValue("soundVolume", "0.3");
        configFile->setArrayIndex(2);
        configFile->setValue("state", "3. Window closed, screen off");
        configFile->setValue("notification", "Email style");
        configFile->setValue("vibration", "true");
        configFile->setValue("led", "true");
        configFile->setValue("sound", "true");
        configFile->setValue("soundPath", "/usr/share/sounds/Emailalert1.aac");
        configFile->setValue("soundVolume", "0.3");
        configFile->setArrayIndex(3);
        configFile->setValue("state", "4. Window active, screen on");
        configFile->setValue("notification", "No interaction required");
        configFile->setValue("vibration", "");
        configFile->setValue("led", "");
        configFile->setValue("sound", "true");
        configFile->setValue("soundPath", "/usr/share/sounds/chat-msg_in_fg.wav");
        configFile->setValue("soundVolume", "0.3");
        configFile->setArrayIndex(4);
        configFile->setValue("state", "5. Window deactive, screen on");
        configFile->setValue("notification", "No interaction required");
        configFile->setValue("vibration", "");
        configFile->setValue("led", "");
        configFile->setValue("sound", "true");
        configFile->setValue("soundPath", "/usr/share/sounds/chat-start_new.wav");
        configFile->setValue("soundVolume", "0.3");
        configFile->setArrayIndex(5);
        configFile->setValue("state", "6. Window closed, screen on");
        configFile->setValue("notification", "Email style");
        configFile->setValue("vibration", "");
        configFile->setValue("led", "");
        configFile->setValue("sound", "true");
        configFile->setValue("soundPath", "/usr/share/sounds/chat-start_new.wav");
        configFile->setValue("soundVolume", "0.3");
        configFile->setArrayIndex(6);
        configFile->setValue("state", "7. Same as 4th but, alert from currently open chat");
        configFile->setValue("notification", "No interaction required");
        configFile->setValue("vibration", "");
        configFile->setValue("led", "");
        configFile->setValue("sound", "true");
        configFile->setValue("soundPath", "/usr/share/sounds/chat-msg_in_fg.wav");
        configFile->setValue("soundVolume", "0.3");
        configFile->endArray();

        configFile->setValue("Animations/enable", "true");
        configFile->setValue("Animations/duration", "400");
    }

    QStringList configList = QStringList() << configFile->value("User/nick").toString()
                                           << configFile->value("User/alter").toString()
                                           << configFile->value("User/name").toString();
    config.insert("User", configList);

    configList = QStringList() << configFile->value("Settings/doNotQuit").toString()
                               << configFile->value("Settings/language").toString()
                               << configFile->value("Settings/nickCompeltionSuffix").toString()
                               << configFile->value("Settings/highlights").toString()
                               << configFile->value("Settings/leaveMsg").toString()
                               << configFile->value("Settings/quitMsg").toString();

    config.insert("Settings", configList);
    highlightList = config.value("Settings").at(3).split(',');

    configList = QStringList() << configFile->value("Chatbox/font").toString()
                               << configFile->value("Chatbox/fontSize").toString();

    config.insert("Chatbox", configList);

    int size = configFile->beginReadArray("ChatboxColors");

    for (int i = 0; i < size; ++i) {
        QString color;
        configFile->setArrayIndex(i);
        color = configFile->value("color").toString();
        colorList.insert(configFile->value("name").toString(), color);
    }
    configFile->endArray();

    size = configFile->beginReadArray("Shortcuts");

    for (int i = 0; i < size; ++i) {
        QStringList shortcut;
        configFile->setArrayIndex(i);
        shortcut.append(configFile->value("shortcut").toString());
        shortcut.append(configFile->value("modifier").toString());
        shortcut.append(configFile->value("key").toString());
        shortcutList.insert(configFile->value("name").toString(), shortcut);
    }
    configFile->endArray();

    configList = QStringList() << configFile->value("Gestures/inverted").toString()
                               << configFile->value("Gestures/moveThreshold").toString();

    config.insert("Gestures", configList);

    alertSettings.append(configFile->value("Alert/disabled").toString() != "");
    alertSettings.append(configFile->value("Alert/onlyOne").toString() != "");

    size = configFile->beginReadArray("Alerts");

    for (int i = 0; i < size; ++i) {
        QStringList alert;
        configFile->setArrayIndex(i);
        alert.append(configFile->value("notification").toString());
        alert.append(configFile->value("vibration").toString());
        alert.append(configFile->value("led").toString());
        alert.append(configFile->value("sound").toString());
        alert.append(configFile->value("soundPath").toString());
        alert.append(configFile->value("soundVolume").toString());
        alertList.insert(configFile->value("state").toString(), alert);
    }
    configFile->endArray();

    configList = QStringList() << configFile->value("Animations/enable").toString()
                               << configFile->value("Animations/duration").toString();

    config.insert("Animations", configList);

    delete configFile;
}

/**
 * Reads network list from file.
 */
void Config::readNetworkList()
{
    QSettings *networkListFile = new QSettings("irggu", "irggu");
    int size = networkListFile->beginReadArray("Networks");

    for (int i = 0; i < size; ++i) {
        QStringList network;
        networkListFile->setArrayIndex(i);
        network.append(networkListFile->value("servers").toString());
        network.append(networkListFile->value("password").toString());
        network.append(networkListFile->value("autoconnect").toString());
        network.append(networkListFile->value("command").toString());
        network.append(networkListFile->value("useSsl").toString());
        network.append(networkListFile->value("ignoreSslErrors").toString());
        network.append(networkListFile->value("inCharSet").toString());
        network.append(networkListFile->value("outCharSet").toString());
        network.append(networkListFile->value("userInfoGlobal").toString());
        network.append(networkListFile->value("nickNetwork").toString());
        network.append(networkListFile->value("alterNetwork").toString());
        network.append(networkListFile->value("nameNetwork").toString());
        network.append(networkListFile->value("ignoreNick").toString());
        network.append(networkListFile->value("ignorePrivateChat").toString());
        network.append(networkListFile->value("ignoreAlerts").toString());
        networkList.insert(networkListFile->value("network").toString(), network);
    }
    networkListFile->endArray();
    delete networkListFile;
}

/**
 * Reads all available charsets.
 */
void Config::readCharSets()
{
    QList<QByteArray> codecs;
    codecs = QTextCodec::availableCodecs();
    QListIterator<QByteArray> codecsIt(codecs);
    QString charSet;
    bool latin1 = false;
    bool utf8 = false;

    while (codecsIt.hasNext()) {
        charSet = codecsIt.next();
        charSetIn.append(charSet);
        charSetOut.append(charSet);

        if (charSet.toLower() == "latin1")
            latin1 = true;

        if (charSet.toLower() == "utf-8")
            utf8 = true;
    }
    charSetIn.sort();
    charSetOut.sort();

    if (latin1 && utf8)
        charSetIn.prepend("latin1/UTF-8");
}
