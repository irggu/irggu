<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fi">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../GUI/mainwindow.ui" line="14"/>
        <location filename="../GUI/mainwindow.ui" line="298"/>
        <source>IrGGu</source>
        <translation type="unfinished">IrGGu</translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="189"/>
        <source>Query</source>
        <translation type="unfinished">Keskustele</translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="199"/>
        <source>Close chat</source>
        <translation type="unfinished">Sulje chatti</translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="220"/>
        <source>Close network</source>
        <translation type="unfinished">Sulje verkko</translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="257"/>
        <source>Tab</source>
        <translation type="unfinished">Tab</translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="308"/>
        <source>Network List</source>
        <translation type="unfinished">Verkkolista</translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="313"/>
        <source>Options</source>
        <translation type="unfinished">Asetukset</translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="318"/>
        <source>Quit</source>
        <translation type="unfinished">Lopeta</translation>
    </message>
</context>
<context>
    <name>NetworkListDialog</name>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="17"/>
        <source>Network List</source>
        <translation type="unfinished">Verkkolist</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="74"/>
        <location filename="../GUI/networklistdialog.ui" line="646"/>
        <source>Nick:</source>
        <translation type="unfinished">Nimimerkki:</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="104"/>
        <location filename="../GUI/networklistdialog.ui" line="679"/>
        <source>Alternative:</source>
        <translation type="unfinished">Vaihtoehto:</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="134"/>
        <location filename="../GUI/networklistdialog.ui" line="712"/>
        <source>Name:</source>
        <translation type="unfinished">Nimi:</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="164"/>
        <source>Networks:</source>
        <translation type="unfinished">Verkot:</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="200"/>
        <source>Close</source>
        <translation type="unfinished">Sulje</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="215"/>
        <location filename="../GUI/networklistdialog.ui" line="348"/>
        <location filename="../GUI/networklistdialog.ui" line="803"/>
        <source>Add</source>
        <translation type="unfinished">Lisää</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="233"/>
        <location filename="../GUI/networklistdialog.ui" line="366"/>
        <location filename="../GUI/networklistdialog.ui" line="821"/>
        <source>Remove</source>
        <translation type="unfinished">Poista</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="251"/>
        <source>Apply</source>
        <translation type="unfinished">Hyväksy</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="283"/>
        <source>Basic</source>
        <translation type="unfinished">Perus</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="300"/>
        <source>Network:</source>
        <translation type="unfinished">Verkko:</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="381"/>
        <source>Servers:</source>
        <translation type="unfinished">Palvelimet:</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="396"/>
        <source>Connect to this network on startup</source>
        <translation type="unfinished">Yhdistä tähän verkkoo käynnistyksessä</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="420"/>
        <source>Commands</source>
        <translation type="unfinished">Komennot</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="434"/>
        <source>Commands on connect</source>
        <translation type="unfinished">Komennot yhdistätteässä</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="459"/>
        <source>Advanced</source>
        <translation type="unfinished">Vaativat</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="470"/>
        <source>Password:</source>
        <translation type="unfinished">Salasana:</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="506"/>
        <source>Character set for receiving:</source>
        <translation type="unfinished">Merkistö vastaanotolle:</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="530"/>
        <source>Character set for sending:</source>
        <translation type="unfinished">Merkistö lähettämiselle:</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="554"/>
        <source>Use SSL</source>
        <translation type="unfinished">Käytä SSL</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="569"/>
        <source>Ignore SSL errors</source>
        <translation type="unfinished">Ohita SSL virheet</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="580"/>
        <source>User</source>
        <translation type="unfinished">Käyttäjä</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="591"/>
        <source>Use global settings</source>
        <translation type="unfinished">Käytä globaali asetuksia</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="741"/>
        <source>Ignore</source>
        <translation type="unfinished">Suodatus</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="765"/>
        <source>Nick</source>
        <translation type="unfinished">Nimimerkki</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="775"/>
        <source>Private Chat</source>
        <translation type="unfinished">Privaatti chatti</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="785"/>
        <source>Alerts</source>
        <translation type="unfinished">Hälytykset</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.ui" line="843"/>
        <source>Connect</source>
        <translation type="unfinished">Yhdistä</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="252"/>
        <source>You didn&apos;t input network name!</source>
        <translation type="unfinished">Et syöttänyt verkon nimeä!</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="255"/>
        <location filename="../GUI/networklistdialog.cpp" line="262"/>
        <source>There is network </source>
        <translation type="unfinished">Verkko </translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="255"/>
        <location filename="../GUI/networklistdialog.cpp" line="262"/>
        <source> already!</source>
        <translation type="unfinished">on jo olemassa!</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="266"/>
        <source>You didn&apos;t add any servers!</source>
        <translation type="unfinished">Et lisännyt yhtään palvelinta!</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="270"/>
        <source>You didn&apos;t input network specific nick!</source>
        <translation type="unfinished">Et syöttänyt verkko kohtaista nimimerkkiä!</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="273"/>
        <source>You didn&apos;t input network specific alternative nick!</source>
        <translation type="unfinished">Et syöttänyt verkko kohtaista nimimerkki vaihtoehtoa!</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="276"/>
        <source>You didn&apos;t input network specific real name!</source>
        <translation type="unfinished">Et syöttänyt verkko kohtaista nimeä!</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="337"/>
        <source>Cannot add network!</source>
        <translation type="unfinished">Ei voida lisätä verkkoa!</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="338"/>
        <source>Cannot apply settings!</source>
        <translation type="unfinished">EI voida hyväksyä asetuksia!</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.cpp" line="372"/>
        <source>NewServer/6667</source>
        <translation type="unfinished">UusiPalvelin/6667</translation>
    </message>
    <message>
        <location filename="../GUI/networklistdialog.h" line="78"/>
        <source>nick</source>
        <translation type="unfinished">nimimerkki</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="17"/>
        <source>Options</source>
        <translation type="unfinished">Asetukset</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="44"/>
        <source>Settings</source>
        <translation type="unfinished">Asetukset</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="55"/>
        <source>Nick Completion suffix:</source>
        <translation type="unfinished">Nimimerkin täydennys pääte:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="85"/>
        <source>Extra hightlights (separate with comma):</source>
        <translation type="unfinished">Ylimääräiset korostukset(eroita pilkulla):</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="115"/>
        <source>Do not quit application when mainwindow is closed</source>
        <translation type="unfinished">Älä lopeta ohjelmaa, kun pääikkuna on suljettu</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="130"/>
        <source>Leave message:</source>
        <translation type="unfinished">Poistumis viesti:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="160"/>
        <source>Quit message:</source>
        <translation type="unfinished">Lopetus viesti:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="186"/>
        <source>System</source>
        <translation type="unfinished">Järjestelmä</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="191"/>
        <source>English</source>
        <translation type="unfinished">Englanti</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="196"/>
        <source>Finnish</source>
        <translation type="unfinished">Suomi</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="209"/>
        <source>Language:</source>
        <translation type="unfinished">Kieli:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="223"/>
        <source>Font &amp;&amp; colors</source>
        <translation type="unfinished">Fontti &amp;&amp; värit</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="234"/>
        <source>Color of sent messages:</source>
        <translation type="unfinished">Lähetettyjen viestejen väri:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="259"/>
        <location filename="../GUI/optionsdialog.ui" line="296"/>
        <location filename="../GUI/optionsdialog.ui" line="333"/>
        <location filename="../GUI/optionsdialog.ui" line="370"/>
        <location filename="../GUI/optionsdialog.ui" line="407"/>
        <source>Colormap</source>
        <translation type="unfinished">Värikartta</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="271"/>
        <source>Color of incomming messages:</source>
        <translation type="unfinished">Vastaanotettujen viestejen väri:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="308"/>
        <source>Color of highlighted messages:</source>
        <translation type="unfinished">Korostettujen viestejen väri:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="345"/>
        <source>Color of any other text:</source>
        <translation type="unfinished">Kaiken muun tekstin väri:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="382"/>
        <source>Color of chatbox background:</source>
        <translation type="unfinished">Chattilaatikon taustaväri:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="419"/>
        <source>Font:</source>
        <translation type="unfinished">Fontti:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="437"/>
        <source>Font size:</source>
        <translation type="unfinished">Fontti koko:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="566"/>
        <source>Shortcuts</source>
        <translation type="unfinished">Pikanäppäimet</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="577"/>
        <source>Shortcut must have at least one modifier(Shift, Ctrl etc) and only one non-modifier key. (example Ctrl+a)</source>
        <translation type="unfinished">Pikanäppäin täytyy olla vähintää yksi muunnosnäppäin(Shift jne) ja yksi ei muunnosnäppäin. (esim. Ctrl+a)</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="589"/>
        <source>Next Network:</source>
        <translation type="unfinished">Seuraava verkko:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="619"/>
        <source>Prev Network:</source>
        <translation type="unfinished">Edellinen verkko:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="649"/>
        <source>Fullscreen:</source>
        <translation type="unfinished">Kokoruutu:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="679"/>
        <source>Next Channel:</source>
        <translation type="unfinished">Seuraava kanava:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="709"/>
        <source>Prev Channel:</source>
        <translation type="unfinished">Edellinen kanava:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="735"/>
        <source>Gestures</source>
        <translation type="unfinished">Eleet</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="746"/>
        <source>Inverted gestures</source>
        <translation type="unfinished">Vastakkaiset eleet</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="761"/>
        <source>MoveThreshold(pixels):</source>
        <translation type="unfinished">Siirtämis kynnys(pikseleinä):</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="797"/>
        <source>Alerts</source>
        <translation type="unfinished">Hälytykset</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="808"/>
        <source>Disable alerts</source>
        <translation type="unfinished">Ota hälytykset pois käytöstä</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="823"/>
        <source>State:</source>
        <translation type="unfinished">Tila:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="834"/>
        <source>1. Window active, screen off</source>
        <translation type="unfinished">1. Ikkuna aktiivinen, näyttö pois päältä</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="839"/>
        <source>2. Window deactive, screen off</source>
        <translation type="unfinished">2. Ikkuna ei ole aktiivinen, näyttö pois päältä</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="844"/>
        <source>3. Window closed, screen off</source>
        <translation type="unfinished">3. Ikkuna suljettu, näyttö pois päältä</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="849"/>
        <source>4. Window active, screen on</source>
        <translation type="unfinished">4. Ikkuna aktiivinen, näyttö päällä</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="854"/>
        <source>5. Window deactive, screen on</source>
        <translation type="unfinished">5. Ikkuna ei ole aktiivinen, näyttö päällä</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="859"/>
        <source>6. Window closed, screen on</source>
        <translation type="unfinished">6. Ikkuna suljettu, näyttö päällä</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="864"/>
        <source>7. Same as 4th but, alert from currently open chat</source>
        <translation type="unfinished">7. Sama kuin neljäs, mutta näkyvillä olevasta chatista</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="877"/>
        <source>Notification:</source>
        <translation type="unfinished">Ilmoitus:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="888"/>
        <source>None</source>
        <translation type="unfinished">Ei mitään</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="893"/>
        <source>Email style</source>
        <translation type="unfinished">Email tyylinen</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="898"/>
        <source>Interaction required</source>
        <translation type="unfinished">Vuorovaikutusta vaativa</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="903"/>
        <source>No interaction required</source>
        <translation type="unfinished">Vuorovaikutusta ei vaativa</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="916"/>
        <source>One notfication</source>
        <translation type="unfinished">Yksi ilmoitus</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="931"/>
        <source>Browse</source>
        <translation type="unfinished">Selaa</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="946"/>
        <source>Sound:</source>
        <translation type="unfinished">Ääni:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="961"/>
        <source>Led</source>
        <translation type="unfinished">Ledi</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="976"/>
        <source>Vibration</source>
        <translation type="unfinished">Värinä</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="1003"/>
        <source>Sound Volume:</source>
        <translation type="unfinished">Äänenvoimmakkuus:</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="1018"/>
        <source>Sound</source>
        <translation type="unfinished">Ääni</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="1047"/>
        <source>Animation</source>
        <translation type="unfinished">Animaatio</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="1058"/>
        <source>Enable animation</source>
        <translation type="unfinished">Käytä animaatiota</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="1073"/>
        <source>Duration(msec):</source>
        <translation type="unfinished">Kesto(msec):</translation>
    </message>
    <message>
        <location filename="../GUI/optionsdialog.ui" line="1117"/>
        <source>Close</source>
        <translation type="unfinished">Sulje</translation>
    </message>
</context>
</TS>
