/****************************************************************************
**  channel.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef CHANNEL_H
#define CHANNEL_H

/**
 *  This class is for managing channels, private chats and server messages.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-04-02
 */

#include "alert/alert.h"
#include <QObject>
#include <QMap>
#include <QStringList>

class Channel: public QObject
{
    Q_OBJECT
public:
    Channel(QString *nick, QString name, int type, QObject* parent = 0);

    void        handlePrivMsg(QString sender, QString line, bool notInChat = false,
                              bool current = false, bool maybeHighlight = false,
                              bool alert = false);
    void        handleOwnMsg(QString nick, QString msg);
    void        handleOwnAction(QString nick, QString msg);
    void        handleServerMsg(QString line);
    void        handleOtherMsg(QString line, bool notInChat = true);
    void        addNames(QStringList names);
    void        addName(QString name);
    void        deleteName(QString name);
    void        giveOp(QString user);
    void        takeOp(QString user);
    void        giveVoice(QString user);
    void        takeVoice(QString user);
    void        reset();
    QString     getName();
    QString     getChat();
    QString     nickCompletion(QString line);
    QStringList getNames();
    bool        changeName(QString oldName, QString newName);
    bool        hasName(QString name);

private:
    QString                       name;
    QString                       *nick;
    QString                       chat;
    QMap<QString, QStringList *>  namesList;
    QMap<QString, QString>        *colorsList;
    QStringList                   nickCompletionList;
    QStringList                   *hightlights;
    QList<bool>                   *alertSettings;
    int                           type;
    Alert                         *alert;

    void checkRowCount();
    void createAlert(QString summary, QString body, bool current);

private Q_SLOTS:
    void notificationClosed(bool clicked);

Q_SIGNALS:
    void newLine(QString);
    void notificationClicked(QString);
    void namesChanged(QStringList);
};

#endif // CHANNEL_H
