/****************************************************************************
**  connection.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "connection.h"
#include "linehandler.h"
#include "config.h"
#include "other/misc.h"

/**
 * Constructor.
 *
 * @param network         Network name.
 * @param addresses       Server addresses.
 * @param nick            Nick.
 * @param alter           Alternative nick.
 * @param name            Real name.
 * @param pass            Server password.
 * @param commands        Commands after connected.
 * @param useSsl          Use SSL.
 * @param ignoreSslErrors Ignore SSL errors.
 * @param inCharSet       Charset for sending.
 * @param outCharSet      Charset for receiving.
 * @param ignoreList      Ignore list.
 * @param *parent         Pointer to parent.
 */
Connection::Connection(QString network, QStringList addresses, QString nick, QString alter,
                       QString name, QString pass, QStringList commands, bool useSsl,
                       bool ignoreSslErrors, QString inCharSet, QString outCharSet,
                       QMap<QString, QList<bool> > ignoreList, QObject *parent) : QObject(parent)
{
    this->address           = "";
    this->addresses         = addresses;
    this->nick              = nick;
    this->alter             = alter;
    this->name              = name;
    this->network           = network;
    this->pass              = pass;
    this->commands          = commands;
    this->useSsl            = useSsl;
    this->ignoreSslErrors   = ignoreSslErrors;
    this->inCharSet         = inCharSet;
    this->outCharSet        = outCharSet;
    this->ignoreList        = ignoreList;
    this->useAlter          = false;
    this->nickChecked       = false;
    this->pingTimer.setInterval(180000);

    this->channels.insert("(server)", new Channel(&nick, "(server)", 0, this));
    this->currentChannel = channels.value("(server)");
    this->current        = true;
    channelChanged();
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName(outCharSet.toLocal8Bit()));

    this->connect(&tcpSocket, SIGNAL(sslErrors(const QList<QSslError> &)),this,
                        SLOT(displaySslErrors(const QList<QSslError> &)));
    this->connect(&tcpSocket, SIGNAL(connected()), this, SLOT(connected()));
    this->connect(&tcpSocket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    this->connect(&tcpSocket, SIGNAL(connected()), this, SLOT(setUser()));
    this->connect(&tcpSocket, SIGNAL(readyRead()), this, SLOT(startRead()));
    this->connect(currentChannel, SIGNAL(newLine(QString)), this, SLOT(newLine(QString)));
    this->connect(&pingTimer, SIGNAL(timeout()), this, SLOT(ping()));
}

/**
 * Destructor.
 */
Connection::~Connection()
{
    QString quitMsg = Config::getConfig()->value("Settings").at(5);
    QString quit = "QUIT :" + quitMsg + "\r\n";
    tcpSocket.write(quit.toAscii());
    tcpSocket.waitForBytesWritten(1500);
    tcpSocket.close();
}

/**
 * Start connecting to server.
 */
void Connection::start()
{
    if (Misc::hasConnection()) {
        if (address == "")
            address = addresses.at(0);
        else if ((addresses.indexOf(address)+1) < addresses.count())
            address = addresses.at(addresses.indexOf(address)+1);
        else
            address = addresses.at(0);

        channels.value("(server)")->handleOtherMsg("Connecting to " + address, false);

        if (useSsl)
            tcpSocket.connectToHostEncrypted(address.section('/', 0, 0),
                                             address.section('/', 1, 1).toInt());
        else
            tcpSocket.connectToHost(address.section('/', 0, 0),
                                    address.section('/', 1, 1).toInt());
    } else {
        channels.value("(server)")->handleOtherMsg("No network connection!", false);
    }

    QTimer::singleShot(5000, this, SLOT(checkConnection()));
}

/**
 * Change channel.
 *
 * @param channel Channel.
 */
void Connection::changeChannel(QString channel)
{
    if (channels.find(channel) != channels.end()) {
        disconnectSignals();

        currentChannel = channels.value(channel);

        connectSignals();
        channelChanged();
    }
}

/**
 * Set Channel to current channel or not current channel.
 *
 * @param setCurrent set channel to current channel.
 */
void Connection::setCurrent(bool setCurrent)
{
    if (setCurrent) {
        current = true;
        channelChanged();
    }
    else {
        current = false;
    }
}

/**
 * Call linehandler with line that needs to be written.
 *
 * @param msg Line that need to be written.
 */
void Connection::write(QString msg)
{
    LineHandler::handleOut(msg, this);
}

/**
 * Emit signals to notify of added or removed channels or channel change.
 *
 * @param added   Added channel.
 * @param removed Removed channel.
 * @param channel Channel.
 */
void Connection::channelChanged(bool added, bool removed, QString channel)
{
    if (current) {
        if (added) {
            channel = currentChannel->getName();
            Q_EMIT channelAdded(channel);
        } else if(removed) {
            Q_EMIT channelRemoved(channel);
        } else {
            channel = currentChannel->getName();
            Q_EMIT chatTextChanged(currentChannel->getChat());
            Q_EMIT ncInfoLabelChanged(network + " " + currentChannel->getName());
            Q_EMIT namesChanged(currentChannel->getNames());
        }
    }
}

/**
 * Set nick.
 *
 * @param nick Nick.
 */
void Connection::setNick(QString nick)
{
    this->nick = nick;
}

/**
 * Connect signals to current channel.
 *
 * @param newChannel Is current channel new channel.
 */
void Connection::connectSignals(bool newChannel)
{
    connect(currentChannel, SIGNAL(newLine(QString)), this, SLOT(newLine(QString)));;
    connect(currentChannel, SIGNAL(namesChanged(QStringList)), this,
            SLOT(namesListChanged(QStringList)));

    if(newChannel) {
        connect(currentChannel, SIGNAL(notificationClicked(QString)), this,
                SLOT(notificationClicked(QString)));
    }
}

/**
 * Diconnect signals from the current channel.
 */
void Connection::disconnectSignals()
{
    disconnect(currentChannel, SIGNAL(newLine(QString)), this, SLOT(newLine(QString)));
    disconnect(currentChannel, SIGNAL(namesChanged(QStringList)),
               this, SLOT(namesListChanged(QStringList)));
}

/**
 * Emit signals to start new whois information or append whois information.
 */
void Connection::whois(QString line, bool isNewWhois)
{
    if (isNewWhois)
        Q_EMIT newWhois(line);
    else
        Q_EMIT appendWhois(line);
}

/**
 * Returns TCP socket.
 *
 * @return TCP socket.
 */
QSslSocket *Connection::getTcpSocket()
{
    return &tcpSocket;
}

/**
 * Returns network name.
 *
 * @return Network name.
 */
QString Connection::getNetwork()
{
    return network;
}

/**
 * Returns nick.
 *
 * @return Nick.
 */
QString *Connection::getNick()
{
    return &nick;
}

/**
 * Returns commands that are sented after connected.
 *
 * @return Commands.
 */
QStringList Connection::getCommands()
{
    return commands;
}

/**
 * Returns channel name list.
 *
 * @return Channel name list.
 */
QStringList Connection::getChannelList()
{
    return channels.keys();
}

/**
 * Returns channels.
 *
 * @return Channels.
 */
QMap<QString, Channel *> *Connection::getChannels()
{
    return &channels;
}

/**
 * Returns ignore list.
 *
 * @return Ignore list.
 */
QMap<QString, QList<bool> > *Connection::getIgnoreList()
{
    return &ignoreList;
}

/**
 * Returns pointer to current channel.
 *
 * @return Pointer to current channel.
 */
Channel *Connection::getCurrentChannel()
{
    return currentChannel;
}

/**
 * Returns pointer to pointer of current channel.
 *
 * @return Pointer to pointer of current channel.
 */
Channel **Connection::getCurrentChannelPointer()
{
    return &currentChannel;
}

/**
 * Returns is alternative nick used.
 *
 * @return Is alternative nick used.
 */
bool Connection::getUseAlter()
{
    return useAlter;
}

/**
 * Send password to server and set user nick and real name.
 *
 * @param alter Use alternative nick.
 */
void Connection::setUser(bool alter)
{
    if (alter) {
        this->nick     = this->alter;
        this->useAlter = true;
    }

    QString setUser = "PASS " + pass + "\r\nNICK " + nick + "\r\nUSER " + nick + " 8 * :"
                      + name + "\r\n";

    tcpSocket.write(setUser.toAscii());
}

/**
 * Check connection, if not connected try next server.
 */
void Connection::checkConnection()
{
    if (Misc::hasConnection() && tcpSocket.state() != QAbstractSocket::ConnectedState) {
        channels.value("(server)")->handleOtherMsg("Unable to connect to " + address, false);
        start();
    } else if (!Misc::hasConnection()) {
        start();
    }
}

/**
 * Start pingtimer when connected.
 */
void Connection::connected()
{
    channels.value("(server)")->handleOtherMsg("Connected to " + address, false);
    pingTimer.start();
    noData = false;
}

/**
 * Add join commands to join channels where user is, so the client will join to channels
 * when reconnected.
 */
void Connection::disconnected()
{
    QMapIterator<QString, Channel *> channelsIt(channels);
    QString channel;

    while (channelsIt.hasNext()) {
        channelsIt.next();
        channel = channelsIt.key();

        if (channel != "(server)") {
            if (channel.at(0) == '&' || channel.at(0) == '#' || channel.at(0) == '+'
                || channel.at(0) == '!') {
                commands.append("/JOIN " + channel);
            }
            channelsIt.value()->handleOtherMsg("Disconnected from the server", false);
        }
    }

    start();
}

/**
 * Display SSl errors.
 *
 * @param &errors Reference to SSL errors.
 */
void Connection::displaySslErrors(const QList<QSslError> &errors)
{
    QListIterator<QSslError> errorsIt(errors);

    while (errorsIt.hasNext()) {
        channels.value("(server)")->handleOtherMsg(errorsIt.next().errorString(), false);
    }

    if (ignoreSslErrors)
    {
        tcpSocket.ignoreSslErrors();
        channels.value("(server)")->handleOtherMsg("SSL errors ignored", false);
    }
}

/**
 * Start reading when there is data to read from the TPC socket.
 */
void Connection::startRead()
{
    pingTimer.start();
    noData = false;
    QString line;
    QTextCodec *codec;
    QByteArray buffer;

    if (inCharSet == "latin1/UTF-8") {
        codec = QTextCodec::codecForName("UTF-8");
    } else {
        codec = QTextCodec::codecForName(inCharSet.toLocal8Bit());
    }

    while (tcpSocket.canReadLine()) {
        buffer = tcpSocket.readLine();
        line = QString::fromUtf8(buffer);

        if (inCharSet == "latin1/UTF-8") {
            line = QString::fromUtf8(buffer);

            if(buffer != codec->fromUnicode(line)) {
                line = QString::fromLatin1(buffer);
            }
        } else {
            line = codec->toUnicode(buffer);
        }

        LineHandler::handleIn(line, this, current);
    }
}

/**
 * Emit chatTextAppend when there is line to append.
 */
void Connection::newLine(QString line)
{
    Q_EMIT chatTextAppend(line);
}

/**
 * Emit changeNetwork when notification was clicked.
 */
void Connection::notificationClicked(QString channel)
{
    changeChannel(channel);
    Q_EMIT changeNetwork(network);
}

/**
 * Emit namesChanged when names has changed.
 */
void Connection::namesListChanged(QStringList names)
{
    Q_EMIT namesChanged(names);
}

/**
 * Ping server when timer has timeout and if there is no reply disconnect.
 */
void Connection::ping()
{
    if (noData) {
        tcpSocket.disconnectFromHost();
        pingTimer.stop();
        noData = false;
    } else {
        QString time = QString::number(QDateTime::currentMSecsSinceEpoch());
        QString ping = "PING " + time + "\r\n";
        tcpSocket.write(ping.toAscii());
        pingTimer.start();
        noData = true;
    }
}
