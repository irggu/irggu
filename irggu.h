/****************************************************************************
**  irggu.h
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef IRGGU_H
#define IRGGU_H

/**
 *  This class is for managing connections and GUI.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-03-31
 */

#include "connection.h"
#include "GUI/mainwindow.h"
#include "dbus/dbusreceiver.h"
#include <QObject>
#include <QTranslator>

class IrGGu : public QObject
{
    Q_OBJECT
public:
    explicit IrGGu(QObject *parent = 0);

private:
    MainWindow* w;
    QMap<QString, Connection *> connections;
    Connection                  *currentConnection;
    DbusReceiver                *dbusReceiver;
    QTranslator                 *t;

    void connectStartUp();
    void connectSignals();
    void disconnectSignals();

public Q_SLOTS:
    void startGUI();
    void destroyGUI();

private Q_SLOTS:
    void loadTranslation();
    void connectNetwork(QString network, QString addresses, QString nick, QString alter,
                        QString name, QString pass, QString commands, QString useSsl,
                        QString ignoreSslErrors, QString inCharSet, QString outCharSet,
                        QString ignoreNick, QString ignorePrivateChat, QString ignoreAlerts,
                        bool signalsToSlots = true);
    void write(QString line);
    void nickCompletion(QString line);
    void changeNetwork(QString network);
    void changeChannel(QString channel);
    void closeNetwork();

Q_SIGNALS:
    void networkAdded(QString);
};

#endif // IRGGU_H
