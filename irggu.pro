# Add files and directories to ship with the application 
# by adapting the examples below.
# file1.source = myfile
# dir1.source = mydir
DEPLOYMENTFOLDERS   = translations styles
translations.source = translations
styles.source       = styles

symbian:TARGET.UID3 = 0xE32A8EAD

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# If your application uses the Qt Mobility libraries, uncomment
# the following lines and add the respective components to the 
# MOBILITY variable. 
# CONFIG += mobility
# MOBILITY +=

QT += network \
    dbus \
    phonon
TARGET = irggu
TEMPLATE = app
SOURCES += main.cpp \
    connection.cpp \
    channel.cpp \
    config.cpp \
    linehandler.cpp \
    customgestures.cpp \
    irggu.cpp \
    alert/alertmanager.cpp \
    alert/alert.cpp \
    GUI/mainwindow.cpp \
    GUI/networklistdialog.cpp \
    GUI/itemdelegate.cpp \
    GUI/optionsdialog.cpp \
    GUI/lineedit.cpp \
    GUI/scrollarea.cpp \
    dbus/irgguadaptor.cpp \
    dbus/dbusreceiver.cpp \
    other/currentsong.cpp \
    other/misc.cpp \
    dbus/dbushandlerdata.cpp \
    other/sound.cpp \
    GUI/listwidget.cpp \
    application.cpp
HEADERS += connection.h \
    channel.h \
    config.h \
    linehandler.h \
    customgestures.h \
    irggu.h \
    alert/alertmanager.h \
    alert/alert.h \
    GUI/mainwindow.h \
    GUI/networklistdialog.h \
    GUI/itemdelegate.h \
    GUI/optionsdialog.h \
    GUI/lineedit.h \
    GUI/scrollarea.h \
    dbus/dbushandler.h \
    dbus/irgguadaptor.h \
    dbus/dbusreceiver.h \
    other/currentsong.h \
    other/misc.h \
    dbus/dbushandlerdata.h \
    other/sound.h \
    GUI/listwidget.h \
    application.h
FORMS += GUI/mainwindow.ui \
    GUI/networklistdialog.ui \
    GUI/optionsdialog.ui
UI_DIR = "GUI/"

OTHER_FILES += \
    todo.txt \
    irggu.png.copyright

TRANSLATIONS = translations_ts/english.ts \
    translations_ts/finnish.ts

# Please do not modify the following two lines. Required for deployment.
include(deployment.pri)
qtcAddDeployment()
