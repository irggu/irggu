/****************************************************************************
**  application.cpp
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "application.h"
#include <QX11Info>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

/**
 * Constructor.
 *
 * @param &argc  Reference to argument count.
 * @param **argv Pointer to Pointer of argument values.
 */
Application::Application(int &argc, char **argv) : QApplication(argc, argv)
{
}

/**
 * Maemo virtualkeyboard does not send return/enter press this is fix for that.
 *
 * @param *w Pointer to widget that receives the event.
 * @param *e Pointer to event.
 * @param passive_only.
 * @return How the event was handled.
 */
int Application::x11ClientMessage(QWidget *w, XEvent *e, bool passive_only)
{
    #ifdef Q_WS_MAEMO_5
    /**
    * After testing it seems that virtualkeyboard sends clear sticky keys only when enter/return
    * is pressed, so when received clear sticky keys we make copy of XEvent and change the key to
    * enter.
    * 20 = Clear Sticky keys
    * 0  = Enter/Return
    */
    if (e->xclient.message_type == XInternAtom(QX11Info::display(), "_HILDON_IM_COM", 0)
        && e->xclient.data.s[2] == 20) {
        XEvent event = *e;
        event.xclient.data.s[2] = 0;
        QApplication::x11ClientMessage(w, &event, passive_only);
    }
    #endif
    return QApplication::x11ClientMessage(w, e, passive_only);
}
