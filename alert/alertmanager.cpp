/****************************************************************************
**  alertmanager.cpp
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "config.h"
#include "dbus/dbushandler.h"
#include "alert/alertmanager.h"
#include "other/misc.h"
#include <QDebug>

QMap<QString, Notifications> AlertManager::notificationStringToEnumMap;
QMap<QString, QStringList>   *AlertManager::alertList     = Config::getAlertList();
QList<bool>                  *AlertManager::alertSettings = Config::getAlertSettings();
QList<Alert *>               AlertManager::alerts;
QString                      AlertManager::service        = "com.nokia.mce";
QString                      AlertManager::path           = "/com/nokia/mce/request";
QString                      AlertManager::interface      = "com.nokia.mce.request";
QString                      AlertManager::enableVibrator = "req_vibrator_enable";

/**
 * Set all needed variables.
 */
void AlertManager::set()
{
    notificationStringToEnumMap.insert("Email style", EMAIL);
    notificationStringToEnumMap.insert("Interaction required", INTERACTION);
    notificationStringToEnumMap.insert("No interaction required", NOINTERACTION);
}

/**
 * Delete alert.
 *
 * @param *alert Pointer to alert.
 * @param close  Close alert.
 */
void AlertManager::deleteAlert(Alert *alert, bool close)
{
    if (close)
        alert->close();

    int index = alerts.indexOf(alert);

    if (index != -1)
        alerts.removeAt(index);

    delete alert;
}

/**
 * Add alert.
 *
 * @param summary  Notification summary.
 * @param body     Notification body.
 * @param current  Create alert for current channel.
 * @param *parent  Pointer to parent for the alert.
 * @return Alert.
 */
Alert *AlertManager::addAlert(QString summary, QString body, bool current, QObject *parent)
{
    Alert *alert = NULL;

    if (AlertManager::alertSettings->at(1) && alerts.count() > 0) {
        alert = alerts.at(0);
        alert->close();
        alert = NULL;
    }
    QStringList alertSettings;

    switch(Misc::alertState()) {
    case WASOFF: {
        alertSettings = alertList->value("1. Window active, screen off");
        alertSettings.prepend("1. Window active, screen off");
        break;
    }
    case WDSOFF: {
        alertSettings = alertList->value("2. Window deactive, screen off");
        alertSettings.prepend("2. Window deactive, screen off");
        break;
    }
    case WCSOFF: {
        alertSettings = alertList->value("3. Window closed, screen off");
        alertSettings.prepend("3. Window closed, screen off");
        break;
    }
    case WASON: {
        QString state = current ? "7. Same as 4th but, alert from currently open chat" :
                        "4. Window active, screen on";
        alertSettings = alertList->value(state) ;
        alertSettings.prepend(state);
        break;
    }
    case WDSON: {
        alertSettings = alertList->value("5. Window deactive, screen on");
        alertSettings.prepend("5. Window deactive, screen on");
        break;
    }
    case WCSON: {
        alertSettings = alertList->value("6. Window closed, screen on");
        alertSettings.prepend("6. Window closed, screen on");
        break;
    }
    }

    switch(notificationStringToEnumMap[alertSettings.at(1)]) {
    case EMAIL: {
        alert = new Alert(summary, body, EMAIL, alertSettings, parent);
        alerts.append(alert);
        break;
    }
    case INTERACTION: {
        alert = new Alert(summary, body, INTERACTION, alertSettings, parent);
        alerts.append(alert);
        break;
    }
    case NOINTERACTION: {
        alert = new Alert(summary, body, NOINTERACTION, alertSettings, parent);
        alerts.append(alert);
        break;
    }
    default: {
        alert = new Alert(summary, body, 0, alertSettings, parent);
        alerts.append(alert);
    }
    }

    return alert;
}
