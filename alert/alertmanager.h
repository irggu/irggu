/****************************************************************************
**  alertmanager.h
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ALERTMANAGER_H
#define ALERTMANAGER_H

/**
 *  This class is for managing alerts.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-03-31
 */

#include "alert/alert.h"
#include <QList>
#include <QStringList>
#include <QMap>

enum AlertStates {
    WASOFF = 6,
    WDSOFF = 5,
    WCSOFF = 7,
    WASON = 2,
    WDSON = 1,
    WCSON = 3,
};

class AlertManager
{
public:
    static void  set();
    static void  deleteAlert(Alert *alert, bool close = true);
    static Alert *addAlert(QString summary, QString body, bool current, QObject *parent = 0);

private:
    static QMap<QString, Notifications> notificationStringToEnumMap;
    static QMap<QString, QStringList>   *alertList;
    static QList<bool>                  *alertSettings;
    static QList<Alert *>               alerts;
    static QString                      service;
    static QString                      path;
    static QString                      interface;
    static QString                      enableVibrator;

};
#endif // ALERTMANAGER_H
