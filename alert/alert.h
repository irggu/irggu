/****************************************************************************
**  alert.h
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ALERT_H
#define ALERT_H

/**
 *  This class is for alert.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-03-31
 */

#include <QObject>
#include <QStringList>

enum Notifications {
    EMAIL = 1,
    INTERACTION,
    NOINTERACTION,
};

class Alert : public QObject
{
    Q_OBJECT
public:
    Alert(QString summary, QString body, int type, QStringList settings, QObject *parent = 0);

    void close();

private:
    static QString             service;
    static QString             path;
    static QString             interface;
    static QString             mceService;
    static QString             mcePath;
    static QString             mceInterface;
    static QString             emailStyle;
    static QString             interaction;
    static QString             noInteraction;
    static QString             notificationClosed;
    static QString             notificationClicked;
    static QString             closeNotification;
    static QString             startVibration;
    static QString             stopVibration;
    static QString             startLed;
    static QString             stopLed;
    quint32                    id;
    QStringList                settings;

    void vibrate(bool stop = false);
    void led(bool stop = false);

private Q_SLOTS:
    void closed(quint32 id);
    void clicked(quint32 id, QString);

Q_SIGNALS:
    void closed(bool);
};

#endif // ALERT_H
