/****************************************************************************
**  alert.cpp
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "alert/alert.h"
#include "dbus/dbushandler.h"
#include "other/sound.h"
#include <QDebug>

QString             Alert::service             = "org.freedesktop.Notifications";
QString             Alert::path                = "/org/freedesktop/Notifications";
QString             Alert::interface           = "org.freedesktop.Notifications";
QString             Alert::mceService          = "com.nokia.mce";
QString             Alert::mcePath             = "/com/nokia/mce/request";
QString             Alert::mceInterface        = "com.nokia.mce.request";
QString             Alert::emailStyle          = "Notify";
QString             Alert::interaction         = "SystemNoteDialog";
QString             Alert::noInteraction       = "SystemNoteInfoprint";
QString             Alert::notificationClosed  = "NotificationClosed";
QString             Alert::notificationClicked = "ActionInvoked";
QString             Alert::closeNotification   = "CloseNotification";
QString             Alert::startVibration      = "req_vibrator_pattern_activate";
QString             Alert::stopVibration       = "req_vibrator_pattern_deactivate";
QString             Alert::startLed            = "req_led_pattern_activate";
QString             Alert::stopLed             = "req_led_pattern_deactivate";

/**
 * Constructor.
 *
 * @param summary  Notification summary.
 * @param body     Notification body.
 * @param type     Notification type.
 * @param settings Alert settings.
 * @param *parent  Pointer to parent.
 */
Alert::Alert(QString summary, QString body, int type, QStringList settings,
             QObject *parent) : QObject(parent)
{
    id             = 0;
    this->settings = settings;

    if (settings.at(4) != "")
        Sound::play(settings.at(0));

    switch(type) {
    case EMAIL: {
        QList<QVariant> arguments;
        arguments.append(QVariant(QString("IrGGu")));
        arguments.append(QVariant(quint32(0)));
        arguments.append(QVariant(QString("irggu")));
        arguments.append(QVariant(summary));
        arguments.append(QVariant(body));
        arguments.append(QVariant(QStringList()));
        arguments.append(QVariant(QVariantMap()));
        arguments.append(QVariant((int)0));

        QList<QVariant> reply = DbusHandler<quint32>::dbusReplyCall("session", service, path,
                                                                    interface, emailStyle,
                                                                    arguments);

        if (!reply.isEmpty())
            id = reply.at(0).toUInt();

        break;
    }
    case INTERACTION: {
        QList<QVariant> arguments;
        arguments.append(QVariant(QString(summary+" "+body)));
        arguments.append(QVariant(quint32(0)));
        arguments.append(QVariant(QString()));
        QList<QVariant> reply = DbusHandler<quint32>::dbusReplyCall("session", service, path,
                                                                    interface, interaction,
                                                                    arguments);

        if (!reply.isEmpty())
            id = reply.at(0).toUInt();

        break;
    }
    case NOINTERACTION: {
        QList<QVariant> arguments;
        arguments.append(QVariant(QString(summary+" "+body)));
        QList<QVariant> reply = DbusHandler<quint32>::dbusReplyCall("session", service, path,
                                                                    interface, noInteraction,
                                                                    arguments);

        if (!reply.isEmpty())
            id = reply.at(0).toUInt();

        break;
    }
    }

    if (id != 0) {
        DbusHandler<>::connectDbusSignal("session", service, path, interface, notificationClosed,
                                         this, SLOT(closed(quint32)));
        DbusHandler<>::connectDbusSignal("session", service, path, interface, notificationClicked,
                                         this, SLOT(clicked(quint32, QString)));
    }

    if (settings.at(2) != "")
        vibrate();

    if (settings.at(3) != "")
        led();
}

/**
 * Close alert.
 */
void Alert::close()
{
    if (settings.at(2) != "")
        vibrate(true);

    if (settings.at(3) != "")
        led(true);

    if (settings.at(4) != "")
        Sound::stop(settings.at(0));

    if (id != 0) {
        QList<QVariant> arguments;
        arguments.append(QVariant(id));
        DbusHandler<>::dbusCall("session", service, path, interface, closeNotification, arguments);
    }
}

/**
 * Start or stop vibration.
 *
 * @param stop Stop vibration.
 */
void Alert::vibrate(bool stop)
{
    QList<QVariant> arguments;
    arguments.append(QVariant(QString("PatternChatAndEmail")));

    if (!stop)
        DbusHandler<>::dbusCall("system", mceService, mcePath, mceInterface, startVibration,
                                arguments);
    else
        DbusHandler<>::dbusCall("system", mceService, mcePath, mceInterface, stopVibration,
                                arguments);
}

/**
 * Start or stop led blinking.
 *
 * @param stop Stop led blinking.
 */
void Alert::led(bool stop)
{
    QList<QVariant> arguments;
    arguments.append(QVariant(QString("PatternCommunicationEmail")));

    if (!stop)
        DbusHandler<>::dbusCall("system", mceService, mcePath, mceInterface, startLed, arguments);
    else
        DbusHandler<>::dbusCall("system", mceService, mcePath, mceInterface, stopLed, arguments);
}

/**
 * Detect when notification is closed.
 *
 * @param id Notification id.
 */
void Alert::closed(quint32 id)
{
    if (this->id == id)
        Q_EMIT closed(false);
}

/**
 * Detect when notification is clicked.
 *
 * @param id Notification id.
 */
void Alert::clicked(quint32 id, QString)
{
    if (this->id == id) {
        if (settings.at(2) != "")
            vibrate(true);

        if (settings.at(3) != "")
            led(true);

        if (settings.at(4) != "")
            Sound::stop(settings.at(0));

        Q_EMIT closed(true);
    }
}
