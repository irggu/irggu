/****************************************************************************
**  customgestures.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "customgestures.h"
#include <QWidget>
#include <QMouseEvent>
#include <QDebug>

/**
 * Constructor.
 *
 * @param moveThreshold Move threshold.
 */
RightSwipeGestureRecognizer::RightSwipeGestureRecognizer(int moveThreshold)
{
    this->moveThreshold = moveThreshold;
}

/**
 * Gesture recognizer.
 *
 * @param *event Pointer to event
 * @return gesture recognizer result.
 */
QGestureRecognizer::Result RightSwipeGestureRecognizer::recognize(QGesture *, QObject *,
                                                                  QEvent *event)
{

    QMouseEvent *ev = static_cast<QMouseEvent *>(event);
    QGestureRecognizer::Result result;

    switch (ev->type()) {
    case QEvent::MouseButtonPress: {
        lastX = ev->globalX();
        break;
    }
    case QEvent::MouseButtonRelease: {
        int distance = ev->globalX() - lastX;
        if (distance > moveThreshold) {
            result = QGestureRecognizer::FinishGesture;
        } else {
            result = QGestureRecognizer::CancelGesture;
        }
        break;
    }
    default:
        result = QGestureRecognizer::Ignore;
        break;
    }
    return result;
}

/**
 * Reset gesture.
 *
 * @param *gesture Pointer to gesture.
 */
void RightSwipeGestureRecognizer::reset(QGesture *gesture)
{
    lastX = 0;
    QGestureRecognizer::reset(gesture);
}

/**
 * Set move threshold.
 *
 * @param moveThreshold Move threshold.
 */
void RightSwipeGestureRecognizer::setMoveThreshold(int moveThreshold)
{
    this->moveThreshold = moveThreshold;
}

/**
 * Constructor.
 *
 * @param moveThreshold Move threshold.
 */
LeftSwipeGestureRecognizer::LeftSwipeGestureRecognizer(int moveThreshold)
{
  this->moveThreshold = moveThreshold;
}

/**
 * Gesture recognizer.
 *
 * @param *event Pointer to event
 * @return gesture recognizer result.
 */
QGestureRecognizer::Result LeftSwipeGestureRecognizer::recognize(QGesture *, QObject *,
                                                              QEvent *event)
{

    QMouseEvent *ev = static_cast<QMouseEvent *>(event);
    QGestureRecognizer::Result result;

    switch (ev->type()) {
    case QEvent::MouseButtonPress: {
        lastX = ev->globalX();
        break;
    }
    case QEvent::MouseButtonRelease: {
        int distance =  lastX - ev->globalX();
        if (distance > moveThreshold) {
            result = QGestureRecognizer::FinishGesture;
        } else {
            result = QGestureRecognizer::CancelGesture;
        }
        break;
    }
    default:
        result = QGestureRecognizer::Ignore;
        break;
    }
    return result;
}

/**
 * Reset gesture.
 *
 * @param *gesture Pointer to gesture.
 */
void LeftSwipeGestureRecognizer::reset(QGesture *gesture)
{
    lastX = 0;
    QGestureRecognizer::reset(gesture);
}

/**
 * Set move threshold.
 *
 * @param moveThreshold Move threshold.
 */
void LeftSwipeGestureRecognizer::setMoveThreshold(int moveThreshold)
{
  this->moveThreshold = moveThreshold;
}
