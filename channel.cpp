/****************************************************************************
**  channel.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "channel.h"
#include "config.h"
#include "alert/alertmanager.h"
#include <QDebug>

/**
 * Constructor.
 *
 * @param *nick   Pointer to nick.
 * @param name    Chat name.
 * @param type    Chat type.
 * @param *parent Pointer to parent.
 */
Channel::Channel(QString *nick, QString name, int type, QObject *parent) : QObject(parent)
{
    this->name          = name;
    this->nick          = nick;
    this->type          = type;
    this->chat          = "";
    this->alertSettings = Config::getAlertSettings();
    this->colorsList    = Config::getColorList();
    this->hightlights   = Config::getHighlightList();
    this->alert         = NULL;

    if(type == 1)
        this->nickCompletionList.append(name);

    this->namesList.insert("@", new QStringList());
    this->namesList.insert("+", new QStringList());
    this->namesList.insert("other", new QStringList());
    this->namesList.insert("has+", new QStringList());

    if (type == 2)
        handleOtherMsg("Now talking at " + name , false);
}

/**
 * Handles private messages.
 *
 * @param line Private message.
 */
void Channel::handlePrivMsg(QString sender, QString line, bool notInChat, bool current,
                            bool maybeHighlight, bool alert)
{
    if (!line.isEmpty()) {
        bool highlight = false;
        QString msg    = "";
        QString nMsg   = "";

        line.replace('<', "&lt;");
        line.replace('>', "&gt;");

        QStringList msgList = line.split(' ');
        QListIterator<QString> msgListIt(msgList);
        QString msgItem;

        while (msgListIt.hasNext()) {
            msgItem = msgListIt.next();

            if (msgItem.startsWith("http://")) {
                msg  += "<a href=" + msgItem + ">" + msgItem + "</a> ";
                nMsg += "<a href=" + msgItem + ">" + msgItem + "</a> ";
            } else if (msgItem == "") {
                msg  += "&nbsp;";
                nMsg += " ";
            } else {
                msg  += msgItem + " ";
                nMsg += msgItem + " ";
            }

            if (maybeHighlight) {
               if (!msgItem.trimmed().isEmpty() && (nick == msgItem
                                                    || hightlights->contains(msgItem))) {
                   maybeHighlight = false;
                   highlight = true;
               }
           }
        }

        nMsg.replace("&lt;", "<");
        nMsg.replace("&gt;", ">");

        msg = msg.trimmed();

        if (!notInChat) {
            line = "<span style='color: "
                 + colorsList->value((highlight ? "highlightMessage" : "incommingMessage"))
                 + "' ><strong>&lt;" + sender + "&gt;</strong> " + msg + "</span>";
            chat += line + "<br />";
        } else {
            line = "<span style='color: " + colorsList->value("otherText") + "' ><strong>&lt;"
                 + sender + "&gt;</strong> " + msg + "</span>";
        }

        if (type == 1 && alert)
            createAlert(name, nMsg, current);
        else if (highlight && alert)
            createAlert(name+" "+sender, nMsg, current);

        checkRowCount();

        Q_EMIT newLine(line);
    }
}

/**
 * Handles messages send by the user.
 *
 * @param nick Users nick.
 * @param msg  Users message.
 */
void Channel::handleOwnMsg(QString nick, QString msg)
{
    msg.replace(" ", "&nbsp;");
    msg.replace('<', "&lt;");
    msg.replace('>', "&gt;");
    msg   = "<span style='color: " + colorsList->value("sentMessage") + "' ><strong>&lt;"
           + nick + "&gt;</strong> " + msg + "</span>";
    chat += msg + "<br />";
    checkRowCount();

    Q_EMIT newLine(msg);
}

/**
 * Handles action send by the user.
 *
 * @param nick Users nick.
 * @param msg  Users message.
 */
void Channel::handleOwnAction(QString nick, QString msg)
{
    msg   = nick + " " + msg;
    msg   = "<span style='color: " + colorsList->value("sentMessage")
            + "' ><strong>&lt;*&gt;</strong> " + msg + "</span>";
    chat += msg + "<br />";
    checkRowCount();

    Q_EMIT newLine(msg);
}

/**
 * Handles server messages.
 *
 * @param line Server message.
 */
void Channel::handleServerMsg(QString line)
{
    line.replace(" ", "&nbsp;");
    line  = "<span style='color: " + colorsList->value("incommingMessage") + "' >"
            + line + "</span>";
    chat += line + "<br />";
    checkRowCount();

    Q_EMIT newLine(line);
}

/**
 * Handles other messages.
 *
 * @param line Message.
 */
void Channel::handleOtherMsg(QString line, bool notInChat)
{
    line = "<span style='color: " + colorsList->value("otherText")
         + "' ><strong>&lt;*&gt;</strong> " + line + "</span>";
    if (!notInChat) {
        chat += line + "<br />";
        checkRowCount();
    }

    Q_EMIT newLine(line);
}

/**
 * Add user names at the channel to the names list.
 *
 * @param names List of names.
 */
void Channel::addNames(QStringList names)
{
    QStringListIterator namesIt(names);
    QString name;
    QString prefix;

    while (namesIt.hasNext()) {
        name = namesIt.next();

        if (name.at(0) == '@' || name.at(0) == '+') {
            prefix = name.at(0);

            if (!namesList.value(prefix)->contains(name))
                namesList.value(prefix)->append(name);

            name.remove(0, 1);

            if (prefix == "+" && !namesList.value("has+")->contains(name))
                namesList.value("has+")->append(name);
        } else {
            if (!namesList.value("other")->contains(name))
                namesList.value("other")->append(name);
        }

        if (!nickCompletionList.contains(name))
            nickCompletionList.append(name);
    }
    namesList.value("@")->sort();
    namesList.value("+")->sort();
    namesList.value("other")->sort();

    Q_EMIT namesChanged(getNames());
}

/**
 * Add user name at the channel to the names list.
 *
 * @param name User name.
 */
void Channel::addName(QString name)
{
    namesList.value("other")->append(name);
    namesList.value("other")->sort();
    nickCompletionList.append(name);

    Q_EMIT namesChanged(getNames());
}

/**
 * delete user name that has left the channel.
 *
 * @param name User name.
 */
void Channel::deleteName(QString name)
{
    namesList.value("other")->removeOne(name);
    namesList.value("has+")->removeOne(name);
    namesList.value("+")->removeOne("+" + name);
    namesList.value("@")->removeOne("@" + name);

    Q_EMIT namesChanged(getNames());
}

/**
 * Move user name to channel operators list.
 *
 * @param name User name.
 */
void Channel::giveOp(QString user)
{
    if (namesList.value("other")->contains(user))
        namesList.value("other")->removeOne(user);
    else
        namesList.value("+")->removeOne("+" + user);

    namesList.value("@")->append("@" + user);
    namesList.value("@")->sort();

    Q_EMIT namesChanged(getNames());
}

/**
 * Move user name from channel operators list.
 *
 * @param name User name.
 */
void Channel::takeOp(QString user)
{
    QString list = "other";
    namesList.value("@")->removeOne("@" + user);

    if (namesList.value("has+")->contains(user)) {
        list = "+";
        user = "+" + user;
    }

    namesList.value(list)->append(user);
    namesList.value(list)->sort();

    Q_EMIT namesChanged(getNames());
}

/**
 * Move user name to voice list.
 *
 * @param name User name.
 */
void Channel::giveVoice(QString user)
{
    namesList.value("has+")->append(user);

    if (namesList.value("other")->contains(user)) {
        namesList.value("other")->removeOne(user);
        namesList.value("+")->append("+" + user);
        namesList.value("+")->sort();

        Q_EMIT namesChanged(getNames());
    }
}

/**
 * Move user name from voice list.
 *
 * @param name User name.
 */
void Channel::takeVoice(QString user)
{
    namesList.value("has+")->removeOne(user);

    if (namesList.value("+")->contains("+" + user)) {
        namesList.value("+")->removeOne("+" + user);
        namesList.value("other")->append(user);
        namesList.value("other")->sort();

        Q_EMIT namesChanged(getNames());
    }
}

/**
 * Reset channel, used when reconnected to server.
 */
void Channel::reset()
{
    nickCompletionList.clear();
    namesList.value("@")->clear();
    namesList.value("+")->clear();
    namesList.value("other")->clear();
    namesList.value("has+")->clear();

    if(type == 1)
        nickCompletionList.append(name);

    if (type == 2)
        handleOtherMsg("Now talking at " + name , false);
}

/**
 * Returns channel name.
 *
 * @return Channel name.
 */
QString Channel::getName()
{
    return name;
}

/**
 * Returns channel chat.
 *
 * @return Channel chat.
 */
QString Channel::getChat()
{
    //return html.section("</p></body></html>", 0, 0) + chat + "</p></body></html>";
    return chat.left(chat.length()-6);
}

/**
 * Returns name/s that starts with the line.
 *
 * @param  line Line that is checked if any name starts with it.
 * @return Name/s start with the line.
 */
QString Channel::nickCompletion(QString line)
{
    QString matchedNicks = "";
    QListIterator<QString> nickCompletionListIt(nickCompletionList);

    while (nickCompletionListIt.hasNext()) {
        QString nick = nickCompletionListIt.next();

        if (nick.toLower().startsWith(line.toLower()))
            matchedNicks += nick + " ";
    }

    return matchedNicks.trimmed();
}

/**
 * Returns names list.
 *
 * @return Names list.
 */
QStringList Channel::getNames()
{
    QStringList names;

    names.append(*namesList.value("@"));
    names.append(*namesList.value("+"));
    names.append(*namesList.value("other"));

    return names;
}

/**
 * Change user name.
 *
 * @param oldName Old user name.
 * @param newName New user name.
 * @return Is the user on this channel.
 */
bool Channel::changeName(QString oldName, QString newName)
{
    bool isOnThisChannel;
    isOnThisChannel = nickCompletionList.contains(oldName);

    if (isOnThisChannel) {
        nickCompletionList.removeOne(oldName);
        nickCompletionList.append(newName);

        if (namesList.value("other")->contains(oldName)) {
            namesList.value("other")->removeOne(oldName);
            namesList.value("other")->append(newName);
            namesList.value("other")->sort();
        } else if (namesList.value("+")->contains("+" + oldName)) {
            namesList.value("+")->removeOne("+" + oldName);
            namesList.value("+")->append("+" + newName);
            namesList.value("+")->sort();

            namesList.value("has+")->removeOne(oldName);
            namesList.value("has+")->append(newName);
        } else {
            namesList.value("@")->removeOne("@" + oldName);
            namesList.value("@")->append("@" + newName);
            namesList.value("@")->sort();

            if (namesList.value("has+")->contains(oldName)) {
                namesList.value("has+")->removeOne(oldName);
                namesList.value("has+")->append(newName);
            }
        }

        Q_EMIT namesChanged(getNames());
    }

    return isOnThisChannel;
}

/**
 * Check if user is on the channel.
 *
 * @param name User name.
 * @return Is the user on this channel.
 */
bool Channel::hasName(QString name)
{
    return nickCompletionList.contains(name);
}

/**
 * Keeps the chat row count in limit.
 */
void Channel::checkRowCount()
{
    int count = chat.count("<br />");
    int limit = 500;

    if (count > limit) {
        for (; count > limit; count--) {
            chat = chat.right(chat.length() - chat.indexOf("<br />") - 6);
        }
    }
}

/**
 * Create alert for this chat.
 *
 * @param summary Alert summary.
 * @param body Alert body.
 * @param current Is this chat current chat.
 */
void Channel::createAlert(QString summary, QString body, bool current)
{
    if (!alertSettings->at(0)) {
        if (alert != NULL) {
            disconnect(alert, SIGNAL(closed(bool)), this, SLOT(notificationClosed(bool)));
            AlertManager::deleteAlert(alert);
            alert = NULL;
        }

        alert = AlertManager::addAlert(summary, body, current, this);

        if (alert != NULL)
            connect(alert, SIGNAL(closed(bool)), this, SLOT(notificationClosed(bool)));
    }
}

/**
 * Detect when notification is closed.
 *
 * @param clicked Was notification clicked.
 */
void Channel::notificationClosed(bool clicked)
{
    disconnect(alert, SIGNAL(closed(bool)), this, SLOT(notificationClosed(bool)));
    AlertManager::deleteAlert(alert, false);
    alert = NULL;

    if(clicked)
        Q_EMIT notificationClicked(name);
}
