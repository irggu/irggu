/****************************************************************************
**  customgestures.h
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef CUSTOMGESTURES_H
#define CUSTOMGESTURES_H

/**
 *  This class is for right swipe gesture.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-03-31
 */

#include <QGesture>
#include <QGestureRecognizer>

class RightSwipeGestureRecognizer : public QGestureRecognizer
{
public:
    explicit RightSwipeGestureRecognizer(int moveThreshold);

    QGestureRecognizer::Result recognize(QGesture *, QObject *, QEvent *event);
    void                       reset(QGesture *gesture);
    void                       setMoveThreshold(int moveThreshold);

private:
    int lastX;
    int moveThreshold;
};

/**
 *  This class is for left swipe gesture.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2011-03-31
 */

class LeftSwipeGestureRecognizer : public QGestureRecognizer
{
public:
    explicit LeftSwipeGestureRecognizer(int moveThreshold);

    QGestureRecognizer::Result recognize(QGesture *, QObject *, QEvent *event);
    void                       reset(QGesture *gesture);
    void                       setMoveThreshold(int moveThreshold);

private:
    int        lastX;
    int        moveThreshold;
};

#endif // CUSTOMGESTURES_H
