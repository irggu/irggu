/****************************************************************************
**  linehandler.cpp
**
**  Copyright information
**
**      Copyright (C) 2010-2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu.
**
**      This program is free software; you can redistribute it and/or
**      modify it under the terms of the GNU General Public License as
**      published by the Free Software Foundation; either version 2 of
**      the License, or (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful, but
**      WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**      General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program. If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#include "linehandler.h"
#include "config.h"
#include "other/currentsong.h"

QMap<QString, Commands> LineHandler::commandStringToEnum;
QMap<QString, Modes>    LineHandler::modeStringToEnum;

/**
 * Set string to enum QMaps.
 */
void LineHandler::set()
{
    commandStringToEnum.insert("PING", PING);
    commandStringToEnum.insert("001", WELLCOME);
    commandStringToEnum.insert("432", ERRORNICKNAME);
    commandStringToEnum.insert("433", ERRORNICKNAME);
    commandStringToEnum.insert("PRIVMSG", PRIVMSG);
    commandStringToEnum.insert("JOIN", JOIN);
    commandStringToEnum.insert("331", NOTOPIC);
    commandStringToEnum.insert("332", TOPIC);
    commandStringToEnum.insert("333", TOPICWHO);
    commandStringToEnum.insert("353", NAMES);
    commandStringToEnum.insert("366", ENDOFNAMES);
    commandStringToEnum.insert("354", WHOSPCRPL);
    commandStringToEnum.insert("315", ENDOFWHO);
    commandStringToEnum.insert("MODE", MODE);
    commandStringToEnum.insert("NP", NP);
    commandStringToEnum.insert("ME", ME);
    commandStringToEnum.insert("TOPIC", TOPIC);
    commandStringToEnum.insert("NAMES", NAMES);
    commandStringToEnum.insert("JOIN", JOIN);
    commandStringToEnum.insert("QUERY", QUERY);
    commandStringToEnum.insert("PART", PART);
    commandStringToEnum.insert("LEAVE", PART);
    commandStringToEnum.insert("NICK", NICK);
    commandStringToEnum.insert("WHOIS", WHOIS);
    commandStringToEnum.insert("311", WHOISUSER);
    commandStringToEnum.insert("312", WHOISSERVER);
    commandStringToEnum.insert("313", WHOISOPERATOR );
    commandStringToEnum.insert("317", WHOISIDLE);
    commandStringToEnum.insert("318", ENDOFWHOIS);
    commandStringToEnum.insert("319", WHOISCHANNELS);
    commandStringToEnum.insert("330", WHOISACCOUNT);
    commandStringToEnum.insert("KICK", KICK);
    commandStringToEnum.insert("367", BANLIST);
    commandStringToEnum.insert("368", ENDOFBANLIST);
    commandStringToEnum.insert("BANLIST", BANLIST);
    commandStringToEnum.insert("QUIT", QUIT);
    commandStringToEnum.insert("RAW", RAW);
    commandStringToEnum.insert("NOTICE", NOTICE);
    commandStringToEnum.insert("PONG", PONG);

    modeStringToEnum.insert("+o", GIVEOP);
    modeStringToEnum.insert("-o", TAKEOP);
    modeStringToEnum.insert("+v", GIVEVOICE);
    modeStringToEnum.insert("-v", TAKEVOICE);
    modeStringToEnum.insert("+b", BAN);
    modeStringToEnum.insert("-b", UNBAN);
};

/**
 * Handle line from server.
 *
 * @param line              Line that needs to be handled.
 * @param *connection       Pointer to connection.
 * @param currentConnection Is this currentConnection.
 */
void LineHandler::handleIn(QString line, Connection *connection, bool currentConnection)
{
    QString filtered   = line;
    filtered.replace(QRegExp("^[^ ]{5,} "), "");
    QString command    = filtered.section(' ', 0 , 0);
    QString theRest    = filtered.section(' ', 1).trimmed();

    switch (commandStringToEnum[command]) {
    case PING: {
            writeCommand("PONG " + theRest, connection);
            break;
        }
    case WELLCOME: {
            QString nick = theRest.section(' ', 0, 0);
            QString line = theRest.section(' ', 1);
            connection->setNick(nick);

            if (line.startsWith(':'))
                line.remove(0, 1);

            connection->getChannels()->value("(server)")->handleServerMsg(line);

            QStringList commands = connection->getCommands();
            QStringListIterator commandsIt(commands);

            while (commandsIt.hasNext()) {
                QString command = commandsIt.next();

                if (!command.isEmpty()) {
                    if (!command.startsWith('/'))
                        command.insert(0, '/');

                    handleOut(command, connection);
                }
            }
            break;
        }
    case ERRORNICKNAME: {
            if (!connection->getUseAlter()) {
                connection->setUser(true);
                Channel *channel = connection->getChannels()->value("(server)");
                channel->handleServerMsg("Invalid nick or nick in use. Using alternative!");
            } else {
                Channel *channel = connection->getChannels()->value("(server)");
                channel->handleServerMsg("Invalid nick or nick in use!");
            }
            break;
        }
    case PRIVMSG: {
            QString target                          = theRest.section(' ', 0, 0).trimmed();
            QString msg                             = theRest;
            QString sender                          = line.section('!', 0, 0);
            QMap<QString, Channel *> *channels      = connection->getChannels();
            QMap<QString, QList<bool> > *ignoreList = connection->getIgnoreList();
            Channel **currentChannelPointer         = connection->getCurrentChannelPointer();
            Channel *currentChannel                 = *currentChannelPointer;
            sender.remove(0, 1);
            msg.remove(target + " :");

            if (channels->find(target) != channels->end()) {

                if (msg.startsWith("")) {
                    msg    = handleCTCPMsg(msg, sender);
                    sender = "*";
                }

                bool current = currentChannel->getName() == target ? currentConnection : false;

                bool alert = true;

                if (ignoreList->find(sender) != ignoreList->end())
                    if (ignoreList->value(sender).at(1))
                        alert = false;

                channels->value(target)->handlePrivMsg(sender, msg, false, current, true, alert);
            } else if (msg.startsWith("PING")) {
                QString notice = "NOTICE " + sender + " :" + msg;
                writeCommand(notice, connection);
            } else if (target.at(0) != '&' && target.at(0) != '#' && target.at(0) != '+'
                     && target.at(0) != '!') {

                QString nick = sender;

                if (msg.startsWith("")) {
                    msg    = handleCTCPMsg(msg, sender);
                    nick = "*";
                }

                bool ignore = false;
                bool alert = true;

                if (ignoreList->find(sender) != ignoreList->end()) {
                    if (ignoreList->value(sender).at(0))
                        ignore = true;

                    if (ignoreList->value(sender).at(1))
                        alert = false;
                }

                if (!ignore) {
                    if (channels->find(sender) == channels->end() && !msg.isEmpty()) {
                        addChannel(channels, currentChannelPointer, sender, 1, connection);
                        currentChannel = *currentChannelPointer;
                        currentChannel->handlePrivMsg(nick, msg, false, currentConnection, false,
                                                      alert);
                    } else if (!msg.isEmpty()){
                        bool current;
                        current = currentChannel->getName() == sender ? currentConnection : false;
                        channels->value(sender)->handlePrivMsg(nick, msg, false, current, false,
                                                               alert);
                    }
                }
            }

            break;
        }
    case JOIN: {
            QString channel                    = theRest.replace(':', "").trimmed();
            QString nick                       = line.section('!', 0, 0);
            QMap<QString, Channel *> *channels = connection->getChannels();
            Channel **currentChannelPointer    = connection->getCurrentChannelPointer();
            nick.remove(0, 1);

            if (channels->find(channel) == channels->end()) {
                addChannel(channels, currentChannelPointer, channel, 2, connection);
            } else if (nick != connection->getNick()){
                channels->value(channel)->addName(nick);
                channels->value(channel)->handleOtherMsg(nick + " has joined " +  channel);
            } else {
                channels->value(channel)->reset();
            }

            break;
        }
    case NOTOPIC: {
            QString channel                    = theRest.section(' ', 1, 1);
            QMap<QString, Channel *> *channels = connection->getChannels();

            channels->value(channel)->handleOtherMsg("No topic is set.");

            break;
        }
    case TOPIC: {
            QString channel;
            QString msg;
            QString topic = theRest.section(" :", 1, 1).trimmed();
            QMap<QString, Channel *> *channels = connection->getChannels();

            if (command == "TOPIC") {
                channel = theRest.section(' ', 0, 0);
                QString sender = line.remove(0, 1).section("!", 0, 0);
                msg = sender + " has changed the topic to: " + topic;
            } else {
                channel = theRest.section(' ', 1, 1);
                msg = "Topic: " + topic;
            }

            channels->value(channel)->handleOtherMsg(msg);

            break;
        }
    case TOPICWHO: {
            QString channel                    = theRest.section(' ', 1, 1);
            QString nick                       = theRest.section(' ', 2, 2);
            QString time                       = theRest.section(' ', 3, 3).trimmed();
            QDateTime date                     = QDateTime::fromTime_t(time.toInt());
            QString msg                        = "Topic set by: " + nick + " at "
                                                 + date.toString();
            QMap<QString, Channel *> *channels = connection->getChannels();

            channels->value(channel)->handleOtherMsg(msg);

            break;
        }
    case NAMES: {
            QString channel                    = theRest.section(' ', 2, 2);
            QMap<QString, Channel *> *channels = connection->getChannels();
            QString names                      = theRest.replace(QRegExp("^.*:"), "").trimmed();
            QString msg                        = "Users on " + channel + ": " + names;

            if (channels->find(channel) != channels->end()) {
                channels->value(channel)->addNames(names.split(' '));
                channels->value(channel)->handleOtherMsg(msg);
            } else {
                channels->value("(server)")->handleOtherMsg(msg);
            }

            break;
        }
    case ENDOFNAMES:
            break;
    case WHOSPCRPL:
            break;
    case ENDOFWHO:
            break;
    case MODE: {
            QString nick                       = line.remove(0, 1).section("!", 0, 0);
            QString target                     = theRest.section(' ', 0, 0);
            QMap<QString, Channel *> *channels = connection->getChannels();

            if (target.at(0) != '&' && target.at(0) != '#' && target.at(0) != '+'
                && target.at(0) != '!') {
                QString mode = theRest.section(' ', 1, 1);

                if (mode.at(0) == ':')
                    mode.remove(0, 1);

                channels->value("(server)")->handleServerMsg(nick + " sets mode " + mode
                                                                    + ' ' +  target);
            } else {
                QString users        = theRest.section(' ', 2);
                QString mode         = theRest.section(' ', 1, 1);
                QString prefix       = mode.at(0);
                QStringList userList = !users.isEmpty() ? users.split(' ') : QStringList();
                QString user;
                QString msg;
                mode.remove(0, 1);

                if (userList.count() == mode.length()) {
                    for (int i = 0; i < mode.length(); i++) {
                        user = userList.at(i);

                        switch (modeStringToEnum[prefix + mode.at(i)]) {
                        case GIVEOP:
                                msg = " gives channel operator status to ";
                                channels->value(target)->giveOp(user);
                                channels->value(target)->handleOtherMsg(nick + msg +  user);
                                break;
                        case TAKEOP:
                                msg = " takes channel operator status from ";
                                channels->value(target)->takeOp(user);
                                channels->value(target)->handleOtherMsg(nick + msg +  user);
                                break;
                        case GIVEVOICE:
                                msg = " gives voice to ";
                                channels->value(target)->giveVoice(user);
                                channels->value(target)->handleOtherMsg(nick + msg +  user);
                                break;
                        case TAKEVOICE:
                                msg = " takes voice from ";
                                channels->value(target)->takeVoice(user);
                                channels->value(target)->handleOtherMsg(nick + msg +  user);
                                break;
                        case BAN:
                                msg = " bans ";
                                channels->value(target)->handleOtherMsg(nick + msg +  user);
                                break;
                        case UNBAN:
                                msg = " unbans ";
                                channels->value(target)->handleOtherMsg(nick + msg +  user);
                                break;
                        }
                    }
                } else {
                    if(users.isEmpty())
                        users = target;

                    msg = " sets mode ";
                    channels->value(target)->handleServerMsg(nick + msg + prefix + mode + ' '
                                                             + users);
                }

            }

            break;
        }
    case PART: {
            QMap<QString, Channel *> *channels = connection->getChannels();
            QString nick                       = line.remove(0, 1).section("!", 0, 0);
            QString channel                    = theRest.section(' ', 0, 0);
            QString msg                        = nick + " has left " + channel + " ("
                                               + theRest.section(" :", 1, 1) + ")";

            if (channels->find(channel) != channels->end()) {
                if (nick == connection->getNick()) {
                    removeChannel(channels, channel, connection);
                } else {
                    channels->value(channel)->deleteName(nick);
                    channels->value(channel)->handleOtherMsg(msg);
                }
            }
            break;
        }
    case NICK: {
            QString oldNick                    = line.remove(0, 1).section("!", 0, 0);
            QString newNick                    = theRest.startsWith(':') ? theRest.remove(0, 1)
                                                                         : theRest;
            QMap<QString, Channel *> *channels = connection->getChannels();
            QMapIterator<QString, Channel *> channelsIt(*channels);
            bool isOnTheChannel;
            Channel *channel;
            QString msg;

            while (channelsIt.hasNext()) {
                channelsIt.next();
                channel        = channelsIt.value();
                isOnTheChannel = channel->changeName(oldNick, newNick);

                if (channel == connection->getCurrentChannel() && isOnTheChannel) {
                    if (oldNick == connection->getNick()) {
                        msg = "You are now known as " + newNick;
                        connection->setNick(newNick);
                    } else {
                        msg = oldNick + " is now known as " + newNick;
                    }

                    channelsIt.value()->handleOtherMsg(msg);
                }
            }
            break;
        }
    case WHOISUSER: {
            QStringList info = theRest.split(' ');
            QString nick     = info.at(1);
            QString msg      = "[" + nick + "] " + info.at(2) + "@" + info.at(3) + ' '
                               + theRest.section(' ', 5);

            if (connection->getCurrentChannel()->hasName(nick)) {
                connection->whois(msg, true);
            } else {
                QMap<QString, Channel *> *channels = connection->getChannels();
                channels->value("(server)")->handleServerMsg(msg);
            }
            break;
        }
    case WHOISSERVER: {
            QString nick = theRest.section(' ', 1, 1);
            QString msg  = "[" + nick + "] " + theRest.section(' ', 2);

            if (connection->getCurrentChannel()->hasName(nick)) {
                connection->whois(msg);
            } else {
                QMap<QString, Channel *> *channels = connection->getChannels();
                channels->value("(server)")->handleServerMsg(msg);
            }
            break;
        }
    case WHOISOPERATOR: {
            QString nick = theRest.section(' ', 1, 1);
            QString msg  = "[" + nick + "] " + theRest.section(" :", 1);

            if (connection->getCurrentChannel()->hasName(nick)) {
                connection->whois(msg);
            } else {
                QMap<QString, Channel *> *channels = connection->getChannels();
                channels->value("(server)")->handleServerMsg(msg);
            }
            break;
        }
    case WHOISIDLE:{
            QTime idle(0, 0);
            QStringList info = theRest.split(' ');
            QString nick     = info.at(1);
            QString signon   = QDateTime::fromTime_t(info.at(3).toInt()).toString();
            idle             = idle.addSecs(info.at(2).toInt());
            QString msg      = "[" + nick + "] idle: " + idle.toString() + " signon: " + signon;

            if (connection->getCurrentChannel()->hasName(nick)) {
                connection->whois(msg);
            } else {
                QMap<QString, Channel *> *channels = connection->getChannels();
                channels->value("(server)")->handleServerMsg(msg);
            }
            break;
        }
    case ENDOFWHOIS: {
            QString nick = theRest.section(' ', 1, 1);
            QString msg  = "[" + nick + "] End of WHOIS.";

            if (connection->getCurrentChannel()->hasName(nick)) {
                connection->whois(msg);
            } else {
                QMap<QString, Channel *> *channels = connection->getChannels();
                channels->value("(server)")->handleServerMsg(msg);
            }
            break;
        }
    case WHOISCHANNELS: {
            QString nick = theRest.section(' ', 1, 1);
            QString msg  = "[" + nick + "] " + theRest.section(" :", 1);

            if (connection->getCurrentChannel()->hasName(nick)) {
                connection->whois(msg);
            } else {
                QMap<QString, Channel *> *channels = connection->getChannels();
                channels->value("(server)")->handleServerMsg(msg);
            }
            break;
        }
    case WHOISACCOUNT: {
            QString nick = theRest.section(' ', 1, 1);
            QString msg  = "[" + nick + "] " + theRest.section(" :", 1) + " "
                           + theRest.section(' ', 2, 2);

            if (connection->getCurrentChannel()->hasName(nick)) {
                connection->whois(msg);
            } else {
                QMap<QString, Channel *> *channels = connection->getChannels();
                channels->value("(server)")->handleServerMsg(msg);
            }
            break;
        }
    case KICK: {
            QString kicker                     = line.remove(0, 1).section("!", 0, 0);
            QString channel                    = theRest.section(' ', 0, 0);
            QString kicked                     = theRest.section(' ', 1, 1);
            QString msg                        = theRest.section(" :", 1);
            QMap<QString, Channel *> *channels = connection->getChannels();

            if (channels->find(channel) != channels->end() && kicked == connection->getNick()) {
                removeChannel(channels, channel, connection);
                channels->value("(server)")->handleServerMsg(kicker + " has kicked you from "
                                                             + channel + "("+ msg +")");
            } else {
                if (kicker == connection->getNick()) {
                    channels->value(channel)->handleOtherMsg("You kicked " + kicked
                                                              + " from this channel(" + msg +")");
                } else {
                    channels->value(channel)->handleOtherMsg(kicker + " kicked " + kicked
                                                              + " from this channel(" + msg +")");
                }

                channels->value(channel)->deleteName(kicked);
            }
            break;
        }
    case BANLIST: {
            QStringList ban                    = theRest.split(' ');
            QString channel                    = ban.at(1);
            QString msg                        = channel + " Ban:";
            QMap<QString, Channel *> *channels = connection->getChannels();


            if (ban.count() > 4)
                msg += " " + QDateTime::fromTime_t(ban.at(4).toInt()).toString();

            msg += " " + ban.at(2);

            if (ban.count() > 3)
                msg += " " + ban.at(3);

            if (channels->find(channel) != channels->end())
                channels->value(channel)->handleServerMsg(msg);
            else
                channels->value("(server)")->handleServerMsg(msg);
            break;
        }
    case ENDOFBANLIST: {
            QString channel                    = theRest.section(' ', 1, 1);
            QString msg                        = channel + " " + theRest.section(" :", 1, 1);
            QMap<QString, Channel *> *channels = connection->getChannels();

            if (channels->find(channel) != channels->end())
                channels->value(channel)->handleServerMsg(msg);
            else
                channels->value("(server)")->handleServerMsg(msg);
            break;
        }
    case QUIT: {
            QString nick                       = line.remove(0, 1).section("!", 0, 0);
            QMap<QString, Channel *> *channels = connection->getChannels();
            QMapIterator<QString, Channel *> channelsIt(*channels);

            bool hasName = connection->getCurrentChannel()->hasName(nick);

            while (channelsIt.hasNext()) {
                channelsIt.next();
                channelsIt.value()->deleteName(nick);
            }

            QString msg = nick + " has quit (" + theRest + ")";

            if (hasName)
                connection->getCurrentChannel()->handleOtherMsg(msg);
            else
                channels->value("(server)")->handleOtherMsg(msg);
            break;
        }
    case NOTICE: {
            QString nick   = line.remove(0, 1).section("!", 0, 0);
            QString target = theRest.section(" :", 0, 0);
            QString msg    = theRest.section(" :", 1);

            if (target.at(0) == '&' || target.at(0) == '#' || target.at(0) == '+'
                || target.at(0) == '!') {

                if (msg.startsWith("")) {
                    msg  = handleCTCPMsg(msg, nick);
                    nick = "*";
                }

                QMap<QString, Channel *> *channels = connection->getChannels();

                if (channels->find(target) != channels->end())
                    channels->value(target)->handlePrivMsg(nick, msg);
            } else {
                if (connection->getCurrentChannel()->hasName(nick)) {

                    if (msg.startsWith("")) {
                        msg  = handleCTCPMsg(msg, nick);
                        nick = "*";
                    }

                    connection->getCurrentChannel()->handlePrivMsg(nick, msg);
                } else {

                    if (msg.startsWith("")) {
                        msg  = handleCTCPMsg(msg, nick);
                        nick = "*";
                    }

                    QMap<QString, Channel *> *channels = connection->getChannels();

                    channels->value("(server)")->handleServerMsg(msg);
                }
            }
            break;
        }
    case PONG: {
            QString from                       = theRest.section(' ', 0, 0);
            QString msg                        = theRest.section(" :", 1);
            QMap<QString, Channel *> *channels = connection->getChannels();
            QMapIterator<QString, Channel *> channelsIt(*channels);

            if (msg.toLongLong() == 0) {
                msg = "Ping reply from " + from + ": " + msg;
            } else {
                int msecs = QDateTime::currentMSecsSinceEpoch() - msg.toLongLong();
                msg       =  "Ping reply from " + from + ": " + QString::number(msecs)
                             + " milliseconds";
            }

            channels->value("(server)")->handleServerMsg(msg);
            break;
        }
    default: {
            QString line = theRest.section(' ', 1);

            if (line.startsWith(':'))
                line.remove(0, 1);

            QMap<QString, Channel *> *channels = connection->getChannels();
            channels->value("(server)")->handleServerMsg(line);
        }
    }
}

/**
 * Handle line from user input.
 *
 * @param line        Line that needs to be handled.
 * @param *connection Pointer to connection.
 */
void LineHandler::handleOut(QString line, Connection *connection)
{
    if (line.left(1) != "/") {
        write(line, connection);
    } else {
        QString filtered = line.remove(0, 1);
        QString command  = filtered.section(' ', 0 , 0);
        QString theRest  = filtered.section(' ', 1).trimmed();

        switch (commandStringToEnum[command.toUpper()]) {
        case NP: {
                QString currentSong = CurrentSong::getCurrentSong();
                if (currentSong != "") {
                    writeAction(currentSong, connection);
                } else {
                    QString msg = "You are not currently listening anything!";

                    connection->getCurrentChannel()->handleOtherMsg(msg);
                }

                break;
            }
        case ME: {
                writeAction(theRest, connection);
                break;
            }
        case TOPIC: {
                QString topic = "TOPIC " + connection->getCurrentChannel()->getName();

                if (theRest != "")
                    topic += ' ' + theRest;

                writeCommand(topic, connection);

                break;
            }
        case NAMES: {
                QString names;

                if (theRest != "")
                    names = "NAMES " + theRest;
                else
                    names = "NAMES " + connection->getCurrentChannel()->getName();

                writeCommand(names, connection);

                break;
            }
        case JOIN: {
                QString join = "JOIN " + theRest;
                writeCommand(join, connection);
                break;
            }
        case QUERY: {
                if (theRest != connection->getNick()) {
                    QMap<QString, Channel *> *channels  = connection->getChannels();
                    Channel **currentChannelPointer     = connection->getCurrentChannelPointer();
                    addChannel(channels, currentChannelPointer, theRest, 1, connection);
                }
                break;
            }
        case MODE: {
                QString channel = connection->getCurrentChannel()->getName();
                QString mode;

                if (channel == "(server)")
                    mode = "MODE " + theRest;
                else
                    mode = "MODE " + channel + ' ' + theRest;

                writeCommand(mode, connection);
                break;
            }
        case PART: {
                QString channel;
                QString part;
                QMap<QString, Channel *> *channels  = connection->getChannels();
                QStringList settings                = Config::getConfig()->value("Settings");

                if (theRest != "") {
                    if (theRest.contains(' ')) {
                        channel = theRest.section(' ', 0, 0);
                        part = "PART " + channel + " :" + theRest.section(' ', 1);
                    } else {
                        channel = theRest;
                        part = "PART " + channel;
                    }
                } else {
                    channel = connection->getCurrentChannel()->getName();
                    part = "PART " + channel + " :" + settings.at(4);
                }

                if (channels->find(channel) != channels->end()) {
                    removeChannel(channels, channel, connection);

                    if (channel.at(0) == '&' || channel.at(0) == '#' ||channel.at(0) == '+'
                        || channel.at(0) == '!') {
                        writeCommand(part, connection);
                    }
                }
                break;
            }
        case NICK: {
                writeCommand("NICK " + theRest, connection);
                break;
            }
        case PING: {
                if (connection->getCurrentChannel()->getName() == "(server)") {
                    if (theRest != "") {
                        writeCommand("PING " + theRest, connection);
                    } else {
                        QString time = QString::number(QDateTime::currentMSecsSinceEpoch());
                        writeCommand("PING " + time, connection);
                    }
                } else {
                    QString time = QString::number(QDateTime::currentMSecsSinceEpoch());
                    writeCommand("PRIVMSG " + theRest +" :PING " + time + "", connection);
                }
                break;
            }
        case WHOIS: {
                writeCommand("WHOIS " + theRest, connection);
                break;
            }
        case KICK: {
                QString nick;

                if (theRest.contains(' ')) {
                    nick    = theRest.section(' ', 0, 0);
                    theRest = " :" + theRest.section(' ', 1);
                } else {
                    nick    = theRest;
                    theRest = "";
                }

                writeCommand("KICK " + connection->getCurrentChannel()->getName() + " " + nick
                             + theRest, connection);
                break;
            }
        case BANLIST: {
                QString command;

                if (theRest != "")
                    command = "MODE " + theRest + " +b";
                else
                    command = "MODE " + connection->getCurrentChannel()->getName() + " +b";

                writeCommand(command, connection);
                break;
            }
        case RAW: {
                writeCommand(theRest, connection);
                break;
            }
        }
    }
}

/**
 * Write message to server.
 *
 * @param msg         Message that needs to be written.
 * @param *connection Pointer to connection.
 */
void LineHandler::write(QString msg,  Connection *connection)
{
    QString nick            = *connection->getNick();
    Channel *currentChannel = connection->getCurrentChannel();

    QString line;
    QString command    = "PRIVMSG " + currentChannel->getName() + " :";
    int length         = 510 - (command.length() + 4);

    while (msg.length() > length ) {
            line = msg.left(length);
            currentChannel->handleOwnMsg(nick, line);
            line = command + line + "\r\n";
            msg.remove(0, length);
            connection->getTcpSocket()->write(line.toAscii());
    }

    if (!msg.isEmpty()) {
        line = command + msg + "\r\n";
        currentChannel->handleOwnMsg(nick, msg);
        connection->getTcpSocket()->write(line.toAscii());
    }

}

/**
 * Write command to server.
 *
 * @param command     Command that needs to be written.
 * @param *connection Pointer to connection.
 */
void LineHandler::writeCommand(QString command, Connection *connection)
{
    command = command + "\r\n";
    connection->getTcpSocket()->write(command.toAscii());
}

/**
 * Write action to server.
 *
 * @param action      Action that needs to be written.
 * @param *connection Pointer to connection.
 */
void LineHandler::writeAction(QString action, Connection *connection)
{
    QString nick            = *connection->getNick();
    Channel *currentChannel = connection->getCurrentChannel();

    currentChannel->handleOwnAction(nick, action);
    action = "PRIVMSG " + currentChannel->getName() + " :ACTION " + action + "\r\n";
    connection->getTcpSocket()->write(action.toAscii());
}

/**
 * Add channel to channels.
 *
 * @param *channels        Channels.
 * @param **currentChannel Pointer to pointer of current channel.
 * @param channel          Name of the channel that is added.
 * @param type             Channel type.
 * @param *connection      Pointer to connection.
 */
void LineHandler::addChannel(QMap<QString, Channel *> *channels, Channel **currentChannel,
                             QString channel, int type, Connection *connection)
{
    connection->disconnectSignals();

    channels->insert(channel, new Channel(connection->getNick(), channel, type, connection));

    *currentChannel = channels->value(channel);

    connection->connectSignals(true);
    connection->channelChanged(true);
}

/**
 * Remove channel from channels.
 *
 * @param *channels   Channels.
 * @param channel     Name of the channel that is removed.
 * @param *connection Pointer to connection.
 */
void LineHandler::removeChannel(QMap<QString, Channel *> *channels, QString channel,
                                Connection *connection)
{
    Channel **currentChannel = connection->getCurrentChannelPointer();

    if (channels->value(channel) == *currentChannel) {
        connection->disconnectSignals();

        delete channels->take(channel);

        *currentChannel = channels->value("(server)");

        connection->channelChanged(false, true, channel);
    }
}

/**
 * Handles CTCP message and returns handled version.
 *
 * @param msg CTCP message.
 * @return Handled CTCP message.
 */
QString LineHandler::handleCTCPMsg(QString msg, QString sender)
{
    QString handled = "";
    if (msg.startsWith("ACTION")) {
            msg.remove("ACTION ");
            msg.remove("");

            handled = sender + " " + msg;
    } else if(msg.startsWith("PING")) {
        msg.remove("PING ");
        msg.remove("");

        int msecs = QDateTime::currentMSecsSinceEpoch() - msg.toLongLong();
        handled   =  "Ping reply from " + sender + ": " + QString::number(msecs) + " milliseconds";
    }

    return handled;
}
